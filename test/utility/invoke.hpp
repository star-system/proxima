#pragma once

#include <proxima/concept/reducible_with.hpp>
#include <proxima/concept/statefully_transducible_with.hpp>
#include <proxima/concept/statelessly_transducible_over.hpp>
#include <proxima/type_traits/output_symbol.hpp>
#include <proxima/type_traits/state_symbol.hpp>

#include <utility>

namespace proxima::utility
{
    template <typename Transducer, typename Symbol>
        requires statelessly_transducible_over<Transducer, Symbol>
    constexpr auto invoke (Transducer tr, Symbol x)
    {
        using output_symbol_type = output_symbol_t<Transducer, Symbol>;
        auto s = output_symbol_type{};
        tr(x, [& s] (auto x) {s = x;});
        return s;
    }

    template <typename Transducer, typename Symbol, typename State>
        requires statefully_transducible_with<Transducer, Symbol, State>
    constexpr auto invoke (Transducer tr, Symbol x, State st)
    {
        using output_symbol_type = output_symbol_t<Transducer, state_symbol_t<State>>;
        auto s = output_symbol_type{};
        tr(x, [& s] (auto a) {s = a;}, st);
        return std::pair(st, s);
    }

    template <typename Kernel, typename Symbol, typename State>
        requires reducible_with<Kernel, Symbol, State>
    constexpr auto invoke (Kernel k, Symbol x, State st)
    {
        k(x, st);
        return st;
    }
} // namespace proxima::utility
