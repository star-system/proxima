#include <proxima/compose.hpp>
#include <proxima/concept/emitting_reducible_over.hpp>
#include <proxima/concept/finalizable_over.hpp>
#include <proxima/concept/reducible_over.hpp>
#include <proxima/concept/transducible_over.hpp>
#include <proxima/kernel/fold.hpp>
#include <proxima/kernel/sum.hpp>
#include <proxima/transducer/chunk.hpp>
#include <proxima/transducer/group_by.hpp>
#include <proxima/transducer/take.hpp>
#include <proxima/transducer/transform.hpp>
#include <proxima/type.hpp>

#include <catch2/catch_test_macros.hpp>

#include <optional>
#include <vector>

TEST_CASE("Композиция преобразователей и ядра свёртки является ядром свёртки",
    "[compose][concept][reducible]")
{
    constexpr auto k =
        proxima::compose
        (
            proxima::transform([] (auto x) {return x * x;}),
            proxima::fold(0.0, [](auto x, auto y) {return x + y;})
        );
    REQUIRE(proxima::reducible_over<decltype(k), int>);
}

TEST_CASE("Композиция нефинализируемых преобразователей и ядра свёртки нефинализируема",
    "[compose][concept][finalizable]")
{
    constexpr auto k =
        proxima::compose
        (
            proxima::transform([] (auto x) {return x * x;}),
            proxima::fold(0.0, [] (auto x, auto y) {return x + y;})
        );
    REQUIRE_FALSE(proxima::finalizable_over<decltype(k), int>);
}

TEST_CASE("Композиция непорождающих преобразователей и ядра свёртки не является порождающей",
    "[compose][concept][reducible][emitting]")
{
    constexpr auto k =
        proxima::compose
        (
            proxima::transform([] (auto x) {return x * x;}),
            proxima::fold(0.0, [] (auto x, auto y) {return x + y;})
        );
    REQUIRE_FALSE(proxima::emitting_reducible_over<decltype(k), int>);
}

TEST_CASE("Композиция финализируемых преобразователей и нефинализируемого ядра свёртки "
    "финализируема", "[compose][concept][finalizable]")
{
    constexpr auto k =
        proxima::compose
        (
            proxima::transform([] (auto x) {return x * x;}),
            proxima::take(5),
            proxima::fold(0.0, [] (auto x, auto y) {return x + y;})
        );
    REQUIRE(proxima::finalizable_over<decltype(k), int>);
}

struct finalizable_kernel_t
{
    template <typename A, typename S>
    void operator () (A &&, S &) const
    {
    }
};

template <typename A>
auto initialize (const finalizable_kernel_t &, proxima::type_t<A>)
{
    return proxima::simple_state_t<A>{};
}

template <typename S>
bool is_final (const finalizable_kernel_t &, S &)
{
    return false;
}

TEST_CASE("Композиция финализируемых преобразователей и финализируемого ядра свёртки финализируема",
    "[compose][concept][finalizable]")
{
    constexpr auto kernel =
        proxima::compose
        (
            proxima::take(5),
            finalizable_kernel_t{}
        );
    const auto state = proxima::initialize(kernel, proxima::type<int>);
    CHECK_FALSE(proxima::is_final(kernel, state));

    CHECK(proxima::finalizable_over<decltype(kernel), int>);
}

TEST_CASE("Композиция преобразователей и ядра свёртки, среди которых есть хотя бы один в "
    "финальном состоянии, является финальной", "[compose]")
{
    const auto k =
        proxima::compose
        (
            proxima::transform([] (auto x) {return x * x;}),
            proxima::take(1),
            proxima::fold(0.0, [] (auto x, auto y) {return x + y;})
        );
    auto s = initialize(k, proxima::type<int>);

    REQUIRE(not proxima::is_final(k, s));
    k(3.14, s);
    REQUIRE(proxima::is_final(k, s));
}

TEST_CASE("Композиция порождающих преобразователей и непорождающего ядра свёртки является "
    "порождающией", "[compose][concept][reducible][emitting]")
{
    constexpr auto k =
        proxima::compose
        (
            proxima::transform([] (auto x) {return x * x;}),
            proxima::group_by(std::equal_to<>{}),
            proxima::fold(0ul, [] (auto x, auto y) {return x + y.size();})
        );
    REQUIRE(proxima::emitting_reducible_over<decltype(k), int>);
}

TEST_CASE("Композиция преобразователей и ядра свёртки, среди которых есть и финализируемые, "
    " и порождающие, является и финализируемой, и порождающей",
    "[compose][concept][reducible][emitting][finalizable]")
{
    constexpr auto k =
        proxima::compose
        (
            proxima::transform([] (auto x) {return x * x;}),
            proxima::take(5),
            proxima::group_by(std::equal_to<>{}),
            proxima::fold(0ul, [] (auto x, auto y) {return x + y.size();})
        );
    REQUIRE(proxima::emitting_reducible_over<decltype(k), int>);
    REQUIRE(proxima::finalizable_over<decltype(k), int>);
}

namespace space
{
    struct remember_last_item
    {
        template <typename A>
        constexpr auto operator () (A symbol, proxima::simple_state_t<A> & state) const
        {
            value(state) = symbol;
        }
    };

    template <typename T>
    constexpr auto initialize (const remember_last_item &, proxima::type_t<T>)
    {
        return proxima::simple_state_t<T>{};
    }
}

TEST_CASE("Пользователь может реализовать свою свёртку")
{
    constexpr auto k =
        proxima::compose
        (
            proxima::transform([] (auto x) {return x + 2;}),
            space::remember_last_item{}
        );

    auto s = initialize(k, proxima::type<int>);

    k(1, s);
    CHECK(value(s) == 3);

    k(2, s);
    CHECK(value(s) == 4);

    k(3, s);
    CHECK(value(s) == 5);
}

struct emitting_kernel_t
{
    template <typename A, typename S>
    void operator () (A &&, S &) const
    {
    }
};

template <typename A>
auto initialize (const emitting_kernel_t &, proxima::type_t<A>)
{
    return proxima::simple_state_t<A>{};
}

template <typename S>
void emit (const emitting_kernel_t &, S &)
{
}

TEST_CASE("Композиция порождающих преобразователей и порождающего ядра свёртки является "
    "порождающим ядром свёртки", "[compose][concept][reducible][emitting]")
{
    constexpr auto k =
        proxima::compose
        (
            proxima::chunk(10),
            emitting_kernel_t{}
        );
    auto s = proxima::initialize(k, proxima::type<double>);
    emit(k, s);

    REQUIRE(proxima::emitting_reducible_over<emitting_kernel_t, double>);
    CHECK(proxima::emitting_reducible_over<decltype(k), double>);
}

TEST_CASE("Композиция преобразователей является преобразователем",
    "[compose][concept][transducible]")
{
    constexpr auto t =
        proxima::compose
        (
            proxima::transform([] (auto x) {return x * x;}),
            proxima::take(1)
        );
    CHECK(proxima::transducible_over<decltype(t), int>);
}

TEST_CASE("Композиция преобразователей без состояния тоже не имеет состояния",
    "[compose][concept][stateless]")
{
    constexpr auto k =
        proxima::compose
        (
            proxima::transform([] (auto x) {return x * x;}),
            proxima::transform([] (auto x) {return x + x;})
        );
    CHECK(proxima::stateless_over<decltype(k), int>);

    const auto tape = [] (auto x) {CHECK(x == 18);};
    k(3, tape);
}

TEST_CASE("Композиция нефинализируемых преобразователей нефинализируема",
    "[compose][concept][finalizable]")
{
    constexpr auto k =
        proxima::compose
        (
            proxima::transform([] (auto x) {return x * x;}),
            proxima::transform([] (auto x) {return x + x;})
        );
    CHECK_FALSE(proxima::finalizable_over<decltype(k), int>);
}
TEST_CASE("Композиция преобразователей без завершения по требованию также не обладает завершением "
    "по требованию", "[compose][concept][emitting]")
{
    constexpr auto k =
        proxima::compose
        (
            proxima::transform([] (auto x) {return x * x;}),
            proxima::take(10)
        );
    CHECK_FALSE(proxima::emitting_transducible_over<decltype(k), int>);
}

TEST_CASE("Композиция преобразователей, среди которых есть хотя бы один финализируемый, "
    "финализируема", "[compose][concept][finalizable]")
{
    SECTION("финализируем второй из двух")
    {
        constexpr auto k =
            proxima::compose
            (
                proxima::transform([] (auto x) {return x * x;}),
                proxima::take(1)
            );
        CHECK(proxima::finalizable_over<decltype(k), int>);

        auto state = proxima::initialize(k, proxima::type<int>);

        CHECK(!proxima::is_final(k, state));

        const auto tape = [] (auto x) {CHECK(x == 9);};
        k(3, tape, state);

        CHECK(proxima::is_final(k, state));
    }
    SECTION("финализируем первый из двух")
    {
        constexpr auto k =
            proxima::compose
            (
                proxima::take(5),
                proxima::transform([] (auto x) {return x * x;})
            );
        CHECK(proxima::finalizable_over<decltype(k), int>);
    }
    SECTION("финализируемы оба")
    {
        constexpr auto k =
            proxima::compose
            (
                proxima::take(100),
                proxima::take(5)
            );
        CHECK(proxima::finalizable_over<decltype(k), int>);
    }
}

TEST_CASE("Композиция преобразователей, среди которых есть хотя бы один с завершением по "
    "требованию, тоже имеет завершение по требованию", "[compose][concept][finalizable]")
{
    SECTION("ЗПТ обладает второй из двух")
    {
        constexpr auto k =
            proxima::compose
            (
                proxima::transform([] (auto x) {return x * x;}),
                proxima::chunk(10)
            );
        CHECK(proxima::emitting_transducible_over<decltype(k), int>);

        auto state = proxima::initialize(k, proxima::type<int>);

        k(17, [] (auto) {}, state);
        proxima::emit(k, [] (auto x) {CHECK(x == std::vector{17 * 17});}, state);
    }
    SECTION("ЗПТ обладает первый из двух")
    {
        constexpr auto k =
            proxima::compose
            (
                proxima::chunk(10),
                proxima::transform([] (const auto & v) {return v.size();})
            );
        CHECK(proxima::emitting_transducible_over<decltype(k), int>);

        auto state = proxima::initialize(k, proxima::type<int>);

        k(121, [] (auto) {}, state);
        proxima::emit(k, [] (const auto & x) {CHECK(x == 1);}, state);
    }
    SECTION("оба обладают ЗПТ")
    {
        constexpr auto k =
            proxima::compose
            (
                proxima::chunk(10),
                proxima::chunk(20)
            );
        CHECK(proxima::emitting_transducible_over<decltype(k), int>);
    }
}

TEST_CASE("Финализируемость и завершение по требованию комбинируются при композиции "
    "преобразователей", "[compose][concept][finalizable][emitting]")
{
    constexpr auto k =
        proxima::compose
        (
            proxima::take(3),
            proxima::chunk(2)
        );
    CHECK(proxima::finalizable_over<decltype(k), int>);
    CHECK(proxima::emitting_transducible_over<decltype(k), int>);

    auto state = proxima::initialize(k, proxima::type<int>);
    CHECK(!proxima::is_final(k, state));

    k(18, [] (auto) {}, state);
    CHECK(!proxima::is_final(k, state));

    proxima::emit(k, [] (auto x) {CHECK(x == std::vector{18});}, state);
}

TEST_CASE("Тип выходного символа составного преобразователя совпадает с выходным символом "
    "последнего преобразователя в цепочке", "[compose][output_symbol]")
{
    constexpr auto t =
        proxima::compose
        (
            proxima::transform([] (auto x) {return static_cast<double>(x);}),
            proxima::chunk(2)
        );
    CHECK(std::is_same_v<proxima::output_symbol_t<decltype(t), int>, std::vector<double>>);
}

TEST_CASE("Допустима композиция композиций", "[compose]")
{
    const auto complex_composition =
        proxima::compose
        (
            proxima::compose
            (
                proxima::transform([] (auto x) {return std::optional{x};}),
                proxima::transform([] (auto optional) {return *optional;})
            ),
            proxima::compose
            (
                proxima::transform([](auto y) {return y * y;}),
                proxima::sum
            )
        );

    auto state = proxima::initialize(complex_composition, proxima::type<int>);
    complex_composition(5, state);

    CHECK(proxima::value(state) == 25);
}

TEST_CASE("Композиция ассоциативна по отношению к результату свёртки", "[compose][reduce]")
{
    constexpr auto take = proxima::take(3);
    constexpr auto transform = proxima::transform([] (auto x) {return x + 10;});
    const auto sequence = {1, 2, 3, 4, 5};

    constexpr auto right_kernel = proxima::compose(take, transform, proxima::sum);
    auto right_state = proxima::initialize(right_kernel, proxima::type<long>);

    auto item = sequence.begin();
    while (!proxima::is_final(right_kernel, right_state) && item != sequence.end())
    {
        right_kernel(*item, right_state);
        ++item;
    }
    CHECK(item == sequence.begin() + 3);
    CHECK(proxima::is_final(right_kernel, right_state));
    CHECK(proxima::value(right_state) == 11 + 12 + 13);

    constexpr auto left_kernel =
        proxima::compose
        (
            proxima::compose(take, transform),
            proxima::sum
        );
    auto left_state = proxima::initialize(left_kernel, proxima::type<long>);

    item = sequence.begin();
    while (!proxima::is_final(left_kernel, left_state) && item != sequence.end())
    {
        left_kernel(*item, left_state);
        ++item;
    }
    CHECK(item == sequence.begin() + 3);
    CHECK(proxima::is_final(left_kernel, left_state));
    CHECK(proxima::value(left_state) == proxima::value(right_state));
}
