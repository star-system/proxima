#include <proxima/type_traits/output_symbol.hpp>

#include <catch2/catch_test_macros.hpp>

#include <type_traits>
#include <utility>

namespace // anonymous
{
    struct no_explicit_definition_t {};

    struct inner_definition_t
    {
        template <typename A>
        using output_symbol_t = std::pair<A, int>;
    };
} // namespace anonymous

namespace space
{
    struct outer_definition_t {};
}

namespace proxima
{
    template <typename Symbol>
    struct output_symbol<space::outer_definition_t, Symbol>
    {
        using type = std::pair<Symbol, int>;
    };
}

TEST_CASE("Метафункция \"output_symbol\"", "[output_symbol]")
{
    SECTION("возвращает входной тип символа, если у него не определена "
        "собственная метафункция \"output_symbol_t\"")
    {
        CHECK(std::is_same_v<proxima::output_symbol<no_explicit_definition_t, int>::type, int>);
        CHECK(std::is_same_v<proxima::output_symbol_t<no_explicit_definition_t, int>, int>);
    }
    SECTION("возвращает результат метафункции самого преобразователя, если она есть")
    {
        CHECK(std::is_same_v
        <
            proxima::output_symbol_t<inner_definition_t, double>,
            inner_definition_t::output_symbol_t<double>
        >);
        CHECK(std::is_same_v
        <
            proxima::output_symbol_t<inner_definition_t, double>,
            std::pair<double, int>
        >);
    }
}

TEST_CASE("Метафункция \"output_symbol\" игнорирует спецификатор \"const\"","[output_symbol]")
{
    CHECK(std::is_same_v<proxima::output_symbol_t<const no_explicit_definition_t, int>, int>);
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<const inner_definition_t, double>,
        std::pair<double, int>
    >);
}

TEST_CASE("Метафункция \"output_symbol\" игнорирует спецификатор \"volatile\"","[output_symbol]")
{
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<volatile no_explicit_definition_t, int>,
        int
    >);
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<volatile inner_definition_t, int>,
        std::pair<int, int>
    >);
}

TEST_CASE("Метафункция \"output_symbol\" игнорирует ссылку","[output_symbol]")
{
    CHECK(std::is_same_v<proxima::output_symbol_t<no_explicit_definition_t &, int>, int>);
    CHECK(std::is_same_v<proxima::output_symbol_t<no_explicit_definition_t &&, int>, int>);
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<inner_definition_t &, int>,
        std::pair<int, int>
    >);
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<inner_definition_t &&, char>,
        std::pair<char, int>
    >);
}

TEST_CASE("Метафункция \"output_symbol\" игнорирует любые сочетания"
    " спецификаторов и ссылок","[output_symbol]")
{
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<const volatile no_explicit_definition_t &, int>,
        int
    >);
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<const volatile no_explicit_definition_t &&, int>,
        int
    >);
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<const volatile inner_definition_t &, int>,
        std::pair<int, int>
    >);
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<const volatile inner_definition_t &&, int>,
        std::pair<int, int>
    >);
}

TEST_CASE("Специализация метафункции \"output_symbol\" игнорирует"
    " спецификатор \"const\"","[output_symbol]")
{
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<const space::outer_definition_t, int>,
        std::pair<int, int>
    >);
}

TEST_CASE("Специализация метафункции \"output_symbol\" игнорирует"
    " спецификатор \"volatile\"","[output_symbol]")
{
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<volatile space::outer_definition_t, int>,
        std::pair<int, int>
    >);
}

TEST_CASE("Специализация метафункции \"output_symbol\" игнорирует ссылку","[output_symbol]")
{
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<space::outer_definition_t &, int>,
        std::pair<int, int>
    >);
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<space::outer_definition_t &&, int>,
        std::pair<int, int>
    >);
}

TEST_CASE("Специализация метафункции \"output_symbol\" игнорирует любые сочетания"
    " спецификаторов и ссылок","[output_symbol]")
{
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<const volatile space::outer_definition_t &, int>,
        std::pair<int, int>
    >);
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<const volatile space::outer_definition_t &&, int>,
        std::pair<int, int>
    >);
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<const volatile space::outer_definition_t, int>,
        std::pair<int, int>
    >);
    CHECK(std::is_same_v
    <
        proxima::output_symbol_t<const volatile space::outer_definition_t, int>,
        std::pair<int, int>
    >);
}
