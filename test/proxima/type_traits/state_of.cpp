#include <proxima/state/nullstate.hpp>
#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>
#include <proxima/type_traits/state_of.hpp>

#include <catch2/catch_test_macros.hpp>

#include <type_traits>
#include <vector>

namespace // anonymous
{
    struct stateful_pseudokernel_t {};

    constexpr auto initialize (const stateful_pseudokernel_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }

    struct stateless_pseudokernel_t {};

    struct stateful_pseudokernel_with_vector_state_t {};

    template <typename T>
    constexpr auto initialize (const stateful_pseudokernel_with_vector_state_t &, proxima::type_t<T>)
    {
        // Вектор не может быть параметризован ссылкой.
        return proxima::simple_state_t<std::vector<T>>{};
    }
} // namespace anonymous

namespace // anonymous
{
    template <typename T, typename A, typename = void>
    struct state_of_is_defined: std::false_type {};

    template <typename T, typename A>
    struct state_of_is_defined<T, A, std::void_t<proxima::state_of_t<T, A>>>: std::true_type {};

    template <typename T, typename A>
    constexpr auto state_of_is_defined_v = state_of_is_defined<T, A>::value;
}

TEST_CASE("Метафункция \"state_of\"", "[state_of]")
{
    SECTION("возвращает результат функции initialize, если она определена")
    {
        CHECK(state_of_is_defined_v<stateful_pseudokernel_t, int>);
        CHECK(std::is_same_v
        <
            proxima::state_of<stateful_pseudokernel_t, int>::type,
            proxima::simple_state_t<int>
        >);
        CHECK(std::is_same_v
        <
            proxima::state_of_t<stateful_pseudokernel_t, int>,
            proxima::simple_state_t<int>
        >);
    }

    SECTION("неопределена, если для ядра не определена функция initialize")
    {
        CHECK(not state_of_is_defined_v<stateless_pseudokernel_t, int>);
    }
    SECTION("неопределена, если инициализирующий символ для функции initialize "
        "не подходит по сигнатуре")
    {
        CHECK(not state_of_is_defined_v<stateful_pseudokernel_t, double>);
    }
}

template <typename...>
struct type_list;

template <typename T, typename... Ts>
constexpr auto are_same_v = std::is_same_v<type_list<T, Ts...>, type_list<Ts..., T>>;

TEST_CASE("Метафункция \"state_of\" игнорирует ссылки и CV-квалификаторы при символе", "[state_of]")
{
    CHECK(are_same_v
    <
        proxima::state_of_t<stateful_pseudokernel_with_vector_state_t, int>,
        proxima::state_of_t<stateful_pseudokernel_with_vector_state_t, int &>,
        proxima::state_of_t<stateful_pseudokernel_with_vector_state_t, const int>,
        proxima::state_of_t<stateful_pseudokernel_with_vector_state_t, volatile int>,
        proxima::state_of_t<stateful_pseudokernel_with_vector_state_t, const volatile int>,
        proxima::state_of_t<stateful_pseudokernel_with_vector_state_t, const int &>,
        proxima::state_of_t<stateful_pseudokernel_with_vector_state_t, int &>,
        proxima::state_of_t<stateful_pseudokernel_with_vector_state_t, int &&>,
        proxima::state_of_t<stateful_pseudokernel_with_vector_state_t, const int &&>
    >);
}
