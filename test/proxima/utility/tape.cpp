#include <proxima/kernel/sum.hpp>
#include <proxima/transducer/take.hpp>
#include <proxima/transducer/transform.hpp>
#include <proxima/utility/tape.hpp>

#include <catch2/catch_test_macros.hpp>

TEST_CASE("Может заворачивать в ленту ядро свёртки с его состоянием", "[make_tape][tape][kernel]")
{
    constexpr auto k = proxima::sum;
    auto s = proxima::initialize(k, proxima::type<int>);

    REQUIRE(proxima::value(s) == 0);
    auto tape = proxima::make_tape(k, s);

    tape(3);
    tape(4);

    CHECK(proxima::value(s) == 7);
}

TEST_CASE("Может заворачивать в ленту преобразователь с его состоянием и другой лентой",
    "[make_tape][tape][transducer][stateful]")
{
    constexpr auto t = proxima::take(2);
    auto s = proxima::initialize(t, proxima::type<int>);

    auto call_count = std::size_t{0};
    auto inner_tape = [& call_count] (auto) {++call_count;};

    auto tape = proxima::make_tape_ignoring_nullstate(t, inner_tape, s);

    REQUIRE(call_count == 0);
    tape(1);
    CHECK(call_count == 1);
    tape(2);
    CHECK(call_count == 2);

    CHECK(proxima::is_final(t, s));
}

TEST_CASE("nullstate просто игнорируется", "[make_tape][tape][transducer][stateful]")
{
    constexpr auto t = proxima::transform([] (auto x) {return x;});

    auto call_count = std::size_t{0};
    auto inner_tape =
        [& call_count] (auto x)
        {
            ++call_count;
            CHECK(x == call_count);
        };

    auto tape = proxima::make_tape_ignoring_nullstate(t, inner_tape, proxima::nullstate);

    REQUIRE(call_count == 0);
    tape(1u);
    CHECK(call_count == 1);
    tape(2u);
    CHECK(call_count == 2);
}

struct vectorizer_t
{
    template <typename A>
    using output_symbol_t = std::vector<A>;

    template <typename A, typename T>
    constexpr void operator () (A && symbol, T && tape) const
    {
        return std::forward<T>(tape)(std::vector{std::forward<A>(symbol)});
    }
};

TEST_CASE("Может заворачивать в ленту преобразователь без состояния",
    "[make_tape][tape][transducer][stateless]")
{
    constexpr auto t = vectorizer_t{};

    auto count = std::size_t{0};
    auto inner_tape = [& count] (const auto & v) {count += v.size();};

    auto tape = proxima::make_tape(t, inner_tape);

    REQUIRE(count == 0);
    tape(1);
    CHECK(count == 1);
    tape(2);
    CHECK(count == 2);
    tape(3);
    CHECK(count == 3);
}
