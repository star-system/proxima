#include <proxima/compose.hpp>
#include <proxima/concept/finalizable_over.hpp>
#include <proxima/kernel/fold.hpp>
#include <proxima/kernel/parallel_reduce_kernel.hpp>
#include <proxima/kernel/sum.hpp>
#include <proxima/transducer/take.hpp>
#include <proxima/transducer/transform.hpp>
#include <proxima/type.hpp>

#include <catch2/catch_test_macros.hpp>

#include <string>
#include <thread>

namespace // anonymous
{
    const auto get_thread_id =
        [] (auto & id)
        {
            return
                [& id] (auto...)
                {
                    id = std::this_thread::get_id();
                    return 0;
                };
        };

} // namespace anonymous

TEST_CASE("Ядро, обёрнутое в параллельном ядре свёртки, исполняется в отдельном потоке",
    "[kernel][parallel]")
{
    const auto no_thread_id = std::thread::id{};
    auto thread_id_1 = no_thread_id;
    const auto k = proxima::fold(0, get_thread_id(thread_id_1));

    const auto pk = proxima::parallel_reduce_kernel_t<decltype(k)>{k};
    auto state = proxima::initialize(pk, proxima::type<int>);
    pk(0, state);
    emit(pk, state);

    REQUIRE(thread_id_1 != no_thread_id);
    REQUIRE(thread_id_1 != std::this_thread::get_id());
}

TEST_CASE("Параллельное ядро свёртки может принимать больше элементов, чем допускает обёрнутое "
    "финализируемое ядро. При этом лишние элементы отбрасываются", "[transducer][pipe]")
{
    using input_symbol_type = std::string;
    const auto k =
        proxima::compose
        (
            proxima::take(2),
            proxima::fold(0, [] (auto call_count, auto) {return ++call_count;})
        );
    static_assert(proxima::finalizable_over<decltype(k), input_symbol_type>);

    const auto pk = proxima::parallel_reduce_kernel_t<decltype(k)>{k};
    auto state = proxima::initialize(pk, proxima::type<input_symbol_type>);
    pk("counted", state);
    pk("counted", state);
    pk("not counted", state);
    pk("again not counted", state);
    emit(pk, state);

    CHECK(value(state) == 2);
}

TEST_CASE("Символ, проходящий через параллельное ядро, не копируется", "[transducer][pipe]")
{
    using input_symbol_type = std::unique_ptr<int>;
    const auto k =
        proxima::compose
        (
            // `p` принимаем по значению, чтобы гарантировать перенос для `std::unique_ptr`.
            proxima::transform([] (auto p) {return *p;}),
            proxima::sum
        );

    const auto pk = proxima::parallel_reduce_kernel_t<decltype(k)>{k};
    auto state = proxima::initialize(pk, proxima::type<input_symbol_type>);
    pk(std::make_unique<int>(6), state);
    emit(pk, state);

    CHECK(value(state) == 6);
}
