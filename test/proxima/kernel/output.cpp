#include <proxima/kernel/output.hpp>
#include <proxima/type.hpp>

#include <catch2/catch_test_macros.hpp>

#include <vector>

TEST_CASE("Ядро \"output\" записывает полученный символ в итератор", "[output][kernel]")
{
    auto v = std::vector{17};
    const auto k = proxima::output(v.begin());
    auto s = proxima::initialize(k, proxima::type<int>);

    k(123, s);

    REQUIRE(v == std::vector{123});
}

TEST_CASE("Ядро \"output\" продвигает итератор, сохранённый в состоянии, на одну позицию вперёд "
    "после каждого своего вызова", "[output][kernel]")
{
    constexpr auto n = 10;
    auto v = std::vector<int>(10);
    const auto k = proxima::output(v.begin());
    auto s = proxima::initialize(k, proxima::type<int>);

    for ([[maybe_unused]] auto i = 0; i < n; ++i)
    {
        REQUIRE(value(s) == v.begin() + i);
        k(123, s);
    }
    REQUIRE(value(s) == v.end());
}
