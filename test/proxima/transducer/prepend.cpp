#include <proxima/compose.hpp>
#include <proxima/concept/statelessly_transducible_over.hpp>
#include <proxima/kernel/sum.hpp>
#include <proxima/reduce.hpp>
#include <proxima/transducer/prepend.hpp>

#include <catch2/catch_test_macros.hpp>

#include <array>
#include <string>
#include <type_traits>

TEST_CASE("Преобразователь \"prepend\" N раз записывает на выходную ленту заданный элемент,"
    "а затем пробрасывает входной элемент", "[prepend][transducer]")
{
    const auto symbol = 'u';
    const auto n = 3;
    constexpr auto t = proxima::prepend('f', n);

    auto result = std::string{};
    auto tape = [& result] (auto c) {result.push_back(c);};

    t(symbol, tape);

    CHECK(result == "fffu");
}

TEST_CASE("На каждый входной элемент на ленту записывается N дополнительных элементов",
    "[prepend][transducer]")
{
    const auto symbol = 'x';
    const auto n = 4;
    constexpr auto t = proxima::prepend('a', n);

    auto call_count = 0;
    auto tape = [& call_count] (auto) {++call_count;};

    t(symbol, tape);
    CHECK(call_count == n + 1);

    t(symbol, tape);
    CHECK(call_count == (n + 1) * 2);
}

TEST_CASE("По умолчанию N == 1", "[prepend][transducer]")
{
    const auto symbol = 'x';
    constexpr auto t = proxima::prepend('a');

    auto call_count = 0;
    auto tape = [& call_count] (auto) {++call_count;};

    t(symbol, tape);
    CHECK(call_count == 1 + 1);
}

TEST_CASE("Преобразователь \"prepend\" может быть выполнен на этапе компиляции ",
    "[prepend][transducer]")
{
    constexpr auto items = std::array{1, 2, 3};
    constexpr auto k = proxima::compose(proxima::prepend(5, 10), proxima::sum);
    constexpr auto result = proxima::reduce(items, k);

    static_assert(result == 1 + 2 + 3 + 5 * 10 * items.size());
    CHECK(result == 1 + 2 + 3 + 5 * 10 * items.size());
}

TEST_CASE("Тип выходного символа преобразователя \"prepend\" совпадает с типом входного символа",
    "[prepend][transducer]")
{
    auto t = proxima::prepend('c', 2);

    using input_symbol_type = int;
    using output_symbol_type = proxima::output_symbol_t<decltype(t), input_symbol_type>;
    CHECK(std::is_same_v<output_symbol_type, input_symbol_type>);
}

TEST_CASE("Преобразователь \"prepend\" удовлетворяет требованиям концепции "
    "\"statelessly_transducible_over\"", "[prepend][transducible][concept]")
{
    constexpr auto t = proxima::prepend(17, 7);
    CHECK(proxima::statelessly_transducible_over<decltype(t), int>);
    CHECK(proxima::statelessly_transducible_over<decltype(t), int &>);
    CHECK(proxima::statelessly_transducible_over<decltype(t), int &&>);
    CHECK(proxima::statelessly_transducible_over<decltype(t), const int &>);
}
