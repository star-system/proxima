#include <proxima/compose.hpp>
#include <proxima/kernel/fold.hpp>
#include <proxima/transducer/pipe.hpp>
#include <proxima/transducer/transform.hpp>
#include <proxima/type.hpp>

#include <catch2/catch_test_macros.hpp>

#include <thread>
#include <type_traits>

namespace // anonymous
{
    const auto get_thread_id =
        [] (auto & id)
        {
            return
                [& id] (auto...)
                {
                    id = std::this_thread::get_id();
                    return 0;
                };
        };

} // namespace anonymous

TEST_CASE("Звенья цепи, находящиеся по одну сторону от \"pipe\", выполняются в одном и том "
    "же потоке", "[transducer][pipe]")
{
    const auto no_thread_id = std::thread::id{};
    auto thread_id_1 = no_thread_id;
    auto thread_id_2 = no_thread_id;
    auto thread_id_3 = no_thread_id;
    auto thread_id_4 = no_thread_id;

    const auto pk =
        proxima::compose
        (
            proxima::transform(get_thread_id(thread_id_1)),
            proxima::transform(get_thread_id(thread_id_2)),
            proxima::pipe,
            proxima::transform(get_thread_id(thread_id_3)),
            proxima::fold(0, get_thread_id(thread_id_4))
        );
    auto state = proxima::initialize(pk, proxima::type<int>);
    pk(0, state);
    emit(pk, state);

    REQUIRE(thread_id_1 == thread_id_2);
    REQUIRE(thread_id_3 == thread_id_4);
}

TEST_CASE("Звенья цепи, находящиеся по разные стороны от \"pipe\", выполняются в разных потоках",
    "[transducer][pipe]")
{
    const auto no_thread_id = std::thread::id{};
    auto thread_id_1 = no_thread_id;
    auto thread_id_2 = no_thread_id;

    const auto pk =
        proxima::compose
        (
            proxima::transform(get_thread_id(thread_id_1)),
            proxima::pipe,
            proxima::fold(0, get_thread_id(thread_id_2))
        );
    auto state = proxima::initialize(pk, proxima::type<int>);
    pk(0, state);
    emit(pk, state);

    REQUIRE(thread_id_1 != thread_id_2);
}

TEST_CASE("Звенья цепи, находящиеся перед первым объектом-меткой \"pipe\", выполняются в "
    "том же потоке, что и вызывающая функция", "[transducer][pipe]")
{
    const auto no_thread_id = std::thread::id{};
    auto thread_id_1 = no_thread_id;

    const auto pk =
        proxima::compose
        (
            proxima::transform(get_thread_id(thread_id_1)),
            proxima::pipe,
            proxima::fold(0, [] (auto...) {return 0;})
        );
    auto state = proxima::initialize(pk, proxima::type<int>);
    pk(0, state);
    emit(pk, state);

    REQUIRE(thread_id_1 == std::this_thread::get_id());
}
