#include <proxima/transducer/pairwise.hpp>
#include <proxima/type.hpp>
#include <proxima/type_traits/output_symbol.hpp>

#include <catch2/catch_test_macros.hpp>

#include <cstddef>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

TEST_CASE("Пусть T — тип входного символа попарного преобразователя, тогда тип его выходного "
    "символа — std::pair<T>", "[pairwise][transducer]")
{
    const auto pairwise = proxima::pairwise;
    using input_symbol_type = float;
    using output_symbol_type = proxima::output_symbol_t<decltype(pairwise), input_symbol_type>;
    CHECK(std::is_same_v<output_symbol_type, std::pair<input_symbol_type, input_symbol_type>>);
}

TEST_CASE("Попарный преобразователь ничего не записывает на выходную ленту, после считывания "
    "первого символа со входной ленты", "[pairwise][transducer]")
{
    const auto pairwise = proxima::pairwise;
    auto state = proxima::initialize(pairwise, proxima::type<int>);

    auto tape_call_count = std::size_t{0};
    pairwise(17, [& tape_call_count] (auto) {++tape_call_count;}, state);

    CHECK(tape_call_count == 0);
}

TEST_CASE("Каждый символ на входной ленте, начиная со второго, порождает запись на выходную ленту "
    "пары, содержащей последние два символа, считанные со входной ленты", "[pairwise][transducer]")
{
    const auto pairwise = proxima::pairwise;
    auto state = initialize(pairwise, proxima::type<std::string>);

    auto symbols = std::vector<std::string>{"one", "two", "three", "four", "five"};

    pairwise(symbols[0], [&] (auto) {}, state);

    for (auto i = 1ul; i < symbols.size(); ++i)
    {
        auto output_symbol = std::pair<std::string, std::string>{};
        const auto tape =
            [& output_symbol] (auto && x) {output_symbol = std::forward<decltype(x)>(x);};
        pairwise(symbols[i], tape, state);
        CHECK(output_symbol == std::pair(symbols[i - 1], symbols[i]));
    }
}
