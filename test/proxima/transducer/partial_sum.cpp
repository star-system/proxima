#include <proxima/concept/transducible_over.hpp>
#include <proxima/transducer/partial_sum.hpp>
#include <proxima/type_traits/output_symbol.hpp>
#include <utility/invoke.hpp>

#include <catch2/catch_test_macros.hpp>

#include <concepts>
#include <initializer_list>
#include <string>
#include <type_traits>

TEST_CASE("Тип выходного символа преобразователя \"partial_sum\" совпадает с типом входного "
    "символа", "[partial_sum][transducer]")
{
    using input_symbol_type = double;
    using output_symbol_type =
        proxima::output_symbol_t<decltype(proxima::partial_sum), input_symbol_type>;

    REQUIRE(std::is_same_v<input_symbol_type, output_symbol_type>);
}

TEST_CASE("Преобразователь \"partial_sum\" на первом шаге возвращает ровно тот символ, который "
    "принял на вход", "[partial_sum][transducer]")
{
    const auto t = proxima::partial_sum;
    auto state = proxima::initialize(t, proxima::type<int>);

    auto [_, result] = proxima::utility::invoke(t, 17, state);
    static_cast<void>(_);

    REQUIRE(result == 17);
}

TEST_CASE("Преобразователь \"partial_sum\" возвращает сумму пройденных символов",
    "[partial_sum][transducer]")
{
    using string_type = std::basic_string<decltype(u8'a')>;

    const auto t = proxima::partial_sum;
    auto state0 = proxima::initialize(t, proxima::type<string_type>);

    const auto [state1, symbol1] = proxima::utility::invoke(t, string_type(u8"раз"), state0);
    REQUIRE(symbol1 == u8"раз");
    const auto [state2, symbol2] = proxima::utility::invoke(t, string_type(u8"два"), state1);
    REQUIRE(symbol2 == u8"раздва");
    const auto [state3, symbol3] = proxima::utility::invoke(t, string_type(u8"три"), state2);
    static_cast<void>(state3);
    REQUIRE(symbol3 == u8"раздватри");
}

TEST_CASE("Преобразователь \"partial_sum\" допускает пользовательский предикат",
    "[partial_sum][transducer]")
{
    const auto sum_mod_9 = [] (auto x, auto y) {return (x + y) % 9;};
    const auto t = proxima::partial_sum(sum_mod_9);
    auto state = proxima::initialize(t, proxima::type<int>);

    int result = 0;
    const auto tape = [& result] (auto c) {result = c;};
    for (auto symbol: {15, 5, 3, 4, 2, 2, 3})
    {
        t(symbol, tape, state);
    }

    REQUIRE(result == 7);
}

TEST_CASE("Преобразователь \"partial_sum\" удовлетворяет требованиям концепции "
    "\"transducible_over\"", "[partial_sum][transducible][concept]")
{
    constexpr auto f = [](auto x, auto y) {return x + y;};
    constexpr auto t = proxima::partial_sum(f);
    CHECK(proxima::transducible_over<decltype(t), int>);
    CHECK(proxima::transducible_over<decltype(t), double>);
    CHECK(proxima::transducible_over<decltype(t), std::string>);
}

TEST_CASE("\"partial_sum\" не является преобразователем, если запомненная функция неприминима к "
    "указанному типу", "[partial_sum][transducible][concept]")
{
    auto it = proxima::partial_sum([](std::integral auto x, decltype(x) y) {return x * y;});
    CHECK_FALSE(proxima::transducible_over<decltype(it), float>);
}
