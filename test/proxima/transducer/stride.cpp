#include <proxima/transducer/stride.hpp>
#include <proxima/type.hpp>
#include <proxima/type_traits/output_symbol.hpp>
#include <proxima/type_traits/state_value.hpp>
#include <utility/invoke.hpp>

#include <catch2/catch_test_macros.hpp>

#include <type_traits>

TEST_CASE("Тип значения состояния \"stride\" совпадает с типом \"n\"", "[stride][transducer]")
{
    const auto n = 10;
    auto stride = proxima::stride(n);
    auto s = proxima::initialize(stride, proxima::type<int>);

    using integral_type = std::decay_t<decltype(n)>;
    REQUIRE(std::is_same_v<integral_type, proxima::state_value_t<decltype(s)>>);
}

TEST_CASE("Тип выходного символа преобразователя \"stride\" совпадает с типом входного символа",
    "[stride][transducer]")
{
    auto stride = proxima::stride(10);

    using input_symbol_type = long;
    using output_symbol_type = proxima::output_symbol_t<decltype(stride), input_symbol_type>;
    REQUIRE(std::is_same_v<output_symbol_type, input_symbol_type>);
}

TEST_CASE("Начальное состояние \"stride\" равно нулю", "[stride][transducer]")
{
    const auto n = 10;
    const auto stride = proxima::stride(n);
    const auto s = proxima::initialize(stride, proxima::type<int>);
    REQUIRE(proxima::value(s) == 0);
}

TEST_CASE("Вызов преобразователя \"stride\" в начальном состоянии записывает символ на выходную "
    "ленту", "[stride][transducer]")
{
    const auto stride = proxima::stride(3);
    const auto s = proxima::initialize(stride, proxima::type<char>);

    const auto [_, output_symbol] = proxima::utility::invoke(stride, 'x', s);
    static_cast<void>(_);

    REQUIRE(output_symbol == 'x');
}

TEST_CASE("Вызов преобразователя \"stride\" n раз из начального состояния приводит его в "
    "состояние, эквивалентное начальному", "[stride][transducer]")
{
    const auto n = 10;
    const auto t = proxima::stride(n);
    auto s = proxima::initialize(t, proxima::type<char>);

    for ([[maybe_unused]] auto i = 0; i < n; ++i)
    {
        t('x', [] (auto) {}, s);
    }

    REQUIRE(proxima::value(s) == 0);
}

TEST_CASE("При n символах, поступивших со входной ленты, преобразователь \"stride\" записывает на "
    "выходную ленту только один", "[stride][transducer]")
{
    const auto n = 10;
    auto stride = proxima::stride(n);
    auto s = proxima::initialize(stride, proxima::type<char>);

    auto tape_call_count = 0;
    for ([[maybe_unused]] auto i = 0; i < n; ++i)
    {
        stride('x', [& tape_call_count] (auto) {++tape_call_count;}, s);
    }

    REQUIRE(tape_call_count == 1);
}

TEST_CASE("Преобразователь \"stride\" может быть выполнен на этапе компиляции",
    "[stride][transducer]")
{
    constexpr auto n = 4;
    constexpr auto stride = proxima::stride(n);
    constexpr auto s = proxima::initialize(stride, proxima::type<char>);

    constexpr auto r = proxima::utility::invoke(stride, 'x', s);
    constexpr auto x = r.second;
    static_assert(x == 'x');
    REQUIRE(x == 'x');
}
