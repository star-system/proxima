#include <proxima/transducer/drop.hpp>
#include <proxima/type_traits/output_symbol.hpp>
#include <proxima/type_traits/state_value.hpp>
#include <utility/invoke.hpp>

#include <catch2/catch_test_macros.hpp>

#include <type_traits>

TEST_CASE("Тип значения состояния \"drop\" совпадает с типом \"n\"", "[drop][transducer]")
{
    const auto n = 10;
    auto drop = proxima::drop(n);
    auto s = proxima::initialize(drop, proxima::type<int>);

    using integral_type = std::decay_t<decltype(n)>;
    REQUIRE(std::is_same_v<integral_type, proxima::state_value_t<decltype(s)>>);
}

TEST_CASE("Тип выходного символа преобразователя \"drop\" совпадает с типом входного символа",
    "[drop][transducer]")
{
    auto drop = proxima::drop(10);

    using input_symbol_type = long;
    using output_symbol_type = proxima::output_symbol_t<decltype(drop), input_symbol_type>;
    REQUIRE(std::is_same_v<output_symbol_type, input_symbol_type>);
}

TEST_CASE("Значение начального состояния преобразователя \"drop\" равно количеству символов, "
    "которые надо отбросить", "[drop][transducer]")
{
    const auto n = 10;
    auto drop = proxima::drop(n);
    auto s = proxima::initialize(drop, proxima::type<int>);
    REQUIRE(proxima::value(s) == n);
}

TEST_CASE("Отбрасывание символа уменьшает значение состояния ровно на единицу",
    "[drop][transducer]")
{
    const auto n = 10;
    auto drop = proxima::drop(n);
    auto s = proxima::initialize(drop, proxima::type<char>);

    drop('x', [] (auto) {}, s);
    REQUIRE(proxima::value(s) == n - 1);
}

TEST_CASE("Отбрасывание максимально возможного числа символов приводит к тому, что, "
    "значение состояния равно нулю", "[drop][transducer]")
{
    const auto n = 10;
    auto drop = proxima::drop(n);
    auto s = proxima::initialize(drop, proxima::type<char>);

    for ([[maybe_unused]] auto i = 0; i < n; ++i)
    {
        drop('x', [] (auto) {}, s);
    }

    REQUIRE(proxima::value(s) == 0);
}

TEST_CASE("После отбрасывания максимально возможного числа символов значение состояния более "
    "не изменяется", "[drop][transducer]")
{
    const auto n = 10;
    auto drop = proxima::drop(n);
    auto s = proxima::initialize(drop, proxima::type<char>);

    for ([[maybe_unused]] auto i = 0; i < 2 * n; ++i)
    {
        drop('x', [] (auto) {}, s);
    }

    REQUIRE(proxima::value(s) == 0);
}

TEST_CASE("После того, как отброшен максимально возможный префикс, берутся все символы без разбора",
    "[drop][transducer]")
{
    const auto n = 10;
    auto drop = proxima::drop(n);
    auto s = proxima::initialize(drop, proxima::type<char>);

    for ([[maybe_unused]] auto i = 0; i < n; ++i)
    {
        drop('x', [] (auto) {}, s);
    }

    auto tape_call_count = 0;

    const auto m = 20;
    for (auto i = 0; i < m; ++i)
    {
        auto tape =
            [& tape_call_count, i] (auto y)
            {
                REQUIRE(y == i - '0');
                ++tape_call_count;
            };
        drop(i - '0', tape, s);
    }

    REQUIRE(tape_call_count == m);
}

TEST_CASE("Отбрасывание префикса может быть выполнено на этапе компиляции", "[drop][transducer]")
{
    constexpr auto drop = proxima::drop(1);
    constexpr auto s = proxima::initialize(drop, proxima::type<char>);

    constexpr auto r_dropped = proxima::utility::invoke(drop, 'x', s);
    constexpr auto r_taken = proxima::utility::invoke(drop, 'y', r_dropped.first);
    constexpr auto y = r_taken.second;
    static_assert(y == 'y');
    REQUIRE(y == 'y');
}
