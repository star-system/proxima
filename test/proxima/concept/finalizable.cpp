#include <proxima/concept/finalizable_over.hpp>
#include <proxima/concept/finalizable_with.hpp>
#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <catch2/catch_test_macros.hpp>

namespace // anonymous
{
    struct finalizable_pseudokernel_t {};

    auto initialize (const finalizable_pseudokernel_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }

    bool is_final (const finalizable_pseudokernel_t &, const proxima::simple_state_t<int> &)
    {
        return true;
    }

    struct nonfinalizable_pseudokernel_t {};

    auto initialize (const nonfinalizable_pseudokernel_t &, proxima::type_t<double>)
    {
        return proxima::simple_state_t<double>{};
    }
} // namespace anonymous

TEST_CASE("Концепт \"finalizable_over\"", "[finalizable][concept]")
{
    SECTION("возвращает истину на заведомо финализируемом ядре")
    {
        CHECK(proxima::finalizable_over<finalizable_pseudokernel_t, int>);
    }
    SECTION("возвращает ложь на заведомо нефинализируемом ядре")
    {
        CHECK_FALSE
        (
            proxima::finalizable_over<nonfinalizable_pseudokernel_t, double>
        );
    }
    SECTION("возвращает ложь на ядре с несоответствующим ему типом состояния")
    {
        CHECK_FALSE(proxima::finalizable_over<finalizable_pseudokernel_t, double>);
    }
    SECTION("возвращает ложь на типе, не являющимся ядром")
    {
        CHECK_FALSE(proxima::finalizable_over<double, long>);
    }
}

TEST_CASE("Концепт \"finalizable_with\"", "[finalizable][concept]")
{
    SECTION("возвращает истину на заведомо финализируемом ядре")
    {
        CHECK(proxima::finalizable_with<finalizable_pseudokernel_t, proxima::simple_state_t<int>>);
    }
    SECTION("возвращает ложь на заведомо нефинализируемом ядре")
    {
        CHECK_FALSE(proxima::finalizable_with
        <
            nonfinalizable_pseudokernel_t,
            proxima::simple_state_t<double>
        >);
    }
    SECTION("возвращает ложь на ядре с несоответствующим ему типом состояния")
    {
        CHECK_FALSE(proxima::finalizable_with
        <
            finalizable_pseudokernel_t,
            proxima::simple_state_t<double>
        >);
    }
    SECTION("возвращает ложь на типе, не являющимся ядром")
    {
        CHECK_FALSE(proxima::finalizable_with<double, long>);
    }
}
