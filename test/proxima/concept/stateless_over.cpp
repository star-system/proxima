#include <proxima/concept/stateless_over.hpp>
#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <catch2/catch_test_macros.hpp>

namespace // anonymous
{
    struct stateful_pseudokernel_t {};

    template <typename V>
    constexpr auto initialize (const stateful_pseudokernel_t &, proxima::type_t<V>)
    {
        return proxima::simple_state_t<V>{V{}};
    }

    struct stateless_pseudokernel_t {};
} // namespace anonymous

TEST_CASE("Концепт \"stateless_over\"", "[stateless][concept]")
{
    SECTION("возвращает ложь на ядре с состоянием")
    {
        CHECK_FALSE(proxima::stateless_over<stateful_pseudokernel_t, int>);
    }
    SECTION("возвращает истину на ядре без состояния")
    {
        CHECK(proxima::stateless_over<stateless_pseudokernel_t, int>);
    }
    SECTION("возвращает истину на типе, не являющимся ядром")
    {
        CHECK(proxima::stateless_over<double, int>);
    }
}
