#include <proxima/concept/reducible_over.hpp>
#include <proxima/concept/reducible_with.hpp>
#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <catch2/catch_test_macros.hpp>

#include <string>

namespace // anonymous
{
    struct dummy_reduce_kernel_t
    {
        template <typename Symbol, typename State>
        void operator () (Symbol &&, State &) const
        {
            // Пусто.
        }
    };

    auto initialize (const dummy_reduce_kernel_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }

    struct fixed_symbol_dummy_reduce_kernel_t;

    auto initialize (const fixed_symbol_dummy_reduce_kernel_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }

    struct fixed_symbol_dummy_reduce_kernel_t
    {
        void operator () (int, proxima::state_of_t<fixed_symbol_dummy_reduce_kernel_t, int> &) const
        {
            // Пусто.
        }
    };
} // namespace anonymous

namespace // anonymous
{
    struct dummy_too_few_arguments_reduce_kernel_t
    {
        template <typename A>
        void operator () (A &&) const
        {
            // Пусто.
        }
    };

    auto initialize (const dummy_too_few_arguments_reduce_kernel_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }

    struct dummy_too_many_arguments_reduce_kernel_t
    {
        template <typename A, typename B, typename C>
        void operator () (A &&, B &&, C &&) const
        {
            // Пусто.
        }
    };

    auto initialize (const dummy_too_many_arguments_reduce_kernel_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }
}

namespace // anonymous
{
    struct wrong_initialize_signature_t
    {
        template <typename Symbol, typename State>
        void operator () (Symbol &&, State &) const
        {
            // Пусто.
        }
    };

    [[maybe_unused]]
    auto initialize (const wrong_initialize_signature_t &, proxima::type_t<int>, int)
    {
        return proxima::simple_state_t<int>{};
    }
} // namespace anonymous

TEST_CASE("Концепт \"reducible_over\"", "[reducible][concept]")
{
    SECTION("возвращает истину на ядре свёртки")
    {
        CHECK(proxima::reducible_over<dummy_reduce_kernel_t, int>);
    }
    SECTION("возвращает ложь на ядре с несоответствующим символом")
    {
        CHECK_FALSE(proxima::reducible_over<dummy_reduce_kernel_t, double>);
    }
    SECTION("возвращает ложь на типе, не являющимся ядром")
    {
        CHECK_FALSE(proxima::reducible_over<double, int>);
    }
}

TEST_CASE("Концепт \"reducible_over\" возвращает ложь на ядре с несоответствующей сигнатурой "
    "вызова", "[reducible][concept]")
{
    CHECK_FALSE(proxima::reducible_over<dummy_too_few_arguments_reduce_kernel_t, int>);
    CHECK_FALSE(proxima::reducible_over<dummy_too_many_arguments_reduce_kernel_t, int>);
}

TEST_CASE("Концепт \"reducible_over\" возвращает ложь на ядре с несоответствующей сигнатурой "
    "функции initialize", "[reducible][concept]")
{
    CHECK_FALSE(proxima::reducible_over<wrong_initialize_signature_t, int>);
}

TEST_CASE("Концепт \"reducible_with\"", "[reducible][concept]")
{
    SECTION("возвращает истину на ядре свёртки")
    {
        CHECK(proxima::reducible_with
        <
            dummy_reduce_kernel_t,
            int,
            proxima::state_of_t<dummy_reduce_kernel_t, int>
        >);
        CHECK(proxima::reducible_with
        <
            fixed_symbol_dummy_reduce_kernel_t,
            int,
            proxima::state_of_t<fixed_symbol_dummy_reduce_kernel_t, int>
        >);
    }
    SECTION("возвращает истину на ядре с символом, который неявно приводится к требуемому")
    {
        CHECK(proxima::reducible_with
        <
            fixed_symbol_dummy_reduce_kernel_t,
            double,
            proxima::state_of_t<fixed_symbol_dummy_reduce_kernel_t, int>
        >);
        CHECK(proxima::reducible_with
        <
            fixed_symbol_dummy_reduce_kernel_t,
            char,
            proxima::state_of_t<fixed_symbol_dummy_reduce_kernel_t, int>
        >);
    }
    SECTION("возвращает ложь на неправильном состоянии")
    {
        CHECK_FALSE(proxima::reducible_with
        <
            fixed_symbol_dummy_reduce_kernel_t,
            double,
            proxima::simple_state_t<double>
        >);
    }
    SECTION("возвращает ложь на ядре с символом, несоответствующим требуемому")
    {
        CHECK_FALSE(proxima::reducible_with
        <
            fixed_symbol_dummy_reduce_kernel_t,
            std::string,
            proxima::state_of_t<fixed_symbol_dummy_reduce_kernel_t, int>
        >);
    }
    SECTION("возвращает ложь на типе, не являющимся ядром")
    {
        CHECK_FALSE(proxima::reducible_with<double, int, char>);
    }
}

TEST_CASE("Концепт \"reducible_with\" возвращает ложь на ядре с несоответствующей сигнатурой "
    "вызова", "[reducible][concept]")
{
    CHECK_FALSE(proxima::reducible_with
    <
        dummy_too_few_arguments_reduce_kernel_t,
        int,
        proxima::state_of_t<dummy_too_few_arguments_reduce_kernel_t, int>
    >);
    CHECK_FALSE(proxima::reducible_with
    <
        dummy_too_many_arguments_reduce_kernel_t,
        int,
        proxima::state_of_t<dummy_too_many_arguments_reduce_kernel_t, int>
    >);
}
