#include <proxima/concept/emitting_transducible_over.hpp>
#include <proxima/concept/emitting_transducible_with.hpp>
#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>
#include <proxima/type_traits/state_of.hpp>

#include <catch2/catch_test_macros.hpp>

#include <string>

namespace // anonymous
{
    struct emitting_transducer_t;

    constexpr auto initialize (const emitting_transducer_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }

    struct emitting_transducer_t
    {
        template <typename Tape>
        void operator () (int, Tape &&, proxima::state_of_t<emitting_transducer_t, int>) const
        {
        }
    };

    template <typename Tape>
    constexpr void emit (const emitting_transducer_t &, Tape &&, proxima::simple_state_t<int> &)
    {
    }
}

namespace // anonymous
{
    struct non_emitting_transducer_t
    {
        template <typename A, typename T, typename S>
        void operator () (A &&, T &&, S &) const
        {
        }
    };

    template <typename A>
    constexpr auto initialize (const non_emitting_transducer_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }
}

namespace // anonymous
{
    struct stateless_transducer_t
    {
        template <typename Tape>
        void operator () (int, Tape &&) const
        {
        }
    };

    template <typename Tape>
    constexpr void emit (const stateless_transducer_t &, Tape &&, proxima::simple_state_t<int> &)
    {
    }
}

namespace // anonymous
{
    struct wrong_transducer_emit_signature_t
    {
        template <typename T>
        constexpr void operator () (int, T &&, proxima::simple_state_t<int> &) const
        {
        }
    };

    constexpr auto initialize (const wrong_transducer_emit_signature_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }

    template <typename T>
    constexpr void
        emit (const wrong_transducer_emit_signature_t &, T &&, proxima::simple_state_t<double> &)
    {
    }

    struct too_few_transducer_emit_arguments_t
    {
        template <typename A, typename T, typename S>
        void operator () (A &&, T &&, S &) const
        {
        }
    };

    template <typename A>
    constexpr auto initialize (const too_few_transducer_emit_arguments_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }

    template <typename A>
    constexpr void emit (const too_few_transducer_emit_arguments_t &, A &&)
    {
    }

    struct too_many_transducer_emit_arguments_t
    {
        template <typename A, typename T, typename S>
        void operator () (A &&, T &&, S &) const
        {
        }
    };

    template <typename A>
    constexpr auto initialize (const too_many_transducer_emit_arguments_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }

    template <typename A, typename B, typename C>
    constexpr void emit (const too_many_transducer_emit_arguments_t &, A &&, B &&, C &&)
    {
    }
} // namespace anonymous

namespace // anonymous
{
    struct too_few_arguments_transducer_t
    {
        template <typename A>
        void operator () (A &&) const
        {
        }

        template <typename A, typename B>
        void operator () (A &&, B &&) const
        {
        }
    };

    template <typename A>
    constexpr auto initialize (const too_few_arguments_transducer_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }

    template <typename T, typename A>
    constexpr void emit (const too_few_arguments_transducer_t &, T &&, proxima::simple_state_t<A> &)
    {
    }

    struct too_many_arguments_transducer_t
    {
        template <typename A, typename B, typename C, typename D>
        void operator () (A &&, B &&, C &&, D &&) const
        {
        }
    };

    template <typename A>
    constexpr auto initialize (const too_many_arguments_transducer_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }

    template <typename T, typename A>
    constexpr void
        emit (const too_many_arguments_transducer_t &, T &&, proxima::simple_state_t<A> &)
    {
    }

    struct no_braces_operator_transducer_t
    {
    };

    template <typename A>
    constexpr auto initialize (const no_braces_operator_transducer_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }

    template <typename T, typename A>
    constexpr void
        emit (const no_braces_operator_transducer_t &, T &&, proxima::simple_state_t<A> &)
    {
    }
}

TEST_CASE("Концепт \"emitting_transducible_over\" возвращает истину, если для преобразователя "
    "определена функция emit", "[emitting][transducible][concept]")
{
    CHECK(proxima::emitting_transducible_over<emitting_transducer_t, int>);
}

TEST_CASE("Концепт \"emitting_transducible_over\" возвращает ложь, если для преобразователя не "
    "определена функция emit", "[emitting][transducible][concept]")
{
    CHECK_FALSE(proxima::emitting_transducible_over<non_emitting_transducer_t, int>);
}

TEST_CASE("Концепт \"emitting_transducible_over\" возвращает ложь, если у преобразователя нет "
    "состояния, даже при том, что функция emit определена", "[emitting][transducible][concept]")
{
    CHECK_FALSE(proxima::emitting_transducible_over<stateless_transducer_t, int>);
}

TEST_CASE("Концепт \"emitting_transducible_over\" возвращает ложь, если состояние для функции "
    "emit для преобразователя не подходит по сигнатуре", "[emitting][transducible][concept]")
{
    CHECK_FALSE(proxima::emitting_transducible_over<emitting_transducer_t, double>);
}

TEST_CASE("Концепт \"emitting_transducible_over\" возвращает ложь, если функция emit для "
    "преобразователя имеет неправильную сигнатуру", "[emitting][transducible][concept]")
{
    CHECK_FALSE(proxima::emitting_transducible_over<wrong_transducer_emit_signature_t, int>);
    CHECK_FALSE(proxima::emitting_transducible_over<too_few_transducer_emit_arguments_t, int>);
    CHECK_FALSE(proxima::emitting_transducible_over<too_many_transducer_emit_arguments_t, int>);
}

TEST_CASE("Концепт \"emitting_transducible_over\" возвращает ложь, если преобразователь имеет "
    "неправильную сигнатуру вызова", "[emitting][transducible][concept]")
{
    CHECK_FALSE(proxima::emitting_transducible_over<too_few_arguments_transducer_t, int>);
    CHECK_FALSE(proxima::emitting_transducible_over<too_many_arguments_transducer_t, int>);
    CHECK_FALSE(proxima::emitting_transducible_over<no_braces_operator_transducer_t, int>);
}


TEST_CASE("Концепт \"emitting_transducible_with\" возвращает истину, если для преобразователя "
    "определена функция emit", "[emitting][transducible][concept]")
{
    CHECK(proxima::emitting_transducible_with
    <
        emitting_transducer_t,
        int,
        proxima::state_of_t<emitting_transducer_t, int>
    >);
}

TEST_CASE("Концепт \"emitting_transducible_with\" возвращает истину, если для преобразователя "
    "определена функция emit, при этом заданный символ не является порождающим для состояния, "
    "но может быть неявно приведён к нужному", "[emitting][transducible][concept]")
{
    CHECK(proxima::emitting_transducible_with
    <
        emitting_transducer_t,
        double,
        proxima::state_of_t<emitting_transducer_t, int>
    >);
}

TEST_CASE("Концепт \"emitting_transducible_with\" возвращает ложь, если для преобразователя "
    "определена функция emit, но заданный символ не является порождающим для состояния и не может "
    "быть приведён к нужному", "[emitting][transducible][concept]")
{
    CHECK_FALSE(proxima::emitting_transducible_with
    <
        emitting_transducer_t,
        std::string,
        proxima::state_of_t<emitting_transducer_t, int>
    >);
}

TEST_CASE("Концепт \"emitting_transducible_with\" возвращает ложь, если для преобразователя не "
    "определена функция emit", "[emitting][transducible][concept]")
{
    REQUIRE(proxima::statefully_transducible_with
    <
        non_emitting_transducer_t,
        int,
        proxima::state_of_t<non_emitting_transducer_t, int>
    >);

    CHECK_FALSE(proxima::emitting_transducible_with
    <
        non_emitting_transducer_t,
        int,
        proxima::state_of_t<non_emitting_transducer_t, int>
    >);
}

TEST_CASE("Концепт \"emitting_transducible_with\" возвращает ложь, если функция emit для "
    "преобразователя имеет неправильную сигнатуру", "[emitting][transducible][concept]")
{
    CHECK_FALSE(proxima::emitting_transducible_with
    <
        wrong_transducer_emit_signature_t,
        int,
        proxima::state_of_t<wrong_transducer_emit_signature_t, int>
    >);
    CHECK_FALSE(proxima::emitting_transducible_with
    <
        too_few_transducer_emit_arguments_t,
        int,
        proxima::state_of_t<too_few_transducer_emit_arguments_t, int>
    >);
    CHECK_FALSE(proxima::emitting_transducible_with
    <
        too_many_transducer_emit_arguments_t,
        int,
        proxima::state_of_t<too_many_transducer_emit_arguments_t, int>
    >);
}
