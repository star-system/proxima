#include <proxima/concept/statefully_transducible_over.hpp>
#include <proxima/concept/statefully_transducible_with.hpp>
#include <proxima/concept/statelessly_transducible_over.hpp>
#include <proxima/concept/transducible_over.hpp>
#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <catch2/catch_test_macros.hpp>

namespace // anonymous
{
    struct stateless_transducer_t
    {
        template <typename Symbol, typename Tape>
        void operator () (Symbol &&, Tape &&) const
        {
            // Пусто.
        }
    };
}

namespace // anonymous
{
    struct stateful_transducer_t;

    auto initialize (const stateful_transducer_t &, proxima::type_t<long>)
    {
        return proxima::simple_state_t<long>{};
    }

    struct stateful_transducer_t
    {
        template <typename Tape>
        void operator () (long, Tape &&, proxima::simple_state_t<long> &) const
        {
            // Пусто.
        }
    };
}

namespace // anonymous
{
    struct too_few_arguments_stateless_transducer_t
    {
        template <typename A>
        void operator () (A &&) const
        {
        }
    };

    struct too_many_arguments_stateless_transducer_t
    {
        template <typename A, typename B, typename C>
        void operator () (A &&, B &&, C &&) const
        {
        }
    };

    struct too_few_arguments_stateful_transducer_t
    {
        template <typename A, typename B>
        void operator () (A &&, B &&) const
        {
        }
    };

    template <typename A>
    auto initialize (const too_few_arguments_stateful_transducer_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }

    struct too_many_arguments_stateful_transducer_t
    {
        template <typename A, typename B, typename C, typename D>
        void operator () (A &&, B &&, C &&, D &&) const
        {
        }
    };

    template <typename A>
    auto initialize (const too_many_arguments_stateful_transducer_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }
}

TEST_CASE("Концепт \"statelessly_transducible_over\"", "[stateless][transducible][concept]")
{
    SECTION("возвращает истину на преобразователе без состояния")
    {
        CHECK(proxima::statelessly_transducible_over<stateless_transducer_t, int>);
    }
    SECTION("возвращает ложь на преобразователе с состоянием")
    {
        CHECK_FALSE(proxima::statelessly_transducible_over<stateful_transducer_t, long>);
    }
    SECTION("возвращает ложь на преобразователе с состоянием при несоответствующем символе")
    {
        CHECK_FALSE(proxima::statelessly_transducible_over<stateful_transducer_t, double>);
    }
    SECTION("возвращает ложь на типе, не являющимся преобразователем")
    {
        CHECK_FALSE(proxima::statelessly_transducible_over<double, int>);
    }
    SECTION("возвращает ложь на преобразователе с несоответствующей сигнатурой вызова")
    {
        CHECK_FALSE(proxima::statelessly_transducible_over<too_few_arguments_stateless_transducer_t, double>);
        CHECK_FALSE(proxima::statelessly_transducible_over<too_many_arguments_stateless_transducer_t, double>);
        CHECK_FALSE(proxima::statelessly_transducible_over<too_few_arguments_stateful_transducer_t, double>);
        CHECK_FALSE(proxima::statelessly_transducible_over<too_many_arguments_stateful_transducer_t, double>);
    }
}

TEST_CASE("Концепт \"statefully_transducible_over\"", "[stateful][transducible][concept]")
{
    SECTION("возвращает ложь на преобразователе без состояния")
    {
        CHECK_FALSE(proxima::statefully_transducible_over<stateless_transducer_t, int>);
    }
    SECTION("возвращает истину на преобразователе с состоянием")
    {
        CHECK(proxima::statefully_transducible_over<stateful_transducer_t, long>);
    }
    SECTION("возвращает ложь на преобразователе с состоянием при несоответствующем символе")
    {
        CHECK_FALSE(proxima::statefully_transducible_over<stateful_transducer_t, double>);
    }
    SECTION("возвращает ложь на типе, не являющимся преобразователем")
    {
        CHECK_FALSE(proxima::statefully_transducible_over<double, int>);
    }
    SECTION("возвращает ложь на преобразователе с несоответствующей сигнатурой вызова")
    {
        CHECK_FALSE(proxima::statefully_transducible_over<too_few_arguments_stateless_transducer_t, double>);
        CHECK_FALSE(proxima::statefully_transducible_over<too_many_arguments_stateless_transducer_t, double>);
        CHECK_FALSE(proxima::statefully_transducible_over<too_few_arguments_stateful_transducer_t, double>);
        CHECK_FALSE(proxima::statefully_transducible_over<too_many_arguments_stateful_transducer_t, double>);
    }
}

TEST_CASE("Концепт \"statefully_transducible_with\"", "[stateful][transducible][concept]")
{
    SECTION("возвращает ложь на преобразователе без состояния")
    {
        CHECK_FALSE(proxima::statefully_transducible_with
        <
            stateless_transducer_t,
            int,
            proxima::simple_state_t<int>
        >);
    }
    SECTION("возвращает истину на преобразователе с состоянием")
    {
        CHECK(proxima::statefully_transducible_with
        <
            stateful_transducer_t,
            long,
            proxima::state_of_t<stateful_transducer_t, long>
        >);
    }
    SECTION("возвращает истину на преобразователе с состоянием при несоответствующем символе, если "
        "этот символ может быть неявно преобразован к нужному")
    {
        CHECK(proxima::statefully_transducible_with
        <
            stateful_transducer_t,
            double,
            proxima::simple_state_t<long>
        >);
    }
    SECTION("возвращает ложь на преобразователе с состоянием при несоответствующем символе, если "
        "этот символ не может быть преобразован к нужному")
    {
        CHECK_FALSE(proxima::statefully_transducible_with
        <
            stateful_transducer_t,
            std::string,
            proxima::simple_state_t<long>
        >);
    }
    SECTION("возвращает ложь на типе, не являющимся преобразователем")
    {
        CHECK_FALSE(proxima::statefully_transducible_with<double, int, char>);
    }
    SECTION("возвращает ложь на преобразователе с несоответствующей сигнатурой вызова")
    {
        CHECK_FALSE(proxima::statefully_transducible_with
        <
            too_few_arguments_stateful_transducer_t,
            double,
            proxima::state_of_t<too_few_arguments_stateful_transducer_t, double>
        >);
        CHECK_FALSE(proxima::statefully_transducible_with
        <
            too_many_arguments_stateful_transducer_t,
            double,
            proxima::state_of_t<too_many_arguments_stateful_transducer_t, double>
        >);
    }
}

TEST_CASE("Концепт \"transducible_over\"", "[transducible][concept]")
{
    SECTION("возвращает истину на преобразователе без состояния")
    {
        CHECK(proxima::transducible_over<stateless_transducer_t, int>);
    }
    SECTION("возвращает истину на преобразователе с состоянием")
    {
        CHECK(proxima::transducible_over<stateful_transducer_t, long>);
    }
    SECTION("возвращает ложь на преобразователе с состоянием при несоответствующем символе")
    {
        CHECK_FALSE(proxima::transducible_over<stateful_transducer_t, double>);
    }
    SECTION("возвращает ложь на типе, не являющимся преобразователем")
    {
        CHECK_FALSE(proxima::transducible_over<double, int>);
    }
    SECTION("возвращает ложь на преобразователе с несоответствующей сигнатурой вызова")
    {
        CHECK_FALSE(proxima::transducible_over<too_few_arguments_stateless_transducer_t, double>);
        CHECK_FALSE(proxima::transducible_over<too_many_arguments_stateless_transducer_t, double>);
        CHECK_FALSE(proxima::transducible_over<too_few_arguments_stateful_transducer_t, double>);
        CHECK_FALSE(proxima::transducible_over<too_many_arguments_stateful_transducer_t, double>);
    }
}
