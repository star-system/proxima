#include <proxima/concept/emitting_reducible_over.hpp>
#include <proxima/concept/emitting_reducible_with.hpp>
#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>
#include <proxima/type_traits/state_of.hpp>

#include <catch2/catch_test_macros.hpp>

namespace // anonymous
{
    struct emitting_kernel_t;

    constexpr auto initialize (const emitting_kernel_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }

    struct emitting_kernel_t
    {
        void operator () (int, proxima::state_of_t<emitting_kernel_t, int> &) const
        {
        }
    };

    constexpr void emit (const emitting_kernel_t &, proxima::simple_state_t<int> &)
    {
    }
}

namespace // anonymous
{
    struct non_emitting_kernel_t
    {
        template <typename A, typename S>
        void operator () (A &&, S &) const
        {
        }
    };

    template <typename A>
    constexpr auto initialize (const non_emitting_kernel_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }
} // namespace anonymous

namespace // anonymous
{
    struct wrong_kernel_emit_signature_t
    {
        void operator () (int, proxima::simple_state_t<int> &) const
        {
        }
    };

    constexpr auto initialize (const wrong_kernel_emit_signature_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }

    constexpr void emit (const wrong_kernel_emit_signature_t &, proxima::simple_state_t<double> &)
    {
    }

    struct too_few_kernel_emit_arguments_t
    {
        template <typename A, typename S>
        void operator () (A &&, S &) const
        {
        }
    };

    template <typename A>
    constexpr auto initialize (const too_few_kernel_emit_arguments_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }

    constexpr void emit (const too_few_kernel_emit_arguments_t &)
    {
    }

    struct too_many_kernel_emit_arguments_t
    {
        template <typename A, typename S>
        void operator () (A &&, S &) const
        {
        }
    };

    template <typename A>
    constexpr auto initialize (const too_many_kernel_emit_arguments_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }

    template <typename A, typename B>
    constexpr void emit (const too_many_kernel_emit_arguments_t &, A &&, B &&)
    {
    }
}

namespace // anonymous
{
    struct too_few_arguments_kernel_t
    {
        template <typename A>
        void operator () (A &&) const
        {
        }
    };

    constexpr auto initialize (const too_few_arguments_kernel_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }

    constexpr void emit (const too_few_arguments_kernel_t &, proxima::simple_state_t<int> &)
    {
    }

    struct too_many_arguments_kernel_t
    {
        template <typename A, typename B, typename C>
        void operator () (A &&, B &&, C &&) const
        {
        }
    };

    template <typename A>
    constexpr auto initialize (const too_many_arguments_kernel_t &, proxima::type_t<A>)
    {
        return proxima::simple_state_t<A>{};
    }

    template <typename A>
    constexpr void emit (const too_many_arguments_kernel_t &, proxima::simple_state_t<A> &)
    {
    }

    struct no_braces_operator_kernel_t
    {
    };

    constexpr auto initialize (const no_braces_operator_kernel_t &, proxima::type_t<int>)
    {
        return proxima::simple_state_t<int>{};
    }

    constexpr void emit (const no_braces_operator_kernel_t &, proxima::simple_state_t<int> &)
    {
    }
}

TEST_CASE("Концепт \"emitting_reducible_over\" возвращает истину, если для ядра определена "
    "функция emit", "[emitting][reducible][concept]")
{
    CHECK(proxima::emitting_reducible_over<emitting_kernel_t, int>);
}

TEST_CASE("Концепт \"emitting_reducible_over\" возвращает ложь, если для ядра не определена "
    "функция emit", "[emitting][reducible][concept]")
{
    CHECK_FALSE(proxima::emitting_reducible_over<non_emitting_kernel_t, int>);
}

TEST_CASE("Концепт \"emitting_reducible_over\" возвращает ложь, если состояние для функции emit "
    "для ядра не подходит по сигнатуре", "[emitting][reducible][concept]")
{
    CHECK_FALSE(proxima::emitting_reducible_over<emitting_kernel_t, double>);
}

TEST_CASE("Концепт \"emitting_reducible_over\" возвращает ложь, если функция emit для ядра имеет "
    "неправильную сигнатуру", "[emitting][reducible][concept]")
{
    CHECK_FALSE(proxima::emitting_reducible_over<wrong_kernel_emit_signature_t, int>);
    CHECK_FALSE(proxima::emitting_reducible_over<too_few_kernel_emit_arguments_t, int>);
    CHECK_FALSE(proxima::emitting_reducible_over<too_many_kernel_emit_arguments_t, int>);
}

TEST_CASE("Концепт \"emitting_reducible_over\" возвращает ложь, если ядро свёртки имеет "
    "неправильную сигнатуру вызова", "[emitting][reducible][concept]")
{
    CHECK_FALSE(proxima::emitting_reducible_over<too_few_arguments_kernel_t, int>);
    CHECK_FALSE(proxima::emitting_reducible_over<too_many_arguments_kernel_t, int>);
    CHECK_FALSE(proxima::emitting_reducible_over<no_braces_operator_kernel_t, int>);
}

TEST_CASE("Концепт \"emitting_reducible_with\" возвращает истину, если для ядра определена "
    "функция emit", "[emitting][reducible][concept]")
{
    CHECK(proxima::emitting_reducible_with
    <
        emitting_kernel_t,
        int,
        proxima::state_of_t<emitting_kernel_t, int>
    >);
}

TEST_CASE("Концепт \"emitting_reducible_with\" возвращает истину, если для ядра определена "
    "функция emit, при этом заданный символ не является порождающим для состояния ядра, "
    "но может быть неявно приведён к нужному", "[emitting][reducible][concept]")
{
    CHECK(proxima::emitting_reducible_with
    <
        emitting_kernel_t,
        double,
        proxima::state_of_t<emitting_kernel_t, int>
    >);
}

TEST_CASE("Концепт \"emitting_reducible_with\" возвращает ложь, если для ядра определена "
    "функция emit, но заданный символ не является порождающим для состояния ядра, и не может "
    "быть неявно приведён к нужному", "[emitting][reducible][concept]")
{
    CHECK_FALSE(proxima::emitting_reducible_with
    <
        emitting_kernel_t,
        std::string,
        proxima::state_of_t<emitting_kernel_t, int>
    >);
}

TEST_CASE("Концепт \"emitting_reducible_with\" возвращает ложь, если для ядра не определена "
    "функция emit", "[emitting][reducible][concept]")
{
    REQUIRE(proxima::reducible_with
    <
        non_emitting_kernel_t,
        int,
        proxima::state_of_t<non_emitting_kernel_t, int>
    >);

    CHECK_FALSE(proxima::emitting_reducible_with
    <
        non_emitting_kernel_t,
        int,
        proxima::state_of_t<non_emitting_kernel_t, int>
    >);
}

TEST_CASE("Концепт \"emitting_reducible_with\" возвращает ложь, если функция emit для ядра имеет "
    "неправильную сигнатуру", "[emitting][reducible][concept]")
{
    CHECK_FALSE(proxima::emitting_reducible_with
    <
        wrong_kernel_emit_signature_t,
        int,
        proxima::state_of_t<wrong_kernel_emit_signature_t, int>
    >);
    CHECK_FALSE(proxima::emitting_reducible_with
    <
        too_few_kernel_emit_arguments_t,
        int,
        proxima::state_of_t<too_few_kernel_emit_arguments_t, int>
    >);
    CHECK_FALSE(proxima::emitting_reducible_with
    <
        too_many_kernel_emit_arguments_t,
        int,
        proxima::state_of_t<too_many_kernel_emit_arguments_t, int>
    >);
}
