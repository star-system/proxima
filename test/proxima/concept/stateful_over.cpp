#include <proxima/concept/stateful_over.hpp>
#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <catch2/catch_test_macros.hpp>

namespace // anonymous
{
    struct stateful_pseudokernel_t {};

    template <typename V>
    constexpr auto initialize (const stateful_pseudokernel_t &, proxima::type_t<V>)
    {
        return proxima::simple_state_t<V>{V{}};
    }

    struct stateless_pseudokernel_t {};
} // namespace anonymous

TEST_CASE("Концепт \"stateful_over\"", "[stateful][concept]")
{
    SECTION("возвращает истину на ядре с состоянием")
    {
        CHECK(proxima::stateful_over<stateful_pseudokernel_t, int>);
    }
    SECTION("возвращает ложь на ядре без состояния")
    {
        CHECK_FALSE(proxima::stateful_over<stateless_pseudokernel_t, int>);
    }
    SECTION("возвращает ложь на типе, не являющимся ядром")
    {
        CHECK_FALSE(proxima::stateful_over<double, int>);
    }
}
