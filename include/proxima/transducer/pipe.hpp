#pragma once

#include <proxima/kernel/parallel_reduce_kernel.hpp>

#include <utility>

namespace proxima
{
    struct pipe_t {};

    /*!
        \~english
            \brief
                Tag object for convenient parallel reduce kernel creation

            \details
                Tells the `compose` function to create the parallel reduce kernel.

                Each segment of the transducers chain between two `pipe`s or between `pipe` and
                the beginning or the ending of the whole chain will be executed in a separate
                thread.

                Usage example:

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::transform([] (auto x) {return x * x;}), // Thread #1
                        proxima::take_if([] (auto x) {return x < 5;})    // Thread #1
                        proxima::pipe,                                   // <-
                        proxima::to_vector                               // Thread #2
                    );
                \endcode

        \~russian
            \brief
                Объект-метка для удобного создания параллельного ядра свёртки

            \details
                Служит индикатором для функции `compose` о том, что нужно создать именно
                параллельное ядро свёртки.

                Каждый отрезок цепочки преобразователей, который находится между двумя разделителями
                `pipe`, или между разделителем и началом или концом всей цепочки, будет
                обрабатываться в отдельном потоке.

                Пример использования:

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::transform([] (auto x) {return x * x;}), // Поток №1
                        proxima::take_if([] (auto x) {return x < 5;})    // Поток №1
                        proxima::pipe,                                   // <-
                        proxima::to_vector                               // Поток №2
                    );
                \endcode

        \~  \see compose
            \see parallel_reduce_kernel_t
     */
    constexpr auto pipe = pipe_t{};

    /*!
        \~english
            \brief
                The extension of the `compose` function for the parallel reduce kernel

            \details
                Is called only in case of tag object `pipe` is met in the transducers chain.
                Wraps the result of the composition of `xs` into the parallel reduce kernel.

            \param xs
                Transducers and a reduce kernel.

            \returns
                A parallel reduce kernel.

        \~russian
            \brief
                Расширение функции `compose` для параллельного ядра свёртки

            \details
                Вызывается только в том случае, когда в цепочке преобразователей встречается
                объект-метка `pipe`.
                Оборачивает результат компоновки `xs` в параллельное ядро свёртки.

            \param xs
                Преобразователи и ядро свёртки.

            \returns
                Параллельное ядро свёртки.

        \~  \see parallel_reduce_kernel_t
            \see compose
            \see pipe_t
            \see pipe
     */
    template <typename... Ts>
    constexpr auto compose (pipe_t, Ts &&... xs)
    {
        using kernel_type = decltype(compose(std::forward<Ts>(xs)...));
        return parallel_reduce_kernel_t<kernel_type>{compose(std::forward<Ts>(xs)...)};
    }
} // namespace proxima
