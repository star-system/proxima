#pragma once

#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <concepts>
#include <functional>
#include <optional>
#include <type_traits>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                A transducer that replaces a symbol with its sum with all previous symbols

            \tparam BinaryFunction
                The type of the function that will be used to sum the symbols.

        \~russian
            \brief
                Преобразователь, заменяющий символ на его сумму со всеми предыдущими символами

            \tparam BinaryFunction
                Тип функции, с помощью которой будет производиться суммирование символов.

        \~  \see partial_sum
     */
    template <typename BinaryFunction>
    struct partial_sum_t
    {
        /*!
            \~english
                \brief
                    Processes one symbol from the input tape

                \details
                    Reads a symbol from the input tape, adds it to the current sum and writes the
                    copy of the resulting sum to the output tape.

                \param symbol
                    The symbol from the input tape.
                \param tape
                    The output tape. The result will be written to this tape.
                \param state
                    The current state of the transducer.

            \~russian
                \brief
                    Обработка одного символа со входной ленты

                \details
                    Считывает символ со входной ленты, добавляет этот символ к текущей сумме и
                    записывает на выходную ленту копию полученной суммы.

                \param symbol
                    Символ, который поступил со входной ленты.
                \param tape
                    Выходная лента, на которую будет записан результат.
                \param state
                    Текущее состояние преобразователя.
         */
        template <typename A, typename T, typename S>
            requires
            (
                std::convertible_to
                <
                    std::invoke_result_t<BinaryFunction, std::remove_cvref_t<A>, A>,
                    std::remove_cvref_t<A>
                >
            )
        constexpr void operator () (A && symbol, T && tape, S & state) const
        {
            if (not value(state).has_value())
            {
                value(state).emplace(std::forward<A>(symbol));
            }
            else
            {
                auto sum = plus(std::move(*value(state)), std::forward<A>(symbol));
                value(state).emplace(std::move(sum));
            }

            std::forward<T>(tape)(*value(state));
        }

        /*!
            \~english
                \brief
                    Creates a transducer with custom function

                \param plus
                    The function that will be used to sum the symbols.

                \returns
                    An instance of `partial_sum_t` with custom function.

            \~russian
                \brief
                    Создать преобразователь с пользовательской функцией

                \param plus
                    Функция, с помощью которого будет производиться суммирование символов.

                \returns
                    Экземпляр преобразователя `partial_sum_t` с заданным предикатом.
         */
        template <typename BinaryFunction1>
        constexpr auto operator () (BinaryFunction1 && plus) const
            -> partial_sum_t<std::decay_t<BinaryFunction1>>
        {
            return {std::forward<BinaryFunction1>(plus)};
        }

        BinaryFunction plus;
    };

    /*!
        \~english
            \brief
                A convenience instrument to use in `compose` function

            \details
                Usage examples:

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::partial_sum,
                        proxima::to_vector
                    );
                \endcode

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::partial_sum([] (auto x, auto y) {return (x + y) % 7;}),
                        proxima::to_vector
                    );
                \endcode

            \returns
                An instance of `partial_sum_t` transducer.

        \~russian
            \brief
                Инструмент для использования в функции `compose`

            \details
                Примеры использования:

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::partial_sum,
                        proxima::to_vector
                    );
                \endcode

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::partial_sum([] (auto x, auto y) {return (x + y) % 7;}),
                        proxima::to_vector
                    );
                \endcode

            \returns
                Экземпляр преобразователя `partial_sum_t`.

        \~  \see partial_sum_t
            \see compose
     */
    constexpr auto partial_sum = partial_sum_t<std::plus<>>{};

    template <typename BinaryFunction, typename Symbol>
    constexpr auto initialize (const partial_sum_t<BinaryFunction> &, type_t<Symbol>)
    {
        return simple_state_t<std::optional<Symbol>, Symbol>{};
    }
}
