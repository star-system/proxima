#pragma once

#include <cassert>
#include <concepts>
#include <cstddef>
#include <type_traits>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                A transducer that prepends several elements to the given element

            \tparam
                Type of element to prepend.
            \tparam N
                Type of the counter of the prepended elements.

        \~russian
            \brief
                Преобразователь, дописывающий дополнительные элементы перед каждым входным

            \tparam Value
                Тип элемента, который будет записан на выходную ленту.
            \tparam N
                Тип счётчика количества добавляемых элементов.

        \~  \see prepend
     */
    template <typename Value, std::integral N>
    struct prepend_t
    {
        /*!
            \~english
                \brief
                    Processes one symbol from the input tape

                \details
                    Writes the specified element `v` to the output tape `N` times, and then
                    rewrites the symbol from the input tape to the output tape.

                \param symbol
                    The symbol from the input tape.
                \param tape
                    The output tape. The result will be written to this tape.

            \~russian
                \brief
                    Обработка одного символа со входной ленты

                \details
                    Записывает на выходную ленту `N` раз заданный элемент `v`, а затем переписывает
                    символ со входной ленты на выходную.

                \param symbol
                    Символ, который поступил со входной ленты.
                \param tape
                    Выходная лента, на которую будет записан результат.
         */
        template <typename A, typename T>
            requires(std::convertible_to<Value, std::decay_t<A>>)
        constexpr void operator () (A && symbol, T && tape) const
        {
            for (auto i = N{0}; i < n; ++i)
            {
                tape(v);
            }
            std::forward<T>(tape)(std::forward<A>(symbol));
        }

        Value v;
        N n;
    };

    /*!
        \~english
            \brief
                Creates a prepending transducer

            \param v
                Element, copies of which will be written to the output tape.
            \param n
                The amount of elements to prepend.

            \pre
                `n >= 0`

            \returns
                An instance of `prepend_t`.

        \~russian
            \brief
                Создать преобразователь, дописывающий элементы перед каждым символом

            \param v
                Элемент, копии которого нужно записать на ленту.
            \param n
                Количество элементов, которое требуется дописать.

            \pre
                `n >= 0`

            \returns
                Экземпляр `prepend_t`.

        \~  \see prepend_t
     */
    template <typename Value, std::integral N = std::size_t>
    constexpr auto prepend (Value && v, N n = 1)
        -> prepend_t<std::decay_t<Value>, N>
    {
        assert(n >= N{0});

        return {std::forward<Value>(v), n};
    }
}
