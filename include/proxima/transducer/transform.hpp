#pragma once

#include <proxima/type_traits/output_symbol.hpp>

#include <functional>
#include <utility>
#include <concepts>

namespace proxima
{
    /*!
        \~english
            \brief
                Mapping or transforming transducer

            \details
                Applies a given function object to a symbol from the input tape, and writes the
                result to the output tape.

            \tparam UnaryFunction1
            \parblock
                The type of the function object that will be applied to the input symbol.
                The signature of the function object should be equivalent to the following:

                    R f (const A &);

                -   The signature does not need to have `const &`.
                -   The type `A` must be such that an input symbol can be implicitly converted to
                    `A`.
                -   The type `R` must be such that it can be written to the output tape.
            \endparblock

        \~russian
            \brief
                Отображающий преобразователь

            \details
                Применяет запомненное при конструировании отображение к символу, поступившему со
                входной ленты, и записывает результат на выходную ленту.

            \tparam UnaryFunction1
            \parblock
                Тип функционального объекта, который будет применяться к очередному символу
                со входной ленты. Его сигнатура должна быть эквивалентна следующей:

                    R f (const A &);

                -   Квалификатор `const &` для аргумента непосредственно не требуется.
                -   Тип `A` должен быть таким, чтобы символ со входной ленты мог неявно
                    преобразоваться к объекту типа `A`.
                -   Тип `R` должен быть таким, чтобы он мог быть записан на выходную ленту.
            \endparblock

        \~  \see transform
     */
    template <typename UnaryFunction1>
    struct transform_t
    {
        template <typename Symbol, typename UnaryFunction2>
            requires(std::invocable<UnaryFunction1, Symbol>)
        constexpr void operator () (Symbol && s, UnaryFunction2 && tape) const
        {
            // std::invoke не помечена как constexpr, поправить как только станет помечена.
            std::forward<UnaryFunction2>(tape)(f(std::forward<Symbol>(s)));
        }

        UnaryFunction1 f;
    };

    template <typename UnaryFunction, typename Symbol>
    struct output_symbol<transform_t<UnaryFunction>, Symbol>:
        std::invoke_result<UnaryFunction, Symbol> {};

    /*!
        \~english
            \brief
                Creates the mapping transducer

            \details
                Usage examples:

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::unique,
                        protima::transform([] (auto x) {return std::to_string(x);}),
                        proxima::to_vector
                    );
                \endcode

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::enumerate(10),
                        protima::transform(proxima::apply(std::multiplies<>{})),
                        proxima::sum
                    );
                \endcode

            \param f
                The function that will be applied to the symbols from the input tape.

            \returns
                An instance of `transform_t`.

        \~russian
            \brief
                Создание отображающего преобразователя

            \details
                Примеры использования:

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::unique,
                        protima::transform([] (auto x) {return std::to_string(x);}),
                        proxima::to_vector
                    );
                \endcode

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::enumerate(10),
                        protima::transform(proxima::apply(std::multiplies<>{})),
                        proxima::sum
                    );
                \endcode

            \param f
                Функция, которая будет применяться к символам со входной ленты.

            \returns
                Экземпляр `transform_t`.

        \~  \see transform_t
     */
    template <typename UnaryFunction>
    constexpr auto transform (UnaryFunction && f) -> transform_t<std::decay_t<UnaryFunction>>
    {
        return {std::forward<UnaryFunction>(f)};
    }
}
