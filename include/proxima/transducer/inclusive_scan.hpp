#pragma once

#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <type_traits>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                The inclusive prefix sum transducer

            \details
                A transducer that replaces an input symbol with inclusive prefix sum using a binary
                operation for the previous symbols based on the initial value.
                "Inclusive" means that the `i`-th input element is included in the `i`-th sum.

            \tparam Value
                The type of the initial value and an output symbol of the transducer, which is also
                the current value of its state.
            \tparam BinaryFunction
            \parblock
                The type of the function object that will be used to sum the symbols.
                The signature of the function object should be equivalent to the following:

                    R f (const A &, const B &);

                -   The signature does not need to have `const &`.
                -   The type `A` must be such that an object of type `Value` can be implicitly
                    converted to `A`.
                -   The type `B` must be such that an input symbol can be implicitly converted to
                    `B`.
                -   The type `R` must be such that an object of type `Value` can be assigned a
                    value of type `R`.
            \endparblock

        \~russian
            \brief
                Преобразователь включительной префиксной суммы

            \details
                Заменяет символ на его включительную префиксную сумму со всеми предыдущими
                символами, используя заданную двухместную операцию и некоторое начальное значение.
                "Включительная" сумма — это такая, что `i`-й элемент включён в `i`-ю сумму.

            \tparam Value
                Тип начального значения и выходного символа преобразователя, который по
                совместительству является значением текущего состояния.
            \tparam BinaryFunction
            \parblock
                Тип функционального объекта, с помощью которой будет производиться суммирование
                символов. Его сигнатура должна быть эквивалентна следующей:

                    R f (const A &, const B &);

                -   Квалификатор `const &` для аргументов непосредственно не требуется.
                -   Тип `A` должен быть таким, чтобы объект типа `Value` мог неявно преобразоваться
                    к объекту типа `A`.
                -   Тип `B` должен быть таким, чтобы символ со входной ленты мог неявно
                    преобразоваться к объекту типа `B`.
                -   Тип `R` должен быть таким, чтобы объекту типа `Value` можно было присвоить
                    объект типа `R`.
            \endparblock

        \~  \see inclusive_scan
     */
    template <typename Value, typename BinaryFunction>
    struct inclusive_scan_t
    {
        template <typename>
        using output_symbol_t = Value;
        /*!
            \~english
                \brief
                    Processes one symbol from the input tape

                \details
                    Reads a symbol from the input tape, adds it to the current sum and writes the
                    copy of the resulting sum to the output tape.

                \param symbol
                    A symbol from the input tape.
                \param tape
                    An output tape. The result will be written to this tape.
                \param state
                    The current state of the transducer.

            \~russian
                \brief
                    Обработка одного символа со входной ленты

                \details
                    Считывает символ со входной ленты, добавляет этот символ к текущей сумме и
                    записывает на выходную ленту копию полученной суммы.

                \param symbol
                    Символ, который поступил со входной ленты.
                \param tape
                    Выходная лента, на которую будет записан результат.
                \param state
                    Текущее состояние преобразователя.
         */
        template <typename A, typename T, typename S>
            requires(std::is_invocable_r_v<Value, BinaryFunction, Value, A>)
        constexpr void operator () (A && symbol, T && tape, S & state) const
        {
            auto sum = plus(std::move(value(state)), std::forward<A>(symbol));
            value(state) = std::move(sum);

            std::forward<T>(tape)(value(state));
        }

        Value init;
        BinaryFunction plus;
    };

    template <typename Value, typename BinaryFunction, typename Symbol>
    constexpr auto initialize (const inclusive_scan_t<Value, BinaryFunction> & t, type_t<Symbol>)
    {
        return simple_state_t<Value, Symbol>{t.init};
    }

    /*!
        \~english
            \brief
                Create an inclusive prefix sum transducer

            \details
                Usage example:

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::inclusive_scan(4, [] (auto x, auto y) {return (x + y) % 7;}),
                        proxima::to_vector
                    );
                \endcode

            \param v
                The initial value that will be considered the current sum applying the transducer
                to the first of the input symbols.
            \param fn
                A functional object that will be applied to the current sum and the next
                input symbol.

            \returns
                An instance of `inclusive_scan_t` transducer.

        \~russian
            \brief
                Создать преобразователь включительной префиксной суммы

            \details
                Пример использования:

                \code{.cpp}
                auto k =
                    proxima::compose
                    (
                        proxima::inclusive_scan(4, [] (auto x, auto y) {return (x + y) % 7;}),
                        proxima::to_vector
                    );
                \endcode

            \param v
                Начальное значение, которое будет считаться текущей суммой при применении
                преобразователя к первому из входных символов.
            \param fn
                Функциональный объект, который будет применяться к текущей сумме и следующему
                входному символу.

            \returns
                Экземпляр преобразователя `inclusive_scan_t`.

        \~  \see inclusive_scan_t
            \see compose
     */
    template <typename Value, typename BinaryFunction>
    constexpr auto inclusive_scan (Value && v, BinaryFunction && fn)
        -> inclusive_scan_t<std::decay_t<Value>, std::decay_t<BinaryFunction>>
    {
        return {std::forward<Value>(v), std::forward<BinaryFunction>(fn)};
    }
}
