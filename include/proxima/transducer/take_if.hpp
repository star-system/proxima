#pragma once

#include <concepts>
#include <type_traits>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                A positive filtering transducer

            \details
                Taking only those symbols that satisfy the predicate.

            \tparam UnaryPredicate
                A predicate, which will be applied to each symbol from the input tape.

        \~russian
            \brief
                Положительно фильтрующий преобразователь

            \details
                Оставляет только те символы, которые удовлетворяют предикату.

            \tparam UnaryPredicate
                Одноместный предикат, на соответствие которому будет проверяться каждый символ со
                входной ленты.

        \~  \see take_if
            \see take_t
            \see take_while_t
            \see drop_t
            \see drop_if_t
            \see drop_while_t
     */
    template <typename UnaryPredicate>
    struct take_if_t
    {
        /*!
            \~english
                \brief
                    Processes one symbol from the input tape

                \details
                    Reads a symbol from the input tape and checks if it satifies the predicate.
                    If the predicate returns true, then the symbol is written to the output tape.
                    Otherwise nothing is written to the output tape.

                \param symbol
                    The symbol from the input tape.
                \param tape
                    The output tape. In case of success the result will be written to this tape.

            \~russian
                \brief
                    Обработка одного символа со входной ленты

                \details
                    Считывает символ со входной ленты и проверяет его на соответствие предикату.
                    Если предикат возвращает истину на символе, то символ записывается на выходную
                    ленту. В противном случае на выходную ленту не пишется ничего.

                \param symbol
                    Символ, который поступил со входной ленты.
                \param tape
                    Выходная лента, на которую будет записан результат в случае успеха.
         */
        template <typename A, typename T>
            requires(std::predicate<UnaryPredicate, const std::remove_cvref_t<A> &>)
        constexpr void operator () (A && symbol, T && tape) const
        {
            if (p(std::as_const(symbol)))
            {
                std::forward<T>(tape)(std::forward<A>(symbol));
            }
        }

        UnaryPredicate p;
    };

    /*!
        \~english
            \brief
                Creates the positive filtering transducer

            \param p
                A predicate that will indicate if the symbol from the input tape should be taken
                or not.

            \returns
                An instance of `take_if_t`.

        \~russian
            \brief
                Создание положительно фильтрующего преобразователя

            \param p
                Предикат, на соответствие которому преобразователь будет проверять символы со
                входной ленты.

            \returns
                Экземпляр `take_if_t`.

        \~  \see take_if_t
            \see drop_if
            \see take_while
            \see drop_while
            \see take
            \see drop
     */
    template <typename UnaryPredicate>
    constexpr auto take_if (UnaryPredicate && p)
        -> take_if_t<std::decay_t<UnaryPredicate>>
    {
        return {std::forward<UnaryPredicate>(p)};
    }
}
