#pragma once

#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <optional>
#include <type_traits>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                A pairwise transducer

            \details
                For each element starting from second, generates and writes to the output tape the
                pair of the last two symbols from the input tape.

        \~russian
            \brief
                Попарный преобразователь

            \details
                Для каждого элемента, начиная со второго, порождает и записывает на выходную ленту
                пару из двух последних символов со входной ленты.

        \~  \see pairwise
     */
    struct pairwise_t
    {
        template <typename Symbol>
        using output_symbol_t = std::pair<Symbol, Symbol>;

        /*!
            \~english
                \brief
                    Processes one symbol from the input tape

                \details
                    Reads a symbol from the input tape.
                    If it was the first element read from the the input tape then it is placed to
                    the second element of the pair.
                    If it is the second, the third or any subsequent element, then the current
                    second element of the pair is moved to the first element of the pair, the input
                    symbol is moved to the second element of the pair, and the resulting pair is
                    being written to the output tape.

                \param symbol
                    The symbol from the input tape.
                \param tape
                    The output tape. The result will be written to this tape, if necessary.
                \param state
                    The current state of the transducer.

            \~russian
                \brief
                    Обработка одного символа со входной ленты

                \details
                    Считывает символ со входной ленты.
                    Если это был первый элемент, считанный со входной ленты, то кладёт его во второй
                    элемент пары.
                    Если это второй или любой последующий элемент, то перекладывает второй элемент
                    пары в первый, а во второй кладёт считанный элемент, после чего записывает
                    полученную пару на выходную ленту.

                \param symbol
                    Символ, который поступил со входной ленты.
                \param tape
                    Выходная лента, на которую при необходимости будет записан результат.
                \param state
                    Текущее состояние преобразователя.
         */
        template <typename A, typename T, typename S>
        constexpr void operator () (A && symbol, T && tape, S & state) const
        {
            if (not value(state).has_value())
            {
                value(state).emplace(std::decay_t<A>{}, std::forward<A>(symbol));
            }
            else
            {
                value(state)->first = std::move(value(state)->second);
                value(state)->second = std::forward<A>(symbol);
                std::forward<T>(tape)(*value(state));
            }
        }
    };

    template <typename T>
    constexpr auto initialize (const pairwise_t &, type_t<T>)
    {
        return simple_state_t<std::optional<std::pair<T, T>>, T>{};
    }

    /*!
        \~english
            \brief
                A convenience instrument to use in `compose` function

            \details
                Usage examples:

                \code{.cpp}
                // adjacent_difference
                auto k =
                    proxima::compose
                    (
                        proxima::pairwise,
                        proxima::transform(proxima::apply([] (auto x, auto y) {return y - x;})),
                        proxima::to_vector
                    );
                \endcode

            \returns
                An instance of `pairwise_t` transducer.

        \~russian
            \brief
                Инструмент для использования в функции `compose`

            \details
                Примеры использования:

                \code{.cpp}
                // adjacent_difference
                auto k =
                    proxima::compose
                    (
                        proxima::pairwise,
                        proxima::transform(proxima::apply([] (auto x, auto y) {return y - x;})),
                        proxima::to_vector
                    );
                \endcode

            \returns
                Экземпляр преобразователя `pairwise_t`.

        \~  \see pairwise_t
            \see compose
     */
    constexpr auto pairwise = pairwise_t{};
}
