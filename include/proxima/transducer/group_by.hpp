#pragma once

#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>
#include <proxima/type_traits/output_symbol.hpp>

#include <concepts>
#include <type_traits>
#include <utility>
#include <vector>

namespace proxima
{
    /*!
        \~english
            \brief
                A grouping transducer

            \details
                Combines input symbols into groups in which all symbols pairwise satisfies the given
                predicate. Writes those groups to the output tape.
                A group is represented by the instance of `std::vector`.

            \tparam BinaryPredicate
                The type of a predicate, which will be applied to symbols from the input tape.

        \~russian
            \brief
                Группирующий преобразователь

            \details
                Записывает на ленту группами/диапазонами, которые записаны в `std::vector`.
                Все символы внутри группы попарно удовлетворяют предикату.

            \tparam BinaryPredicate
                Предикат, относительно которого надо группировать символы.

        \~  \see group_by
     */
    template <typename BinaryPredicate>
    struct group_by_t
    {
        template <typename Symbol>
        using output_symbol_t = std::vector<Symbol>;

        /*!
            \~english
                \brief
                    Processes one symbol from the input tape

                \details
                    Reads a symbol from the input tape and checks if it belongs to the current
                    group.
                    If the current group is empty, then the symbol becomes the first symbol in a
                    group.
                    If the current group is not empty and the symbol belongs to it according to the
                    predicate, then the symbol is pushed to the end of the current group.
                    If the symbol does not belong to the current group, then the current group is
                    written to the output tape, and the symbol becomes the first symbol in a new
                    group.

                \param symbol
                    The symbol from the input tape.
                \param tape
                    The output tape. The result will be written to this tape, if necessary.
                \param state
                    The current state of the transducer.

            \~russian
                \brief
                    Обработка одного символа со входной ленты

                \details
                    Считывает символ со входной ленты и проверяет его на принадлежность к текущей
                    группе.
                    Если группа пока пуста, то символ записывается в неё безусловно.
                    Если группа непуста, и символ принадлежит группе согласно предикату, то он
                    просто записывается в конец группы.
                    Если же символ не принадлежит текущей группе, то вся старая группа записывается
                    на выходную ленту, а текущий символ становится первым элементом новой группы.

                \param symbol
                    Символ, который поступил со входной ленты.
                \param tape
                    Выходная лента, на которую при необходимости будет записан результат.
                \param state
                    Текущее состояние преобразователя.
         */
        template <typename Symbol, typename UnaryFunction, typename State>
            requires
            (
                std::predicate
                <
                    BinaryPredicate,
                    const std::remove_cvref_t<Symbol> &,
                    const std::remove_cvref_t<Symbol> &
                >
            )
        constexpr void operator () (Symbol && symbol, UnaryFunction && tape, State & state) const
        {
            auto & grouped = value(state);

            if (not grouped.empty() && not fn(std::as_const(grouped.back()), std::as_const(symbol)))
            {
                tape(std::move(grouped));
                grouped.clear();
            }

            grouped.push_back(std::forward<Symbol>(symbol));
        }

        BinaryPredicate fn;
    };

    template <typename BinaryPredicate, typename T>
    constexpr auto initialize (const group_by_t<BinaryPredicate> &, type_t<T>)
    {
        return simple_state_t<std::vector<T>, T>{};
    }

    /*!
        \~english
            \brief
                Creates a grouping transducer

            \param fn
                The grouping predicate.

            \returns
                An instance of `group_by_t`.

        \~russian
            \brief
                Создать группирующий преобразователь

            \param fn
                Предикат для группировки.

            \returns
                Экземпляр преобразователя `group_by_t` с указанным предикатом.

        \~  \see group_by_t
     */
    template <typename BinaryPredicate>
    constexpr auto group_by (BinaryPredicate && fn) -> group_by_t<std::decay_t<BinaryPredicate>>
    {
        return {std::forward<BinaryPredicate>(fn)};
    }

    /*!
        \~english
            \brief
                Emit the stored values of the grouping transducer

            \details
                If at least one symbol has been processed by the grouping transducer, its state
                will never be empty. There will always be at least one symbol in the current group.
                Therefore, after the reduction is finished, the stored group must be released.

            \param tape
                The output tape that will be used to write the emitted group.
            \param s
                The state with the stored group.

        \~russian
            \brief
                Выбросить накопленные в группирующем преобразователе результаты

            \details
                После того, как в преобразователь поступил хотя бы один символ, его состояние уже
                никогда не будет пустым. Всегда в текущей группе будет хотя бы один элемент.
                Поэтому после окончания вычислений нужно высвободить эту группу.

            \param tape
                Выходная лента, на которую будет выписана высвобожденная группа.
            \param s
                Состояние, в котором лежит группа с накопленными символами.

        \~  \see group_by_t
            \see emitting_transducible_over
     */
    template <typename BinaryPredicate, typename UnaryFunction, typename State>
    constexpr void
        emit (const group_by_t<BinaryPredicate> &, UnaryFunction && tape, State & s)
        {
            if (auto & grouped = value(s); not grouped.empty())
            {
                tape(std::move(grouped));
            }
        }
}
