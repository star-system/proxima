#pragma once

#include <concepts>
#include <type_traits>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                A transducer that applies a function to a forwarded symbol

            \tparam UnaryFunction
                The type of the function that will be applied to each symbol from the input tape.

        \~russian
            \brief
                Преобразователь, применяющий функцию к пробрасываемому символу

            \tparam UnaryFunction
                Тип функции, которая будет применена к каждому из символов со входной ленты.

        \~  \see transform_t
     */
    template <typename UnaryFunction>
    struct for_each_t
    {
        /*!
            \~english
                \brief
                    Processes one symbol from the input tape

                \details
                    Reads a symbol from the input tape, applies the function `f` to it, and
                    writes the symbol to the ouput tape.

                    There aren't any special rules for application of the function `f` to the
                    `symbol` except that the call `f(symbol)` must be defined. For instance,
                    this call may mutate the value of the symbol.

                    The result of a call `f(symbol)` is ignored.

                \param symbol
                    The symbol from the input tape.
                \param tape
                    The output tape. The symbol will be written to this tape.

            \~russian
                \brief
                    Обработка одного символа

                \details
                    Считывает символ со входной ленты, применяет к нему функцию `f`, а затем
                    записывает этот символ на выходную ленту.

                    Правила применения функции `f` к символу `symbol` не оговариваются за
                    исключением того, что вызов `f(symbol)` должен быть определён. В частности,
                    этот вызов может изменить значение символа.

                    Результат вызова `f(symbol)` игнорируется.

                \param symbol
                    Символ со входной ленты, к которому будет применена функция `f`.
                \param tape
                    Выходная лента, на которую будет записан символ.
         */
        template <typename A, typename T>
            requires(std::invocable<UnaryFunction, A &>)
        constexpr void operator () (A && symbol, T && tape) const
        {
            f(symbol);
            std::forward<T>(tape)(std::forward<A>(symbol));
        }

        UnaryFunction f;
    };

    /*!
        \~english
            \brief
                Creates the transducer that applies a function to a forwarded symbol

            \param f
                The function that will be applied to the symbols from the input tape.

            \returns
                An instance of `for_each_t`.

        \~russian
            \brief
                Создание преобразователя, применяющего функцию к пробрасываемому символу

            \param f
                Функция, которая будет применяться к символам со входной ленты.

            \returns
                Экземпляр `for_each_t`.

        \~  \see for_each_t
            \see transform
     */
    template <typename UnaryFunction>
    constexpr auto for_each (UnaryFunction && f)
        -> for_each_t<std::decay_t<UnaryFunction>>
    {
        return {std::forward<UnaryFunction>(f)};
    }
} // namespace proxima
