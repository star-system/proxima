#pragma once

#include <proxima/concept/emitting_reducible_over.hpp>
#include <proxima/concept/finalizable_with.hpp>
#include <proxima/concept/reducible_over.hpp>
#include <proxima/concept/reducible_with.hpp>
#include <proxima/type.hpp>
#include <proxima/type_traits/state_of.hpp>

#include <initializer_list>
#include <iterator>
#include <ranges>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                The reduction of a sequence

            \details
                Receives a sequence, a reduce kernel and its state.
                Passes all elements through the reduce kernel, thus changing its state.
                Returns the new state that occurred after running all the symbols through the
                kernel.

            \param [first, last)
                A sequence that must be reduced.
            \param s
                The initial state before the reduction.
            \param k
                The reduce kernel that specifies the reduction scheme.

            \returns
                The state after the reduction.

        \~russian
            \brief
                Свёртка диапазона с ядром

            \details
                Принимает на вход диапазон `[first, last)`, ядро свёртки и его состояние.
                Пропускает все символы через ядро свёртки, меняя тем самым его состояние.
                Возвращает новое состояние, которое возникло после прогона всех символов через
                ядро.

            \param [first, last)
                Полуинтервал, задающий исходную последовательность, к которой нужно применить
                свёртку.
            \param s
                Начальное состояние схемы вычислений до начала процесса свёртки.
            \param k
                Ядро свёртки, которое задаёт схему вычислений.

            \returns
                Обновлённое состояние ядра. Состояние после того как все символы были пропущены
                через ядро.

        \~  \see state_of_t
            \see reducible_with
     */
    template <std::input_iterator I, std::sentinel_for<I> J, typename S, typename K>
        requires reducible_with<K, std::iter_value_t<I>, S>
    constexpr auto reduce (I first, J last, S s, K & k)
    {
        while (first != last)
        {
            k(*first, s);
            ++first;
        }

        return s;
    }

    /*!
        \~english
            \brief
                The reduction of a sequence

            \details
                This is an overload for the reduce kernels modeling the Finalizable concept.

            \param [first, last)
                A sequence that must be reduced.
            \param s
                The initial state before the reduction.
            \param k
                The reduce kernel that specifies the reduction scheme.

            \returns
                The state after the reduction.

        \~russian
            \brief
                Свёртка диапазона с ядром

            \details
                Перегрузка для ядер свёртки, моделирующих концепцию раннего завершения. Т.е. делая
                шаг свёртки, нужно ещё и проверять не перешло ли ядро в конечное состояние.

            \param [first, last)
                Полуинтервал, задающий исходную последовательность, к которой нужно применить
                свёртку.
            \param s
                Начальное состояние схемы вычислений до начала процесса свёртки.
            \param k
                Ядро свёртки, которое задаёт схему вычислений.

            \returns
                Обновлённое состояние ядра. Состояние после того как все символы были пропущены
                через ядро.

        \~  \see state_of_t
            \see reducible_with
            \see finalizable_with
     */
    template <std::input_iterator I, std::sentinel_for<I> J, typename S, typename K>
        requires
        (
            reducible_with<K, std::iter_value_t<I>, S> &&
            finalizable_with<K, S>
        )
    constexpr auto reduce (I first, J last, S s, K & k)
    {
        while (first != last and not is_final(k, s))
        {
            k(*first, s);
            ++first;
        }

        return s;
    }

    /*!
        \~english
            \brief
                The reduction of a sequence without an explicit initial state

            \details
                The initial state is defined implicitly inside the function and is never returned
                to the outside.

            \param [first, last)
                A sequence that must be reduced.
            \param k
                The reduce kernel that specifies the reduction scheme.

            \returns
                The value of the resulting state of the reduce kernel after the reduction.

        \~russian
            \brief
                Свёртка диапазона с ядром без указания начального состояния

            \details
                Поскольку начальное состояние не задано явно, оно создаётся автоматически внутри
                функции и не выдаётся наружу.

            \param [first, last)
                Полуинтервал, задающий исходную последовательность, к которой нужно применить
                свёртку.
            \param k
                Ядро свёртки, которое задаёт схему вычислений.

            \returns
                Значение состояния после вычислений. Иначе, просто те результаты вычислений,
                которые копятся в состоянии.

        \~  \see reducible_over
     */
    template <std::input_iterator I, std::sentinel_for<I> J, reducible_over<std::iter_value_t<I>> K>
    constexpr auto reduce (I first, J last, K && k)
    {
        using value_type = std::iter_value_t<I>;
        auto s = proxima::reduce(first, last, initialize(k, type<value_type>), k);
        return std::move(value(s));
    }

    /*!
        \~english
            \brief
                The reduction of a sequence without an explicit initial state

            \details
                This is an overload for the reduce kernels modeling the Emitting concept.

            \param [first, last)
                A sequence that must be reduced.
            \param k
                The reduce kernel that specifies the reduction scheme.

            \returns
                The value of the reduce kernel state after the reduction and emitting the resulting
                state.

        \~russian
            \brief
                Свёртка диапазона с ядром без указания начального состояния

            \details
                Перегрузка для ядер свёртки, моделирующих концепцию завершения по требованию.
                Т.е. ядер, чьё состояние нужно высвободить по окончании вычислительного процесса.

            \param [first, last)
                Полуинтервал, задающий исходную последовательность, к которой нужно применить
                свёртку.
            \param k
                Ядро свёртки, которое задаёт схему вычислений.

            \returns
                Значение состояния после вычислений и высвобождения итогового состояния.

        \~  \see emitting_reducible_over
     */
    template <std::input_iterator I, std::sentinel_for<I> J, typename K>
        requires emitting_reducible_over<K, std::iter_value_t<I>>
    constexpr auto reduce (I first, J last, K && k)
    {
        using value_type = std::iter_value_t<I>;
        auto s = proxima::reduce(first, last, initialize(k, type<value_type>), k);
        emit(k, s);
        return std::move(value(s));
    }

    /*!
        \~english
            \brief
                The reduction of a sequence without an explicit initial state

            \details
                A convenience range-based overload.

            \param r
                A sequence that must be reduced.
            \param k
                The reduce kernel that specifies the reduction scheme.

            \returns
                The value of the resulting state of the reduce kernel after the reduction.

        \~russian
            \brief
                Свёртка диапазона с ядром без указания начального состояния

            \details
                Перегрузка для удобства. Принимает диапазон и ядро — всё. Инициализация
                схемы происходит внутри функции.

            \param r
                Диапазон. Т.е. сущность у которой можно получить итераторы `begin` и `end`.
            \param k
                Ядро свёртки, (приводимое или нет, не суть), которое задаёт схему вычислений.

            \returns
                Значение состояния после вычислений.

        \~  \see reducible_over
     */
    template <std::ranges::input_range R, reducible_over<std::ranges::range_value_t<R>> K>
    constexpr auto reduce (R && r, K && k)
    {
        using std::begin;
        using std::end;
        return proxima::reduce(begin(r), end(r), std::forward<K>(k));
    }

    /*!
        \~english
            \brief
                Reduction of a range represented by the initializer list without specifying
                the initial state of the reduce kernel

            \details
                A convenience overload. It is equivalent to the corresponding overload with a range
                instead of the initializer list.

            \param l
                A range represented by the initializer list.
            \param k
                The reduce kernel that specifies the reduction scheme.

            \returns
                The value of the resulting state of the reduce kernel after the reduction.

        \~russian
            \brief
                Свёртка диапазона, представленного списком инициализации, без указания
                начального состояния ядра свёртки

            \details
                Перегрузка для удобства. Эквивалентна соответствующей перегрузке с диапазоном
                вместо списка инициализации.

            \param l
                Диапазон, представленный списком инициализации.
            \param k
                Ядро свёртки, которое задаёт схему вычислений.

            \returns
                Значение состояния после вычислений.

        \~  \see reducible_over
     */
    template <typename Value, reducible_over<Value> K>
    constexpr auto reduce (std::initializer_list<Value> l, K && k)
    {
        return proxima::reduce(l.begin(), l.end(), std::forward<K>(k));
    }

    /*!
        \~english
            \brief
                The reduction of a sequence

            \details
                A convenience range-based overload.

            \param r
                A sequence that must be reduced.
            \param s
                The initial state before the reduction.
            \param k
                The reduce kernel that specifies the reduction scheme.

            \returns
                The state of the reduce kernel after the reduction.

        \~russian
            \brief
                Свёртка диапазона с ядром

            \details
                Перегрузка для работы с диапазоном. Принимает диапазон и ядро, и состояние.

            \param r
                Диапазон. Т.е. сущность у которой можно получить итераторы `begin` и `end`.
            \param s
                Начальное состояние схемы вычислений до начала процесса свёртки.
            \param k
                Ядро свёртки, (приводимое или нет, не суть), которое задаёт схему вычислений.

            \returns
                Состояние, в котором находится схема после вычислений.

        \~  \see state_of_t
            \see reducible_with
     */
    template <std::ranges::input_range R, typename S, typename K>
        requires reducible_with<K, std::ranges::range_value_t<R>, S>
    constexpr auto reduce (R && r, S s, K & k)
    {
        using std::begin;
        using std::end;
        return proxima::reduce(begin(r), end(r), std::move(s), k);
    }

    /*!
        \~english
            \brief
                Reduction of a range represented by the initializer list

            \details
                A convenience overload. It is equivalent to the corresponding overload with a range
                instead of the initializer list.

            \param l
                A range represented by the initializer list.
            \param s
                The initial state before the reduction.
            \param k
                The reduce kernel that specifies the reduction scheme.

            \returns
                The state of the reduce kernel after the reduction.

        \~russian
            \brief
                Свёртка диапазона, представленного списком инициализации

            \details
                Перегрузка для удобства. Эквивалентна соответствующей перегрузке с диапазоном
                вместо списка инициализации.

            \param l
                Диапазон, представленный списком инициализации.
            \param s
                Начальное состояние схемы вычислений до начала процесса свёртки.
            \param k
                Ядро свёртки, которое задаёт схему вычислений.

            \returns
                Состояние, в котором находится схема после вычислений.

        \~  \see state_of_t
            \see reducible_with
     */
    template <typename Value, typename S, typename K>
        requires reducible_with<K, Value, S>
    constexpr auto reduce (std::initializer_list<Value> l, S s, K & k)
    {
        return proxima::reduce(l.begin(), l.end(), std::move(s), k);
    }
}
