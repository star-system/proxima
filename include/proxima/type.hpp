#pragma once

namespace proxima
{
    template <typename>
    struct type_t {};

    template <typename T>
    constexpr auto type = type_t<T>{};
} // namespace proxima
