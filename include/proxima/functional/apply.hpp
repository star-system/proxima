#pragma once

#include <tuple>
#include <type_traits>
#include <utility>

namespace proxima
{
    template <typename NaryFunction>
    struct apply_fn
    {
        template <typename Tuple>
        constexpr decltype(auto) operator () (Tuple && t) const &
        {
            return std::apply(f, std::forward<Tuple>(t));
        }

        template <typename Tuple>
        constexpr decltype(auto) operator () (Tuple && t) &
        {
            return std::apply(f, std::forward<Tuple>(t));
        }

        template <typename Tuple>
        constexpr decltype(auto) operator () (Tuple && t) &&
        {
            return std::apply(std::move(f), std::forward<Tuple>(t));
        }

        NaryFunction f;
    };

    template <typename NaryFunction>
    constexpr auto apply (NaryFunction && f)
        -> apply_fn<std::decay_t<NaryFunction>>
    {
        return {std::forward<NaryFunction>(f)};
    }
} // namespace proxima
