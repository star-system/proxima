#pragma once

#include <type_traits>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                Function object wrapper that returns the negation of the predicate it holds

            \details
                Let the prediate be `p`, then `not_fn_t{p}(x) == not p(x)`.
                The only difference from `std::not_fn` is that this function object is fully
                `constexpr`.

            \tparam Predicate
                The wrapped predicate type. Its arity is not limited.

        \~russian
            \brief
                Функциональный объект-обёртка, возвращающий отрицание обёрнутого предиката

            \details
                Обозначим запомненный предикат `p`, тогда `not_fn_t{p}(x) == not p(x)`.
                Единственное отличие от `std::not_fn` в том, что данный функциональный объект
                полностью вычислим на этапе компиляции.

            \tparam Predicate
                Тип обёрнутого предиката. Его размерность не ограничена.

        \~  \see not_fn
     */
    template <typename Predicate>
    struct not_fn_t
    {
        template <typename ... As>
        constexpr bool operator () (As && ... as) const &
        {
            return not p(std::forward<As>(as)...);
        }

        template <typename ... As>
        constexpr bool operator () (As && ... as) &
        {
            return not p(std::forward<As>(as)...);
        }

        template <typename ... As>
        constexpr bool operator () (As && ... as) &&
        {
            return not std::move(p)(std::forward<As>(as)...);
        }

        Predicate p;
    };

    /*!
        \~english
            \brief
                Creates the negator of the arbitrary predicate

            \param p
                The predicate that would be wrapped inside the negator.

            \returns
                An instance of `not_fn_t` with the predicate `p`.

        \~russian
            \brief
                Создаёт отрицатель произвольного предиката

            \param p
                Предикат, который будет обёрнут в отрицатель.

            \returns
                Экземпляр `not_fn_t` с предикатом `p`.
     */
    template <typename Predicate>
    constexpr auto not_fn (Predicate && p)
        -> not_fn_t<std::decay_t<Predicate>>
    {
        return {std::forward<Predicate>(p)};
    }
}
