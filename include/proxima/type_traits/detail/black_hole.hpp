#pragma once

namespace proxima::detail
{
    struct black_hole_t
    {
        template <typename A>
        constexpr void operator () (A &&)
        {
        }
    };
} // namespace proxima::detail
