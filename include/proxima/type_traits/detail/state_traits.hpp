#pragma once

namespace proxima::detail
{
    template <typename State>
    struct state_traits_impl
    {
        // Пусто во имя поддержки SFINAE.
    };

    template <typename State>
        requires
            requires
            {
                typename State::symbol_type;
                typename State::value_type;
            }
    struct state_traits_impl<State>
    {
        using symbol_type = typename State::symbol_type;
        using value_type = typename State::value_type;
    };
} // namespace proxima::detail
