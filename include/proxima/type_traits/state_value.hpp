#pragma once

#include <proxima/type_traits/state_traits.hpp>

namespace proxima
{
    /*!
        \~english
            \brief
                State value

            \details
                A metafunction that maps the state type to the type of the value it stores.

                Each state has one and only one value. This value determines the reduction process
                in every point in time.
                To extract this value from the state, the free function `value` is used for a
                reduce kernel or `data` for a transducer.

                    value: S -> V
                    data: S -> V,

                where `S` — state type, `V` — value type.

            \tparam S
                State type.

            \returns
                Value type `V`.

        \~russian
            \brief
                Значение состояния

            \details
                Метафункция, отображающая тип состояния в тип значения, которое оно хранит.

                Каждое состояние имеет одно, и только одно значение. Данное значение определяет
                процесс свёртки в каждый момент времени.
                Для того, чтобы извлечь это значение из состояния, используется свободная функция
                `value` для ядра свёртки (или `data` для преобразователя).

                    value: S -> V
                    data: S -> V,

                где `S` — тип состояния, `V` — тип значения.

            \tparam S
                Тип состояния.

            \returns
                Тип значения `V`.

        \~  \see state_traits
            \see value
            \see data
     */
    template <typename S>
    struct state_value
    {
        using type = typename state_traits<S>::value_type;
    };

    template <typename S>
    using state_value_t = typename state_value<S>::type;
} // namespace proxima
