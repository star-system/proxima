#pragma once

#include <proxima/type_traits/state_traits.hpp>

namespace proxima
{
    /*!
        \~english
            \brief
                State symbol

            \details
                A metafunction that maps the state type to the type of the symbol it was produced
                from.

                Each state can be represented as a result of the initialization function of reduce
                kernel (or transducer) and symbol which it will operate with.

                    initialize: K × A -> S,

                where `K` — reduce kernel type, `A` — symbol type, `S` — state type.

            \tparam S
                State type.

            \returns
                Type `A` of the symbol from which the state was obtained, and no matter what reduce
                kernel (or transducer) participated in function.

        \~russian
            \brief
                Символ состояния

            \details
                Метафункция, отображающая тип состояния в тип символа, из которого оно было
                получено.

                Каждое состояние можно представить как результат функции инициализации от ядра
                свёртки (или преобразователя) и символа, с которыми оно будет оперировать.

                    initialize: K × A -> S,

                где `K` — тип ядра свёртки, `A` — тип символа, `S` — тип состояния.

            \tparam S
                Тип состояния.

            \returns
                Тип символа `A`, из которого было получено состояние, причём не важно, какое ядро
                свёртки (или преобразователь) участвовало в отображении.

        \~  \see state_traits
            \see initialize
     */
    template <typename S>
    using state_symbol_t = typename state_traits<S>::symbol_type;
} // namespace proxima
