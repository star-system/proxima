#pragma once

#include <proxima/concept/emitting_reducible_over.hpp>
#include <proxima/concept/emitting_transducible_over.hpp>
#include <proxima/concept/finalizable_over.hpp>
#include <proxima/concept/reducible_over.hpp>
#include <proxima/concept/stateful_over.hpp>
#include <proxima/concept/statefully_transducible_with.hpp>
#include <proxima/concept/statelessly_transducible_over.hpp>
#include <proxima/concept/transducible_over.hpp>
#include <proxima/state/composite_state.hpp>
#include <proxima/state/nullstate.hpp>
#include <proxima/type.hpp>
#include <proxima/type_traits/output_symbol.hpp>
#include <proxima/utility/is_finalizable_and_final.hpp>
#include <proxima/utility/tape.hpp>
#include <proxima/utility/transduce_ignoring_nullstate.hpp>

#include <concepts>
#include <type_traits>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                Composite transducer or reduce kernel

            \details
                Conceptually is a "matryoshka" that consists of a tranducer (the outer part of the
                "matryoshka") and a transducer or reduce kernel (the inner part of the
                "matryoshka").

                Behaves just like any other transducer or reduce kernel: has a state and a defined
                `initialize` function that creates this state, as well as the call operator that
                takes the approproate symbol, a tape (in case of transducer) and a state (in case
                the composite is stateful).

                The composite takes over the properties of the transducers and the reduce
                kernel, from which it was created. For example, if the transducer or the reduce
                kernel is finalizable, then composite reduce kernel will also be finalizable.
                Another example: if composite consists of two stateless transducers, then the
                composite will be stateless, too.

            \tparam T
                Transducer forming the outer part of the "matryoshka".
            \tparam K
                Transducer or reduce kernel forming the inner part of the "matryoshka".

        \~russian
            \brief
                Композит

            \details
                Составной преобразователь или ядро свёртки.

                Представляет собой "матрёшку", состоящую из преобразователя (внешней части
                "матрёшки") и преобразователя или ядра свёртки (внутренней части "матрёшки").

                Ведёт себя так же, как и любой другой преобразователь или ядро свёртки: имеет
                оператор вызова, принимающий соответствующие символ, ленту (в случае, если
                композит — преобразователь) и состояние (в случае, если композит им обладает).

                Составной преобразователь или ядро свёртки перенимает свойства преобразователей и
                ядра свёртки, из которых было создано. Например, если хотя бы один из них имеет
                раннее завершение, то полученный композит тоже будет иметь раннее завершение.
                Другой пример: если композит состоит из двух преобразователей без состояния, то и
                у композита не будет состояния.

            \tparam T
                Преобразователь представляющий внешнюю часть "матрёшки".
            \tparam K
                Преобразователь или ядро свёртки, представляющие внутреннюю часть "матрёшки".

        \~  \see compose
            \see composite_state_t
     */
    template <typename T, typename K>
    struct composite_t
    {
        template <typename Symbol, typename A>
            requires
            (
                std::convertible_to<Symbol, A> &&
                transducible_over<T, A> &&
                reducible_over<K, output_symbol_t<T, A>>
            )
        constexpr void operator () (Symbol && symbol, composite_state_t<T, K, A> & state) const
        {
            auto t = make_tape(tail, state.tail);
            transduce_ignoring_nullstate(head, std::forward<Symbol>(symbol), t, state.head);
        }

        template <typename Symbol, typename UnaryFunction, typename A>
            requires
            (
                std::convertible_to<Symbol, A> &&
                (
                    statefully_transducible_over<T, A> ||
                    statefully_transducible_over<K, output_symbol_t<T, A>>
                )
            )
        constexpr void
            operator ()
            (
                Symbol && symbol,
                UnaryFunction && tape,
                composite_state_t<T, K, A> & state
            ) const
        {
            auto t = make_tape_ignoring_nullstate(tail, tape, state.tail);
            transduce_ignoring_nullstate(head, std::forward<Symbol>(symbol), t, state.head);
        }

        template <typename Symbol, typename UnaryFunction>
            requires
            (
                statelessly_transducible_over<T, Symbol> &&
                statelessly_transducible_over<K, output_symbol_t<T, Symbol>> &&
                std::invocable<UnaryFunction, output_symbol_t<K, output_symbol_t<T, Symbol>>>
            )
        constexpr void operator () (Symbol && symbol, UnaryFunction && tape) const
        {
            auto t = make_tape(tail, tape);
            head(std::forward<Symbol>(symbol), t);
        }

        T head;
        K tail;
    };

    template <typename T, typename K, typename A>
        requires(transducible_over<T, A> && transducible_over<K, output_symbol_t<T, A>>)
    struct output_symbol<composite_t<T, K>, A>
    {
        using type = output_symbol_t<K, output_symbol_t<T, A>>;
    };

    /*!
        \~english
            \brief
                Initialize the composite state

            \param k
                A composite.
            \param type
                A tag containing the type of a symbol which is used to produce the initial state of
                the composite transducer or reduce kernel.

            \returns
                The initial state of the composite transducer or reduce kernel.

        \~russian
            \brief
                Инициализация состояния композита

            \param k
                Составной преобразователь или ядро свёртки..
            \param type
                Метка, содержащая тип символа, которым будет проинициализировано начальное
                состояние составного преобразователя или ядра свёртки.

            \returns
                Начальное состояние составного преобразователя или ядра свёртки.

        \~  \see composite_t
            \see stateful_over
            \see stateful_with
            \see type_t
            \see composite_state_t
            \see compose
     */
    template <typename T, typename K, typename A>
        requires(stateful_over<T, A> && stateful_over<K, output_symbol_t<T, A>>)
    constexpr auto initialize (const composite_t<T, K> & k, type_t<A> /* type */)
        -> composite_state_t<T, K, A>
    {
        return{initialize(k.head, type<A>), initialize(k.tail, type<output_symbol_t<T, A>>)};
    }

    template <typename T, typename K, typename A>
        requires(stateful_over<T, A>)
    constexpr auto initialize (const composite_t<T, K> & k, type_t<A> /* type */)
        -> composite_state_t<T, K, A>
    {
        return {initialize(k.head, type<A>), nullstate};
    }

    template <typename T, typename K, typename A>
        requires(stateful_over<K, output_symbol_t<T, A>>)
    constexpr auto initialize (const composite_t<T, K> & k, type_t<A> /* type */)
        -> composite_state_t<T, K, A>
    {
        return {nullstate, initialize(k.tail, type<output_symbol_t<T, A>>)};
    }

    /*!
        \~english
            \brief
                Check for finalizability (early stop) of a composite

            \details
                This function is defined if and only if one (or both) of the transducer `T` and the
                transducer or reduce kernel `K`, forming the composite `k`, are finalizable on the
                corresponding states, forming the composite state `s`.

                I.e. `is_final` is defined for `composite_t<T, K>` if and only if
                `is_final` is defined for `T` or `is_final` is defined for `K`.

            \param k
                The composite transducer or reduce kernel.
            \param s
                The state of the composite transducer or reduce kernel `k`.

            \tparam T, K
                Two transducers or a transducer and a reduce kernel, from which the composite `k`
                consists.
            \tparam A
                The type of the initial symbol from which the state `s` of the composite `k` has
                been initialized.

            \returns
                `true` if reducing is over, that is, the final state has been reached, starting
                from which the reduction process is undefined, and `false` otherwise.

        \~russian
            \brief
                Проверка композита на раннее завершение

            \details
                Данная функция определена тогда, и только тогда, когда один или оба компонента,
                образующих композит `k`, имеют свойство раннего завершения на соответствующих им
                состояниях, образующих составное состояние `s`.

                Говоря формально, `is_final` определена для `composite_t<T, K>`
                тогда, и только тогда, когда `is_final` определена для `T` или `is_final`
                определена для `K`.

            \param k
                Композит, т.е. составной преобразователь или ядро свёртки.
            \param s
                Состояние композита `k`.

            \tparam T, K
                Два преобразователя или преобразователь и ядро свёртки, образующие композит `k`.
            \tparam A
                Тип начального символа, которым было проинициализировано состояние `s`
                композита `k`.

            \returns
                `true`, если процесс свёртки окончен, то есть достигнуто финальное состояние,
                начиная с которого процесс свёртки неопределён, и `false` в противном случае.

        \~  \see composite_t
            \see composite_state_t
            \see finalizable_over
            \see finalizable_with
            \see compose
     */
    template <typename T, typename K, typename A>
        requires(finalizable_over<T, A> && finalizable_over<K, output_symbol_t<T, A>>)
    constexpr bool
        is_final (const composite_t<T, K> & k, const composite_state_t<T, K, A> & s)
    {
        return is_final(k.head, s.head) || is_final(k.tail, s.tail);
    }

    template <typename T, typename K, typename A>
        requires(finalizable_over<T, A>)
    constexpr bool
        is_final (const composite_t<T, K> & k, const composite_state_t<T, K, A> & s)
    {
        return is_final(k.head, s.head);
    }

    template <typename T, typename K, typename A>
        requires(finalizable_over<K, output_symbol_t<T, A>>)
    constexpr bool
        is_final (const composite_t<T, K> & k, const composite_state_t<T, K, A> & s)
    {
        return is_final(k.tail, s.tail);
    }

    /*!
        \~english
            \brief
                Emit stored value of a composite

            \details
                This function is defined if and only if one (or both) of the transducer `T` and the
                transducer or reduce kernel `K`, forming the composite `k`, are emitting on the
                corresponding states, forming the composite state `s`.

                I.e. `emit` is defined for `composite_t<T, K>` if and only if `emit`
                is defined for `T` or `emit` is defined for `K`.

            \note
                Transducer's `emit` and reduce kernel's `emit` have different signatures.

            \param k
                The composite.
            \param s
                State of the composite `k`.

            \tparam T, K
                Two transducers or a transducer and a reduce kernel, from which the composite `k`
                consists.
            \tparam A
                The type of the initial symbol from which the state `s` of the composite `k` has
                been initialized.

        \~russian
            \brief
                Высвободить запомненное значение состояния композита

            \details
                Данная функция определена тогда, и только тогда, когда один или оба компонента,
                образующих композит `k`, имеют свойство завершения по требованию на соответствующих
                им состояниях, образующих составное состояние `s`.

                Говоря формально, `emit` определена для `composite_t<T, K>` тогда, и
                только тогда, когда `emit` определена для `T` или `emit` определена для `K`.

            \note
                У преобразователя и ядра свёртки сигнатуры функции `emit` отличаются.

            \param k
                Композит, т.е. составной преобразователь или ядро свёртки.
            \param s
                Состояние композита `k`.

            \tparam T, K
                Два преобразователя или преобразователь и ядро свёртки, образующие композит `k`.
            \tparam A
                Тип начального символа, которым было проинициализировано состояние `s`
                композита `k`.

        \~  \see composite_t
            \see composite_state_t
            \see emitting_reducible_over
            \see emitting_trasducible_over
            \see compose
     */
    template <typename T, typename K, typename A>
        requires
        (
            emitting_transducible_over<T, A> &&
            emitting_reducible_over<K, output_symbol_t<T, A>>
        )
    constexpr auto
        emit (const composite_t<T, K> & k, composite_state_t<T, K, A> & s)
    {
        if (not is_finalizable_and_final(k.tail, s.tail))
        {
            auto t = make_tape(k.tail, s.tail);
            emit(k.head, t, s.head);
        }

        emit(k.tail, s.tail);
    }

    template <typename T, typename K, typename A>
        requires(emitting_transducible_over<T, A> && reducible_over<K, output_symbol_t<T, A>>)
    constexpr auto
        emit (const composite_t<T, K> & k, composite_state_t<T, K, A> & s)
    {
        if (not is_finalizable_and_final(k.tail, s.tail))
        {
            auto t = make_tape(k.tail, s.tail);
            emit(k.head, t, s.head);
        }
    }

    template <typename T, typename K, typename A>
        requires(emitting_reducible_over<K, output_symbol_t<T, A>>)
    constexpr auto
        emit (const composite_t<T, K> & k, composite_state_t<T, K, A> & s)
    {
        emit(k.tail, s.tail);
    }

    template <typename T, typename K, typename UnaryFunction, typename A>
        requires
        (
            emitting_transducible_over<T, A> &&
            emitting_transducible_over<K, output_symbol_t<T, A>>
        )
    constexpr auto
        emit (const composite_t<T, K> & k, UnaryFunction && tape, composite_state_t<T, K, A> & s)
    {
        if (not is_finalizable_and_final(k.tail, s.tail))
        {
            auto t = make_tape_ignoring_nullstate(k.tail, tape, s.tail);
            emit(k.head, t, s.head);
        }

        emit(k.tail, std::forward<UnaryFunction>(tape), s.tail);
    }

    template <typename T, typename K, typename UnaryFunction, typename A>
        requires(emitting_transducible_over<T, A> && transducible_over<K, output_symbol_t<T, A>>)
    constexpr auto
        emit (const composite_t<T, K> & k, UnaryFunction && tape, composite_state_t<T, K, A> & s)
    {
        if (not is_finalizable_and_final(k.tail, s.tail))
        {
            auto t = make_tape_ignoring_nullstate(k.tail, tape, s.tail);
            emit(k.head, t, s.head);
        }
    }

    template <typename T, typename K, typename UnaryFunction, typename A>
        requires(emitting_transducible_over<K, output_symbol_t<T, A>>)
    constexpr auto
        emit (const composite_t<T, K> & k, UnaryFunction && tape, composite_state_t<T, K, A> & s)
    {
        emit(k.tail, std::forward<UnaryFunction>(tape), s.tail);
    }
} // namespace proxima
