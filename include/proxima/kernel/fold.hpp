#pragma once

#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <concepts>
#include <type_traits>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                The classic reduce kernel

            \details
                Takes a symbol and applies a binary funcion object to that symbol and the current
                state of the reduce kernel. The result of this call becomes the current state of
                the reduce kernel.

            \tparam Value
                A type of the initial value and every current value of the reduce kernel.
            \tparam BinaryFunction
            \parblock
                The type of function object that will be applied to the current value and
                the next input symbol.
                The signature of the function object should be equivalent to the following:

                    R f (const A &, const B &);

                -   The signature does not need to have `const &`.
                -   The type `A` must be such that an object of type `Value` can be implicitly
                    converted to `A`.
                -   The type `B` must be such that an input symbol can be implicitly converted to
                    `B`.
                -   The type `R` must be such that an object of type `Value` can be assigned a
                    value of type `R`.
            \endparblock

        \~russian
            \brief
                Классическое ядро свёртки

            \details
                Принимает символ и применяет к этому символу и текущему состоянию двухместный
                функциональный объект. Результат этого вызова становится текущим состоянием ядра
                свёртки.

            \tparam Value
                Тип начального значения, к которому будет применяться свёртка. Каждое текущее
                значение состояния ядра свёртки тоже будет этого типа.
            \tparam BinaryFunction
            \parblock
                Тип функционального объекта, который будет применяться к текущему значению и
                следующему входному символу.
                Его сигнатура должна быть эквивалентна следующей:

                    R f (const A &, const B &);

                -   Квалификатор `const &` для аргументов непосредственно не требуется.
                -   Тип `A` должен быть таким, чтобы объект типа `Value` мог неявно преобразоваться
                    к объекту типа `A`.
                -   Тип `B` должен быть таким, чтобы символ со входной ленты мог неявно
                    преобразоваться к объекту типа `B`.
                -   Тип `R` должен быть таким, чтобы объекту типа `Value` можно было присвоить
                    объект типа `R`.
            \endparblock

        \~  \see fold
     */
    template <typename Value, typename BinaryFunction>
    struct fold_t
    {
        template <typename Symbol, typename State>
            requires(std::is_invocable_r_v<Value, BinaryFunction, Value, Symbol>)
        constexpr void operator () (Symbol && s, State & st) const
        {
            auto result = fn(std::move(value(st)), std::forward<Symbol>(s));
            value(st) = std::move(result);
        }

        Value init;
        BinaryFunction fn;
    };

    template <typename Value, typename BinaryFunction, typename T>
    constexpr auto initialize (const fold_t<Value, BinaryFunction> & f, type_t<T>)
    {
        return simple_state_t<Value, T>{f.init};
    }

    /*!
        \~english
            \brief
                Creates a classic reduce kernel

            \param v
                The initial value.
            \param fn
                A functional object that will be applied to the current value and the next
                input symbol.

            \details
                Usage example:

                \code{.cpp}
                const auto v = std::vector<int>{...};
                const auto sum = proxima::reduce(v, proxima::fold(0, std::plus<>{}));
                \endcode

            \returns
                An instance of `fold_t` with a specified initial value and functional object.

        \~russian
            \brief
                Создаёт классическое ядро свёртки

            \param v
                Начальное значение, к которому будет применяться свёртка.
            \param fn
                Функциональный объект, который будет применяться к текущему значению и следующему
                входному символу.

            \details
                Пример использования:

                \code{.cpp}
                const auto v = std::vector<int>{...};
                const auto sum = proxima::reduce(v, proxima::fold(0, std::plus<>{}));
                \endcode

            \returns
                Экземпляр простого ядра свёртки с заданными начальным значением и функциональным
                объектом.

        \~  \see fold_t
     */
    template <typename BinaryFunction, typename Value>
    constexpr auto fold (Value && v, BinaryFunction && fn)
        -> fold_t<std::decay_t<Value>, std::decay_t<BinaryFunction>>
    {
        return {std::forward<Value>(v), std::forward<BinaryFunction>(fn)};
    }
}
