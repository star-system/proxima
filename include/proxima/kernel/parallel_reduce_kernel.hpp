#pragma once

#include <proxima/concept/emitting_reducible_over.hpp>
#include <proxima/concept/finalizable_over.hpp>
#include <proxima/state/parallel_state.hpp>
#include <proxima/type_traits/state_of.hpp>
#include <proxima/utility/is_finalizable_and_final.hpp>

#include <memory>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                Parallel reduce kernel

            \details
                Mediates between the input tape and the wrapped reduce kernel, which is calculated
                asynchronously through a thread pool.

                Behaves just like any other reduce kernel: has a state and a defined `initialize`
                function that creates this state, as well as the call operator that takes the
                approproate symbol and state.

                Parallel reduce kernel takes over the properties of the wrapped reduce kernel.
                For example, if wrapped reduce kernel is finalizable, then parallel reduce kernel
                will also be finalizable.

            \tparam K
                The type of the wrapped reduce kernel, which will be executed in a separate thread.

        \~russian
            \brief
                Параллельное ядро свёртки

            \details
                Осуществляет посредничество между входной лентой и обёрнутым ядром свёртки, которое
                вычисляется асинхронно посредством пула потоков.

                Ведёт себя так же, как и любое другое ядро свёртки: имеет состояние и определённую
                функцию `initialize`, создающую это состояние, а также оператор вызова, принимающий
                соответствующие символ и состояние.

                Параллельное ядро свёртки перенимает свойства ядра свёртки, из которого было
                создано. Например, если оно имеет раннее завершение, то полученное параллельное ядро
                свёртки тоже будет иметь раннее завершение.

            \tparam K
                Обёрнутое ядро свёртки, которое будет вычисляться в отдельном потоке.

        \~  \see parallel_state_t
            \see pipe_t
            \see pipe
     */
    template <typename K>
    struct parallel_reduce_kernel_t
    {
        /*!
            \~english
                \brief
                    Reads a symbol from the input tape and handles it asynchronously

                \details
                    If the thread pool is not stopped and its queue is not closed, the following
                    asynchronous task is set:
                    -   If the kernel is finalizable and has reached the final state, then the
                        thread pool is signaled to stop;
                    -   Otherwise, the next reduction step of the wrapped kernel is performed
                        with its current state and the received symbol.

                \param symbol
                    The next symbol to be processed asynchronously.
                \param state
                    The state of the parallel reduce kernel.

            \~russian
                \brief
                    Считывает символ с ленты и обрабатывает его асинхронно

                \details
                    Если пул потоков не остановлен и его очередь не закрыта, то ставится следующая
                    асинхронная задача:
                    -   Если ядро финализируемо и достигло финального состояния, то пул потоков
                        останавливается;
                    -   В противном случае производится очередной шаг свёртки обёрнутого ядра
                        с его текущим состоянием и полученным символом.

                \param symbol
                    Очередной символ, который будет обработан асинхронно.
                \param state
                    Состояние параллельного ядра свёртки.

            \~  \see parallel_state_t
         */
        template <typename A, typename S>
        void operator () (A && symbol, S & state) const
        {
            state.pool->try_post
            ([
                & kernel = wrapped_kernel,
                symbol = std::forward<A>(symbol),
                & state = *state.wrapped_state,
                & pool = *state.pool
            ] () mutable
            {
                if (not is_finalizable_and_final(kernel, state))
                {
                    kernel(std::move(symbol), state);
                }
                else
                {
                    pool.stop();
                }
            });
        }

        K wrapped_kernel;
    };

    /*!
        \~english
            \brief
                The parallel reduce kernel state initialization

            \details
                Initializes the state of the wrapped reduce kernel.
                Creates a thread pool which will handle the wrapped reduce kernel.

            \tparam K
                The type of the wrapped reduce kernel, which will be executed in a thread pool.
            \tparam A
                    The type of symbols on the input tape.

            \returns
                The state of the parallel reduce kernel.

        \~russian
            \brief
                Инициализация начального состояния параллельного ядра свёртки

            \details
                Инициализирует состояние обёрнутого ядра свёртки.
                Создаёт пул потоков, в котором будет производиться обработка этого ядра свёртки.

            \tparam K
                Обёрнутое ядро свёртки, которое будет вычисляться асинхронно.
            \tparam A
                Тип символов, которые будут поступать на входной ленте.

            \returns
                Состояние параллельного ядра свёртки.

        \~  \see parallel_state_t
            \see parallel_state_synchronization_t
            \see parallel_reduce_kernel_worker
            \see parallel_reduce_kernel_t
     */
    template <typename K, typename A>
    auto initialize (const parallel_reduce_kernel_t<K> & k, type_t<A>)
    {
        return
            parallel_state_t<K, A>
            {
                std::make_unique<state_of_t<K, A>>(initialize(k.wrapped_kernel, type<A>)),
                std::make_unique<thread_pool>(1)
            };
    }

    /*!
        \~english
            \brief
                Check for early stop of the parallel reduce kernel

            \details
                This function is defined if and only if `K` is finalizable on the corresponding
                state.

                I.e. `is_final` is defined for `parallel_reduce_kernel_t<K>` if and only if
                `is_final` is defined for `K`.

            \param k
                The parallel reduce kernel.
            \param s
                The state of the parallel reduce kernel `k`.

            \tparam K
                The wrapped reduce kernel which is executed in a separate thread.
            \tparam A
                The type of the symbol from which the state of the wrapped reduce kernel `K` has
                been initialized.

            \returns
                The result of `is_final` function applied to the wrapped reduce kernel.

        \~russian
            \brief
                Проверка на раннее завершение параллельного ядра свёртки

            \details
                Данная функция определена тогда, и только тогда, когда обёрнутое ядро свёртки `K`
                имеет свойство раннего завершения.

                Говоря формально, `is_final` определена для `parallel_reduce_kernel_t<K>`
                тогда, и только тогда, когда `is_final` определена для `K`.

            \param k
                Параллельное ядро свёртки.
            \param s
                Состояние параллельного ядра свёртки `k`.

            \tparam K
                Обёрнутое ядро свёртки, выполняемое в отдельном потоке.
            \tparam A
                Тип начального символа, которым было проинициализировано состояние обёрнутого
                ядра свёртки `K`.

            \returns
                Результат функции `is_final`, применённой к обёрнутому ядру свёртки.

        \~  \see parallel_reduce_kernel_t
            \see parallel_state_t
            \see finalizable_over
     */
    template <typename K, typename A>
        requires finalizable_over<K, A>
    auto is_final (const parallel_reduce_kernel_t<K> & k, const parallel_state_t<K, A> & s)
    {
        // Есть подозрения, что здесь может происходить гонка за состоянием ядра свёртки.
        // Исследовать возможность ложноположительных срабатываний, сигнализирующих о финальности
        // состояний.
        return is_final(k.wrapped_kernel, *s.wrapped_state);
    }

    /*!
        \~english
            \brief
                Emit the value of the parallel reduce kernel

            \details
                For the parallel reduce kernel, getting into this function means that all symbols
                from the input tape have already been read, so it remains only to wait for the
                completion of all asynchronous tasks in the thread pool.

                Also if the wrapped reduce kernel is emitting, its state needs to be emitted.

            \param k
                The parallel reduce kernel.
            \param s
                The state of the parallel reduce kernel `k`.

            \tparam K
                The wrapped reduce kernel which is executed in a separate thread.
            \tparam A
                The type of the symbol from which the state of the wrapped reduce kernel `K` has
                been initialized.

            \post
                The state `s` can't be used for reduction anymore. The value of the state, of
                course, still can be extracted with a `value` function.

        \~russian
            \brief
                Высвободить значение параллельного ядра свёртки

            \details
                Для параллельного ядра свёртки попадание в эту функцию означает, что все символы с
                входной ленты уже были прочитаны, поэтому осталось только дождаться выполнения всех
                асинхронных задач в пуле потоков.

                При этом, если обёрнутое ядро свёртки обладает свойством завершения по требованию,
                то нужно высвободить его состояние.

            \param k
                Параллельное ядро свёртки.
            \param s
                Состояние параллельного ядра свёртки `k`.

            \tparam K
                Обёрнутое ядро свёртки, выполняемое в отдельном потоке.
            \tparam A
                Тип начального символа, которым было проинициализировано состояние обёрнутого
                ядра свёртки `K`.

            \post
                Состояние `s` больше не может быть использовано для вычисления свёртки. При этом,
                разумеется, из него можно достать значение с помощью функции `value`.

        \~  \see parallel_reduce_kernel_t
            \see parallel_state_t
            \see emitting_reducible_over
     */
    template <typename K, typename A>
    void emit (const parallel_reduce_kernel_t<K> & k, parallel_state_t<K, A> & s)
    {
        // Проверить гипотезу о том, что можно не завершать потоки в пуле, а только дождаться
        // завершения текущих задач.
        // Разумеется, для этого потребуется какой-то отдельный интерфейс.
        s.pool->wait();

        if constexpr (emitting_reducible_over<K, A>)
        {
            emit(k.wrapped_kernel, *s.wrapped_state);
        }
    }
} // namespace proxima
