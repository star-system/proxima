#pragma once

#include <proxima/state/simple_state.hpp>
#include <proxima/type.hpp>

#include <type_traits>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                The product reduce kernel

            \details
                Counts the product of elements that are read from the input tape.

        \~russian
            \brief
                Перемножающее ядро свёртки

            \details
                Вычисляет произведение символов, считанных со входной ленты.
     */
    struct product_t
    {
        /*!
            \~english
                \brief
                    Reads a symbol from the input tape and multiplies the product by it

                \details
                    Performs the `operator *` on the current value of the state and the symbol from
                    the input tape. The result is stored in the state value.

                \param symbol
                    The symbol from the input tape.
                \param state
                    The current state of the reduce kernel. Contains product of the elements so far.

            \~russian
                \brief
                    Считывает символ со входной ленты и домножает произведение на него

                \details
                    Выполняет вызов оператора умножения над текущим значением состояния и символом,
                    считанным со входной ленты. Результат сохраняется в значении состояния.

                \param symbol
                    Символ со входной ленты.
                \param state
                    Текущее состояние ядра свёртки. Содержит текущее произведение элементов.
         */
        template <typename A, typename S>
        constexpr void operator () (A && symbol, S & state) const
        {
            value(state) = value(state) * std::forward<A>(symbol);
        }
    };

    /*!
        \~english
            \brief
                A convenience instrument for using the product reduce kernel

            \details
                Usage examples:

                \code{.cpp}
                auto product = proxima::reduce(sequence, proxima::product);
                \endcode

                \code{.cpp}
                auto product_of_unique_elements =
                    proxima::compose
                    (
                        proxima::unique,
                        proxima::product
                    );
                auto product = proxima::reduce(sequence, product_of_unique_elements);
                \endcode

            \returns
                An instance of `product_t` reduce kernel.

        \~russian
            \brief
                Инструмент для удобного создания перемножающего ядра свёртки

            \details
                Примеры использования:

                \code{.cpp}
                auto product = proxima::reduce(sequence, proxima::product);
                \endcode

                \code{.cpp}
                auto product_of_unique_elements =
                    proxima::compose
                    (
                        proxima::unique,
                        proxima::product
                    );
                auto product = proxima::reduce(sequence, product_of_unique_elements);
                \endcode

            \returns
                Экземпляр ядра свёртки `product_t`.

        \~  \see product_t
            \see compose
            \see reduce
     */
    constexpr auto product = product_t{};

    /*!
        \~english
            \brief
                Initializes the initial state of the product reduce kernel

            \tparam Arithmetic
                The type of symbols that will be multiplied. Must meed the requirements of
                Arithmetic concept.

            \returns
                A simple state initialized with `Arithmetic{1}`.

        \~russian
            \brief
                Инициализация начального состояния перемножающего ядра свёртки

            \tparam Arithmetic
                Тип символов, которые будут перемножаться. Обязан быть арифметическим.

            \returns
                Простое состояние, инициализированное значением `Arithmetic{1}`.

        \~  \see simple_state_t
            \see type_t
            \see product_t
     */
    template <typename Arithmetic>
    constexpr auto initialize (const product_t &, type_t<Arithmetic>)
    {
        static_assert(std::is_arithmetic_v<Arithmetic>);
        return simple_state_t<Arithmetic>{Arithmetic{1}};
    }
}
