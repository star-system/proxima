#pragma once

#include <proxima/concept/reducible_with.hpp>
#include <proxima/concept/statefully_transducible_with.hpp>
#include <proxima/concept/statelessly_transducible_over.hpp>
#include <proxima/state/nullstate.hpp>

#include <concepts>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                A helper class to wrap a transducer (or a reduce kernel) to an output tape

            \details
                When a transducer passes the result of its work further down the chain, it does not
                know a type of the transducer (of the reduce kernel) to which it is passing data.
                It just writes data on the tape, and does not worry about who reads it.
                This class is designed to wrap the transducer together with its tape (or the
                reduce kernel together with its state) in an independent object, which is enough to
                pass the value to, in other words, "write the data on the tape".

            \tparam TK
                Type of the wrapped transducer (ot the reduce kernel).
            \tparam FS
                Type of the tape which the wrapped transducer will write on (or type of the state of
                the wrapped reduce kernel).

        \~russian
            \brief
                Вспомогательный класс для заворачивания преобразователя (или ядра свёртки) в
                выходную ленту

            \details
                Когда преобразователь отдаёт результат своей работы далее по цепочке, он не знает
                конкретного типа преобразователя (или ядра свёртки), которому он передаёт данные. Он
                знает только то, что пишет данные на ленту, а кто читает с этой ленты — не важно.
                Данный класс предназначен именно для того, чтобы завернуть преобразователь с его
                лентой (или ядро свёртки вместе с его состоянием) в самостоятельный объект, которому
                достаточно отдать значение, то есть "записать на ленту".

            \tparam TK
                Тип заворачиваемого преобразователя (или ядра свёртки).
            \tparam FS
                Тип ленты, на которую будет записывать заворачиваемый преобразователь (или тип
                состояния заворачиваемого ядра свёртки).

        \~
            \see make_tape
            \see reducible_with
            \see statelessly_transducible_over
     */
    template <typename TK, typename FS>
    struct tape_t
    {
        template <typename A>
            requires reducible_with<TK, A, FS>
        constexpr void operator () (A && symbol)
        {
            transducer_or_kernel(std::forward<A>(symbol), function_or_state);
        }

        template <typename A>
            requires(statelessly_transducible_over<TK, A>)
        constexpr void operator () (A && symbol)
        {
            transducer_or_kernel(std::forward<A>(symbol), function_or_state);
        }

        const TK & transducer_or_kernel;
        FS & function_or_state;
    };

    /*!
        \~english
            \brief
                A helper class to wrap a stateful transducer to a tape

            \details
                A wrapper similar to tape_t, but only for a case of a stateful transducer.

            \tparam T
                Type of the wrapped transducer.
            \tparam F
                Type of the tape which the wrapped transducer will write on.
            \tparam S
                Type of the state of the wrapped transducer.

        \~russian
            \brief
                Заворачивание преобразователя с состоянием в ленту

            \details
                Обёртка, аналогичная `tape_t`, но только для преобразователя с состоянием.

            \tparam T
                Тип заворачиваемого преобразователя.
            \tparam F
                Тип ленты, на которую будет записывать заворачиваемый преобразователь.
            \tparam S
                Тип состояния заворачиваемого преобразователя.

        \~
            \see tape_t
            \see make_tape
            \see statefully_transducible_over
     */
    template <typename T, typename F, typename S>
    struct stateful_transducer_tape_t
    {
        template <typename A>
            requires(statefully_transducible_with<T, A, S>)
        constexpr void operator () (A && symbol)
        {
            transducer(std::forward<A>(symbol), tape, state);
        }

        const T & transducer;
        F & tape;
        S & state;
    };

    template <typename TK, typename FS>
    constexpr auto make_tape (const TK & transducer_or_kernel, FS & function_or_state)
    {
        return tape_t<TK, FS>{transducer_or_kernel, function_or_state};
    }

    template <typename T, typename UnaryFunction, typename S>
    constexpr auto
        make_tape_ignoring_nullstate (const T & transducer, UnaryFunction & tape, S & state)
    {
        return stateful_transducer_tape_t<T, UnaryFunction, S>{transducer, tape, state};
    }

    template <typename T, typename UnaryFunction>
    constexpr auto
        make_tape_ignoring_nullstate (const T & transducer, UnaryFunction & tape, nullstate_t)
    {
        return tape_t<T, UnaryFunction>{transducer, tape};
    }
} // namespace proxima
