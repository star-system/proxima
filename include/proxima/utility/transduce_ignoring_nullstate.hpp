#pragma once

#include <proxima/concept/statefully_transducible_with.hpp>
#include <proxima/concept/statelessly_transducible_over.hpp>
#include <proxima/state/nullstate.hpp>

#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                Generalized transducing

            \detail
                Applies the `transducer`. If the `state` is `nullstate`, then it is ignored.

            \tparam T
                Type of the `transducer` to apply.
            \tparam A
                Type of `symbol` to apply `transducer` to.
            \tparam UnaryFunction
                Type of the output `tape`.
            \tparam S
                Type of the `state` of the `transducer`.

        \~russian
            \brief
                Обобщённое применение преобразователя

            \detail
                Применяет преобразователь. Если состояние пустое (`nullstate`), то оно просто
                игнорируется.

            \tparam T
                Тип преобразователя, который требуется применить.
            \tparam A
                Тип символа, к которому нужно применить преобразование.
            \tparam UnaryFunction
                Тип выходной ленты.
            \tparam S
                Тип состояния, соответствующего преобразователю.

        \~  \see statefully_transducible_with
        \~  \see statelessly_transducible_with
     */
    template <typename T, typename A, typename UnaryFunction, typename S>
        requires(statefully_transducible_with<T, A, S>)
    constexpr void
        transduce_ignoring_nullstate
        (
            T && transducer,
            A && symbol,
            UnaryFunction && tape,
            S & state
        )
    {
        std::forward<T>(transducer)
        (
            std::forward<A>(symbol),
            std::forward<UnaryFunction>(tape),
            state
        );
    }

    template <typename T, typename A, typename UnaryFunction>
        requires(statelessly_transducible_over<T, A>)
    constexpr void
        transduce_ignoring_nullstate
        (
            T && transducer,
            A && symbol,
            UnaryFunction && tape,
            nullstate_t
        )
    {
        std::forward<T>(transducer)(std::forward<A>(symbol), std::forward<UnaryFunction>(tape));
    }
} // namespace proxima
