#pragma once

#include <proxima/concept/finalizable_with.hpp>

#include <type_traits>

namespace proxima
{
    /*!
        \~english
            \brief
                Generalized finality check

            \tparam K
                The type of the reduce kernel which is required to be checked for finality.
            \tparam S
                The type of the state of `kernel`.

            \returns
                `true` if and only if `K` is finalizable over the state `S` and `state` is final.
                `false` in any other case.

        \~russian
            \brief
                Обобщённая проверка на раннее завершение

            \tparam K
                Тип ядра свёртки, которое требуется проверить на раннее завершение.
            \tparam S
                Тип состояния, соответствующего ядру свёртки `kernel`.

            \returns
                `true` в том, и только в том случае, если `K` обладает свойством раннего завершения
                над состоянием `S`, и при этом состояние `state` является финальным. Во всех
                остальных случаях возвращается `false`.

        \~  \see finalizable_with
            \see is_final
     */
    template <typename K, typename S>
        requires finalizable_with<K, S>
    constexpr auto is_finalizable_and_final (const K & kernel, const S & state)
    {
        return is_final(kernel, state);
    }

    template <typename K, typename S>
    constexpr auto is_finalizable_and_final (const K &, const S &)
    {
        return false;
    }
}
