#pragma once

#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                The state of a single transducer or a reduce kernel

            \tparam V
                The type of the contained value.
            \tparam A
                The type of the symbol from which the state was initialized.
                It is equal to the type of the value by default.

        \~russian
            \brief
                Состояние одиночного преобразователя или ядра свёртки

            \tparam V
                Тип значения, которое хранится внутри состояния.
            \tparam A
                Тип символа, которым проинициализировано состояние.
                По-умолчанию равен типу значения.
     */
    template <typename V, typename A = V>
    struct simple_state_t
    {
        using symbol_type = A;
        using value_type = V;

        value_type value;
    };

    template <typename V, typename A>
    constexpr V & value (simple_state_t<V, A> & state)
    {
        return state.value;
    }

    template <typename V, typename A>
    constexpr const V & value (const simple_state_t<V, A> & state)
    {
        return state.value;
    }

    template <typename V, typename A>
    constexpr V value (simple_state_t<V, A> && state)
    {
        return std::move(state).value;
    }
} // namespace proxima
