#pragma once

#include <proxima/concept/stateful_over.hpp>
#include <proxima/state/nullstate.hpp>
#include <proxima/type_traits/output_symbol.hpp>
#include <proxima/type_traits/state_of.hpp>
#include <proxima/type_traits/state_value.hpp>

#include <type_traits>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                Composite state

            \details
                Represents a "matryoshka" of states, with the outer part of the "matryoshka"
                corresponding to a transducer `T` (the outer part of the "matryoshka" of the
                composite) and the inner part of the "matryoshka" corresponding to a
                transducer or reduce kernel `K` (the inner part of the "matryoshka" of the
                composite).

                An empty state (`nullstate`) corresponds to the stateless transducers.

            \tparam T
                Transducer forming the outer part of the "matryoshka" of the composite.
            \tparam K
                Transducer or reduce kernel forming the inner part of the "matryoshka" of the
                composite.
            \tparam A
                Type of the symbol from which the state was initialized.

        \~russian
            \brief
                Составное состояние

            \details
                Представляет собой "матрёшку" состояний, в которой внешняя часть "матрёшки"
                соответствует некоему преобразователю `T` (внешней части "матрёшки" композита),
                а внутренняя часть "матрёшки" — некоему преобразователю или ядру свёртки `K`
                (внутренней части "матрёшки" композита).

                Преобразователям, у которых нет состояний (то есть не определена функция
                `initialize`), соответствует пустое состояние (`nullstate`).

            \tparam T
                Преобразователь представляющий внешнюю часть "матрёшки" составного ядра свёртки.
            \tparam K
                Преобразователь или ядро свёртки, представляющий внутрюннюю часть "матрёшки"
                композита.
            \tparam A
                Тип символа, из которого было проинициализировано состояние.

        \~  \see initialize
            \see stateful_over
            \see nullstate
            \see composite_t
     */
    template <typename T, typename K, typename A>
    struct composite_state_t
    {
        using head_type =
            typename std::conditional_t
            <
                stateful_over<T, A>,
                state_of<T, A>,
                std::type_identity<nullstate_t>
            >
            ::type;
        using tail_type =
            typename std::conditional_t
            <
                stateful_over<K, output_symbol_t<T, A>>,
                state_of<K, output_symbol_t<T, A>>,
                std::type_identity<nullstate_t>
            >
            ::type;

        using value_type =
            typename std::conditional_t
            <
                std::is_same_v<tail_type, nullstate_t>,
                std::type_identity<void>,
                state_value<tail_type>
            >
            ::type;
        using symbol_type = A;

        head_type head;
        tail_type tail;
    };

    /*!
        \~english
            \brief
                Access to value of composite reduce kernel state

            \details
                The result of each reduction is stored in the state of the reduce kernel.
                The purpose of this function is to obtain that value.

            \param state
                The state of the composite reduce kernel.

            \returns
                Value of the innermost part of the "matryoshka" (the smallest one) of the state.
                If composite is not a reduce kernel, then result is undefined.

        \~russian
            \brief
                Доступ к значению состояния составного ядра свёртки

            \details
                Результат каждого вычисления свёртки хранится в состоянии этого ядра свёртки.
                Данная функция нужна для того, чтобы получить это значение.

            \param state
                Состояние составного ядра свёртки.

            \returns
                Значение состояния наименьшей из "матрёшек" (то есть наиболее глубоко вложенной),
                составляющих состояние.
                Если композит является преобразователем, а не ядром свёртки, то результат функции
                не определён.

        \~  \see composite_state_t
            \see composite_t
     */
    template <typename T, typename K, typename A>
    constexpr auto & value (composite_state_t<T, K, A> & state)
    {
        return value(state.tail);
    }

    template <typename T, typename K, typename A>
    constexpr const auto & value (const composite_state_t<T, K, A> & state)
    {
        return value(state.tail);
    }

    template <typename T, typename K, typename A>
    constexpr auto value (composite_state_t<T, K, A> && state)
    {
        return value(std::move(state).tail);
    }
} // namespace proxima
