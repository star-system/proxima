#pragma once

#include <proxima/composite.hpp>

#include <type_traits>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                Composition of transducers and/or reduce kernel

            \details
                Receives several transducers and, possibly, one, and only one reduce kernel standing
                in the last position.

                    xs = t_1, ..., t_n[, k]

                where each `t_i` is a transducer, `k` is a reduce kernel.

                Composition is associative.

                    compose(t_1, compose(t_2, k)) = compose(compose(t_1, t_2), k)

            \param xs
                Transducers and, possibly, a reduce kernel.

            \returns
                If a reduce kernel is present, then returns a composite reduce kernel.
                If there are only transducers in the composition, then returns a composite
                transducer.

        \~russian
            \brief
                Композиция преобразователей и/или ядра свёртки

            \details
                Принимает несколько преобразователей и, возможно, одно, и только одно ядро свёртки,
                стоящее на последнем месте.

                    xs = t_1, ..., t_n[, k]

                где каждое `t_i` — преобразователь, `k` — ядро свёртки.

                Композиция ассоциативна.

                    compose(t_1, compose(t_2, k)) = compose(compose(t_1, t_2), k)

            \param xs
                Преобразователи и, возможно, ядро свёртки.

            \returns
                Если в конце стоит ядро свёртки, то составное ядро свёртки. Если в композиции
                участвуют только преобразователи, то составной преобразователь.

        \~  \see composite_t
     */
    template <typename... Ts>
    constexpr auto compose (Ts &&... xs);

    template <typename K>
    constexpr auto compose (K && kernel)
    {
        return std::forward<K>(kernel);
    }

    template <typename Head, typename... Tail>
    constexpr auto compose (Head && head, Tail &&... tail)
    {
        using transducer_type = std::decay_t<Head>;
        using kernel_type = decltype(compose(std::forward<Tail>(tail)...));
        return
            composite_t<transducer_type, kernel_type>
            {
                std::forward<Head>(head),
                compose(std::forward<Tail>(tail)...)
            };
    }
} // namespace proxima
