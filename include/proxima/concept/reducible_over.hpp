#pragma once

#include <proxima/concept/reducible_with.hpp>
#include <proxima/type_traits/state_of.hpp>

namespace proxima
{
    /*!
        \~english
            \brief
                Specifies that a kernel is reducible over a symbol

            \tparam K
                Potential reduce kernel type.
            \tparam A
                The input symbol type.

            \returns
                `reducible_with<K, A, state_of_t<K, A>>`.

        \~russian
            \brief
                Концепция свёртки ядра над символом

            \tparam K
                Тип потенциального ядра свёртки.
            \tparam A
                Тип входного символа.

            \returns
                `reducible_with<K, A, state_of_t<K, A>>`.

        \~  \see state_of_t
     */
    template <typename K, typename A>
    concept reducible_over = reducible_with<K, A, state_of_t<K, A>>;
} // namespace proxima
