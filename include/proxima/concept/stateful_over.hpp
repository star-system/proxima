#pragma once

#include <proxima/type_traits/state_of.hpp>

namespace proxima
{
    /*!
        \~english
            \brief
                Specifies that a transducer or a reduce kernel has state

            \details
                A transducer or a reduce kernel `K` is considered stateful over the symbol `A`
                if the following function is defined:

                    initialize: K × A -> S,

                where `K` is the reduce kernel or the transducer type, `A` is the symbol type,
                `S` is the state type.

            \tparam K
                The transducer or the reduce kernel type.
            \tparam A
                The symbol type.

            \returns
                `true` if `initialize` is defined and `false` otherwise.

        \~russian
            \brief
                Ядро свёртки или преобразователь с состоянием

            \details
                Говорят, что ядро свёртки или преобразователь `K` имеет состояние над
                символом `A`, если для него определена функция

                    initialize: K × A -> S,

                где `K` — тип ядра свёртки или преобразователя, `A` — тип символа,
                `S` — тип состояния.

            \tparam K
                Тип ядра свёртки или преобразователя.
            \tparam A
                Тип символа.

            \returns
                `true`, если функция `initialize` определена, и `false` в противном случае.

        \~  \see initialize
            \see state_of_t
            \see stateless
     */
    template <typename K, typename A>
    concept stateful_over = requires {typename state_of_t<K, A>;};
} // namespace proxima
