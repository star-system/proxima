#pragma once

#include <proxima/concept/stateless_over.hpp>
#include <proxima/type_traits/detail/black_hole.hpp>

#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                Specifies a stateless transducer

            \details
                Class `T` considered statelessly transducible over symbol `A` if `state_of_t<T, A>`
                is undefined and the following member function is defined:

                    T: A × Tp -> void,

                where `A` is the symbol type, `Tp` is the output tape type.

                Type of the tape does not matter. It matters only that it consumes the output of
                the transducer. Therefore, the check uses an "omnivorous" tape, and the tape is
                not exposed in the arguments of the metafunction.

            \tparam T
                Potential stateless transducer type.
            \tparam A
                Symbol type.

            \returns
                `true` if `state_of_t<T, A>` is undefined and `T(A, Tp)` call is defined.
                Otherwise returns `false`.

        \~russian
            \brief
                Преобразователь без состояния

            \details
                Говорят, что класс `T` обладает операцией преобразования без состояния над
                символом `A`, если не определено состояние `state_of_t<T, A>` и определена
                функция-член

                    T: A × Tp -> void,

                где `A` — тип символа, `Tp` — тип выходной ленты.

                Тип ленты не имеет значения. Важно только то, что на неё записывается символ,
                получаемый с выхода преобразователя. Поэтому в проверке используется "всеядная"
                лента, а в аргументах метафункции она и вовсе отсутствует.

            \tparam T
                Тип потенциального преобразователя без состояния.
            \tparam A
                Тип символа.

            \returns
                `true` в случае, если не определена метафункция `state_of_t<T, A>` и
                определён вызов `T(A, Tp)`. В противном случае возвращает `false`.

        \~  \see stateless_over
     */
    template <typename T, typename A>
    concept statelessly_transducible_over =
        stateless_over<T, A> &&
        requires (T && t, A && a, detail::black_hole_t bh)
        {
            std::forward<T>(t)(std::forward<A>(a), bh);
        };
} // namespace proxima
