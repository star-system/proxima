#pragma once

#include <concepts>

namespace proxima
{
    /*!
        \~english
            \brief
                Specifies that a transducer or a reduce kernel is finalizable with a given state

            \details
                A transducer or a reduce kernel is considered finalizable if the following function
                is defined:

                    is_final: K × S -> bool,

                where `K` is the reduce kernel or the transducer type, `S` is the state type.

            \tparam K
                The transducer or the reduce kernel type.
            \tparam S
                The type of the state.

            \returns
                `true` if `is_final` is defined and `false` otherwise.

        \~russian
            \brief
                Определяет финализируемость преобразователя или ядра свёртки с состоянием

            \details
                Ядро свёртки (или преобразователь) финализируемо, если определена функция

                    is_final: K × S -> bool,

                где `K` — тип преобразователя или ядра свёртки, `S` — тип состояния.

            \tparam K
                Тип преобразователя или ядра свёртки.
            \tparam S
                Тип состояния.

            \returns
                `true` в случае, если функция `is_final` определена, и `false` в противном случае.

        \~  \see finalizable_over
            \see is_final
     */
    template <typename K, typename S>
    concept finalizable_with =
        requires (const K & k, const S & s)
        {
            {is_final(k, s)} -> std::convertible_to<bool>;
        };
} // namespace proxima
