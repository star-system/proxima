#pragma once

#include <proxima/concept/emitting_transducible_with.hpp>
#include <proxima/type_traits/state_of.hpp>

namespace proxima
{
    /*!
        \~english
            \brief
                Specifies an emitting transducer over a symbol

            \tparam T
                Potential emitting transducer type.
            \tparam A
                Type of symbol from which state of `T` should be initialized.

            \returns
                `emitting_transducible_with<T, A, state_of_t<T, A>>`.

        \~russian
            \brief
                Определяет преобразователь над символом с завершением по требованию

            \tparam T
                Тип потенциального преобразователя с завершением по требованию.
            \tparam A
                Тип инициализирующего символа для состояния преобразователя.

            \returns
                `emitting_transducible_with<T, A, state_of_t<T, A>>`.

        \~  \see emitting_transducible_with
            \see state_of_t
     */
    template <typename T, typename A>
    concept emitting_transducible_over = emitting_transducible_with<T, A, state_of_t<T, A>>;
} // namespace proxima
