#pragma once

#include <proxima/concept/statefully_transducible_with.hpp>
#include <proxima/type_traits/detail/black_hole.hpp>

namespace proxima
{
    /*!
        \~english
            \brief
                Specifies an emitting transducer

            \details
                The transducer `T` is considered emitting with the state `S` over the symbol `A` if
                `T` is statefully transducible with the state `S` over the symbol `A` and
                the following function is defined:

                    void emit(const T &, Tp &, S &),

                where `T` is the transducer type, `Tp` is the tape type, `S` is the state type.

                Type of the tape does not matter. It matters only that the tape consumes the output
                symbol of the transducer. Therefore, the check uses an "omnivorous" tape, and the
                tape is not exposed in the arguments of the metafunction.

            \tparam T
                Potential emitting transducer type.
            \tparam A
                Symbol type.
            \tparam S
                State type.

            \returns
                `true` if `statefully_transducible_with<T, A, S>` is `true` and the
                appropriate `emit` function is defined. Otherwise returns `false`.

        \~russian
            \brief
                Определяет преобразователь с завершением по требованию

            \details
                Говорят, что класс `T` обладает операцией преобразования с завершением по требованию
                с состоянием `S` над символом `A`, если он обладает операцией преобразования с
                состоянием `S` над символом `A`, а также для него определена функция

                    void emit(const T &, Tp &, S &),

                где `T` — тип преобразователя, `Tp` — тип ленты, `S` — тип состояния.

                Тип ленты значения не имеет. Важно лишь то, что она принимает выход
                преобразователя. Поэтому в проверке используется "всеядная" лента, и в аргументы
                метафункции лента не выставляется.

            \tparam T
                Тип преобразователя.
            \tparam A
                Тип символа.
            \tparam S
                Тип состояния.

            \returns
                `true`, если истинно `statefully_transducible_with<T, A, S>` и определена
                функция `emit`, и `false` во всех остальных случаях.

        \~  \see statefully_transducible_with
            \see emit
     */
    template <typename T, typename A, typename S>
    concept emitting_transducible_with =
        statefully_transducible_with<T, A, S> &&
        requires (const T & t, detail::black_hole_t & bh, S & s)
        {
            emit(t, bh, s);
        };
} // namespace proxima
