#pragma once

#include <proxima/concept/finalizable_with.hpp>
#include <proxima/concept/stateful_over.hpp>
#include <proxima/type_traits/state_of.hpp>

namespace proxima
{
    /*!
        \~english
            \brief
                Specifies that a transducer or a reduce kernel is finalizable over a symbol

            \tparam K
                The transducer or the reduce kernel type.
            \tparam A
                The type of the symbol from which the state `S` of `K` should be initialized.

            \returns
                `finalizable_with<K, S>` if `K` is stateful over the symbol `A`, and `false`
                otherwise (`S = state_of_t<K, A>`).

        \~russian
            \brief
                Определяет финализируемость преобразователя или ядра свёртки над символом

            \tparam K
                Тип преобразователя или ядра свёртки.
            \tparam A
                Тип символа, которым должно быть проинициализировано состояние `S`.

            \returns
                `finalizable_with<K, S>`, если `K` обладает состоянием над символом `A`, и `false`
                в противном случае (`S = state_of_t<K, A>`).

        \~  \see finalizable_with
            \see stateful_over
            \see state_of_t
     */
    template <typename K, typename A>
    concept finalizable_over = stateful_over<K, A> && finalizable_with<K, state_of_t<K, A>>;
} // namespace proxima
