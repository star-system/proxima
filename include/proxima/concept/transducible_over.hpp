#pragma once

#include <proxima/concept/statefully_transducible_over.hpp>
#include <proxima/concept/statelessly_transducible_over.hpp>

namespace proxima
{
    /*!
        \~english
            \brief
                Specifies a transducer

            \tparam T
                Potential transducer type.
            \tparam A
                Symbol type.

            \returns
                `true` if `T` is statelessly or statefully transducible over the symbol 'A',
                and `false` otherwise.

        \~russian
            \brief
                Преобразователь

            \tparam T
                Тип потенциального преобразователя.
            \tparam A
                Тип символа.

            \returns
                `true`, если `T` обладает операцией преобразования с состоянием или без над
                символом `A`, и `false` в противном случае.

        \~  \see statefully_transducible_over
            \see statelessly_transducible_over
     */
    template <typename T, typename A>
    concept transducible_over =
        statefully_transducible_over<T, A> || statelessly_transducible_over<T, A>;
} // namespace proxima
