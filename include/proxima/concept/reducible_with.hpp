#pragma once

#include <proxima/type_traits/state_symbol.hpp>

#include <concepts>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                Specifies that a kernel is reducible with a given symbol and state

            \details
                Class `K` considered reducible over the symbol `A` if the following member function
                is defined:

                    K: A × S -> void,

                where `A` is the symbol type, `S ` is the state type.

            \tparam K
                Potential reduce kernel type.
            \tparam A
                The input symbol type.
            \tparam S
                The state of the reduce kernel.

            \returns
                `true` if `K(A, S)` call is defined and `false` otherwise.

        \~russian
            \brief
                Концепция свёртки ядра с заданными символом и состоянием

            \details
                Класс `K` обладает операцией свёртки с состоянием `S` над символом `A`, если
                определена функция-член

                    K: A × S -> void,

                где `A` — тип символа, `S` — тип состояния.

            \tparam K
                Тип потенциального ядра свёртки.
            \tparam A
                Тип входного символа.
            \tparam S
                Тип состояния.

            \returns
                `true` в случае, если вызов `K(A, S)` определён, и `false` в противном случае.

        \~  \see reduce
     */
    template <typename K, typename A, typename S>
    concept reducible_with =
        std::convertible_to<A, state_symbol_t<S>> &&
        requires (const K & kernel, A && symbol, S & state)
        {
            kernel(std::forward<A>(symbol), state);
        };
} // namespace proxima
