#pragma once

#include <proxima/type_traits/detail/black_hole.hpp>
#include <proxima/type_traits/state_symbol.hpp>

#include <concepts>
#include <utility>

namespace proxima
{
    /*!
        \~english
            \brief
                Specifies a stateful transducer

            \details
                Class `T` considered statefully transducible over symbol `A` if the following
                member function is defined:

                    T: A × Tp × S -> void

                where `A` is the symbol type, `Tp` is the output tape type, `S` is the state type.

                Type of the tape does not matter. It matters only that it consumes the output of
                the transducer. Therefore, the check uses an "omnivorous" tape, and the tape is
                not exposed in the arguments of the metafunction.

            \tparam T
                Potential stateful transducer type.
            \tparam A
                Symbol type.
            \tparam S
                State type.

            \returns
                `true` if `T(A, Tp, S)` call is defined and `false` otherwise.

        \~russian
            \brief
                Преобразователь с состоянием

            \details
                Говорят, что класс `T` обладает операцией преобразования с состоянием над
                символом `A`, если определена функция-член

                    T: A × Tp × S -> void

                где `A` — тип символа, `Tp` — тип выходной ленты,`S` — тип состояния.

                Тип ленты не имеет значения. Важно только то, что на неё записывается символ,
                получаемый с выхода преобразователя. Поэтому в проверке используется "всеядная"
                лента, а в аргументах метафункции она и вовсе отсутствует.

            \tparam T
                Тип потенциального преобразователя с состоянием.
            \tparam A
                Тип символа.
            \tparam S
                Тип состояния.

            \returns
                `true` в случае, если вызов `T(A, Tp, S)` определён, и `false` в противном случае.

        \~  \see state_of
     */
    template <typename T, typename A, typename S>
    concept statefully_transducible_with =
        std::convertible_to<A, state_symbol_t<S>> &&
        requires (T && transducer, A && symbol, detail::black_hole_t tape, S & state)
        {
            std::forward<T>(transducer)(std::forward<A>(symbol), tape, state);
        };
} // namespace proxima
