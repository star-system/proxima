#pragma once

#include <proxima/concept/reducible_with.hpp>

namespace proxima
{
    /*!
        \~english
            \brief
                Specifies an emitting reduce kernel

            \details
                The reduce kernel `K` is considered emitting with the state `S` over the symbol `A`
                if `K` is reducible with the state `S` over the symbol `A` and the following
                function is defined:

                    void emit(const K &, S &),

                where `K` is the reduce kernel type, `S` is the state type.

            \tparam K
                Potential emitting reduce kernel.
            \tparam A
                Symbol type.
            \tparam S
                State type.

            \returns
                `true` if `reducible_with<K, A, S>` is `true` and the appropriate `emit` function
                is defined. Otherwise returns `false`.

        \~russian
            \brief
                Определяет ядро свёртки с завершением по требованию

            \details
                Говорят, что класс `K` обладает операцией свёртки с завершением по требованию
                с состоянием `S` над символом `A`, если он обладает операцией свёртки с
                состоянием `S` над символом `A`, а также для него определена функция

                    void emit(const K &, S &),

                где `K` — тип ядра свёртки, `S` — тип состояния.

            \tparam K
                Тип ядра свёртки.
            \tparam A
                Тип символа.
            \tparam S
                Тип состояния.

            \returns
                `true`, если истинно `reducible_with<K, A, S>` и определена функция `emit`,
                и `false` во всех остальных случаях.

        \~  \see reducible_with
            \see emit
     */
    template <typename K, typename A, typename S>
    concept emitting_reducible_with =
        reducible_with<K, A, S> &&
        requires (const K & k, S & s)
        {
            emit(k, s);
        };
} // namespace proxima
