#pragma once

#include <proxima/concept/statefully_transducible_with.hpp>
#include <proxima/type_traits/state_of.hpp>

namespace proxima
{
    /*!
        \~english
            \brief
                Specifies a stateful transducer over a given symbol

            \tparam T
                Potential stateful transducer type.
            \tparam A
                Symbol type.

            \returns
                `statefully_transducible_with<T, A, state_of_t<T, A>>`.

        \~russian
            \brief
                Преобразователь с состоянием над заданным символом

            \tparam T
                Тип потенциального преобразователя с состоянием.
            \tparam A
                Тип символа.

            \returns
                `statefully_transducible_with<T, A, state_of_t<T, A>>`.

        \~  \see state_of_t
     */
    template <typename T, typename A>
    concept statefully_transducible_over = statefully_transducible_with<T, A, state_of_t<T, A>>;
} // namespace proxima
