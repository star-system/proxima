#pragma once

#include <proxima/concept/emitting_reducible_with.hpp>
#include <proxima/type_traits/state_of.hpp>

namespace proxima
{
    /*!
        \~english
            \brief
                Specifies an emitting reduce kernel over a symbol

            \tparam K
                Potential reduce kernel type.
            \tparam A
                Type of symbol from which state of `T` should be initialized.

            \returns
                `emitting_reducible_with<K, A, state_of_t<K, A>>`.

        \~russian
            \brief
                Определяет ядро свёртки над символом с завершением по требованию

            \tparam K
                Тип потенциального ядра свёртки с завершением по требованию.
            \tparam A
                Тип инициализирующего символа для состояния ядра `K`.

            \returns
                `emitting_reducible_with<K, A, state_of_t<K, A>>`.

        \~  \see emitting_reducible_with
            \see state_of_t
     */
    template <typename K, typename A>
    concept emitting_reducible_over = emitting_reducible_with<K, A, state_of_t<K, A>>;
} // namespace proxima
