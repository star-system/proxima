#pragma once

#include <proxima/concept/stateful_over.hpp>

namespace proxima
{
    /*!
        \~english
            \brief
                Specifies that a transducer or a reduce kernel does not have state

            \details
                Is a negation of `stateful_over` concept.

            \tparam K
                The transducer or the reduce kernel type.
            \tparam A
                The symbol type.

            \returns
                `true` if `initialize` is not defined and `false` otherwise.

        \~russian
            \brief
                Преобразователь или ядро свёртки без состояния

            \details
                Является отрицанием концепции `stateful_over`.

            \tparam K
                Тип преобразователя или ядра свёртки.
            \tparam A
                Тип символа.

            \returns
                `true` в случае, если функция `initialize` не определена, и `false` в противном
                случае.

        \~  \see initialize
            \see stateful_over
     */
    template <typename K, typename A>
    concept stateless_over = not stateful_over<K, A>;
} // namespace proxima
