#pragma once

#include <proxima/thread/detail/synchronization.hpp>
#include <proxima/thread/detail/worker.hpp>

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <functional>
#include <future>
#include <iterator>
#include <mutex>
#include <optional>
#include <thread>
#include <type_traits>
#include <utility>
#include <vector>

namespace proxima
{
    /*!
        \~english
            \brief
                A thread pool

        \~russian
            \brief
                Пул потоков
     */
    class thread_pool
    {
    public:
        /*!
            \~english
                \brief
                    Create a thread pool with a specified number of worker threads

                \param thread_count
                    The number of worker threads that will be available after the pool is created.

                \pre
                    `thread_count > 0`

            \~russian
                \brief
                    Создать пул потоков с заданным количеством рабочих потоков

                \param thread_count
                    Количество рабочих потоков, которые будут доступны после создания пула.

                \pre
                    `thread_count > 0`
         */
        explicit thread_pool (std::size_t thread_count)
        {
            assert(thread_count > 0);

            if (thread_count > 1)
            {
                m_threads.reserve(thread_count);
                std::generate_n(std::back_inserter(m_threads), thread_count,
                    [this]
                    {
                        return std::thread(&detail::public_queue_worker, std::ref(m_synchro));
                    });
            }
            else
            {
                m_threads.emplace_back(&detail::private_queue_worker, std::ref(m_synchro));
            }
        }

        thread_pool (const thread_pool &) = delete;
        thread_pool & operator = (const thread_pool &) = delete;
        thread_pool (thread_pool &&) = delete;
        thread_pool & operator = (thread_pool &&) = delete;

        /*!
            \~english
                \brief
                    Thread pool completition

                \details
                    Performs stop() followed by wait().

            \~russian
                \brief
                    Завершение пула потоков

                \details
                    Последовательно вызывает методы stop() и wait().

            \~  \see stop
                \see wait
         */
        ~thread_pool ()
        {
            stop();
            wait();
        }

        /*!
            \~english
                \brief
                    Enqueue a task

                \details
                    This method should be used only when it is known, that all preconditions are
                    fulfilled. Otherwise the try_post() method should be used.

                \param f
                    A function object that must be executed asynchronously.

                \pre
                    The queue is not closed and the thread pool is not stopped. In other words,
                    the stop() method and the wait() method have never been called for this
                    thread pool.
                \post
                    The task to execute the `f` function object is enqueued.

                \returns
                    `std::future` of the result of `f`.

            \~russian
                \brief
                    Поставить задачу на исполнение

                \details
                    Данный метод должен быть использован только в том случае, когда точно известно,
                    что выполнены все предусловия. Если такой уверенности нет, то следует
                    воспользоваться методом try_post().

                \param f
                    Функциональный объект, который должен быть вызван асинхронно.

                \pre
                    Очередь не закрыта и пул потоков не остановлен. Иначе говоря, ни метод stop(),
                    ни метод wait() ни разу не были вызваны для данного пула потоков.
                \post
                    Задача на выполнение функционального объекта `f` поставлена в очередь.

                \returns
                    `std::future` от результата, возвращаемого функциональным объектом `f`.

            \~  \see try_post
         */
        template <typename NullaryFunction>
        auto post (NullaryFunction && f)
        {
            assert(not m_threads.empty());
            assert(not m_synchro.queue_is_closed);

            using return_type = std::invoke_result_t<NullaryFunction>;
            auto task = std::packaged_task<return_type ()>(std::forward<NullaryFunction>(f));
            auto future = task.get_future();

            std::unique_lock queue_lock(m_synchro.queue_mutex);
            m_synchro.tasks.emplace_back(std::move(task));

            queue_lock.unlock();
            m_synchro.queue_cond.notify_one();

            return future;
        }

        /*!
            \~english
                \brief
                    Try to enqueue a task

                \details
                    If the queue is not closed (i.e. wait() method has not been ever called for
                    this thread pool) and the thread pool is not stopped (i.e. stop() method has
                    not been ever called for it), then adds to the end of the execution queue a
                    task, which should call the function object `f`.

                \param f
                    A function object that must be executed asynchronously.

                \returns
                    `std::optional` with `std::future` of the result of `f` in case of success,
                    and an empty `std::optional` otherwise.

            \~russian
                \brief
                    Попытаться поместить задачу в очередь

                \details
                    Если очередь не закрыта (то есть метод wait() не был ни разу вызван для данного
                    пула потоков) и пул потоков не остановлен (то есть для него ни разу не был
                    вызван метод stop()), то добавляет в конец очереди на исполнение задачу,
                    которая должна вызвать функциональный объект `f`.

                \param f
                    Функциональный объект, который должен быть вызван асинхронно.

                \returns
                    В случае успеха возвращает `std::optional` от `std::future` от результата,
                    возвращаемого функциональным объектом `f`, и пустой `std::optional` в противном
                    случае.

            \~  \see post
                \see stop
                \see wait
         */
        template <typename NullaryFunction>
        auto try_post (NullaryFunction && f)
        {
            using return_type = std::invoke_result_t<NullaryFunction>;

            if (not m_synchro.queue_is_closed)
            {
                std::unique_lock queue_lock(m_synchro.queue_mutex);
                if (not m_synchro.queue_is_closed)
                {
                    auto task =
                        std::packaged_task<return_type ()>(std::forward<NullaryFunction>(f));
                    auto future = task.get_future();

                    m_synchro.tasks.emplace_back(std::move(task));

                    queue_lock.unlock();
                    m_synchro.queue_cond.notify_one();

                    return std::optional{std::move(future)};
                }
            }

            return std::optional<std::future<return_type>>{};
        }

        /*!
            \~english
                \brief
                    Stop the thread pool

                \details
                    Signals worker threads in the pool to complete as soon as possible, and clears
                    the current task queue. If a thread is currently executing a task, the thread
                    will exit only after completion of that task.
                    Invocation of stop() returns without waiting for the threads to complete.

                \post
                    Thread pool is closed.
                \post
                    Task queue is empty.

            \~russian
                \brief
                    Остановка пула потоков

                \details
                    Сигнализирует рабочим потокам о том, что им необходимо завершиться как можно
                    быстрее и очищает текущую очередь задач. Если рабочий поток в момент вызова
                    метода выполняет какую-либо задачу, то он завершится только после выполнения
                    этой задачи.
                    Вызов данного метода возвращает управление не дожидаясь завершения рабочих
                    потоков.

                \post
                    Пул потоков остановлен.
                \post
                    Очередь задач пуста.
         */
        void stop ()
        {
            std::unique_lock queue_lock(m_synchro.queue_mutex);

            m_synchro.queue_is_closed = true;
            m_synchro.tasks.clear();

            queue_lock.unlock();
            m_synchro.queue_cond.notify_all();
        }

        /*!
            \~english
                \brief
                    Complete all tasks posted to this thread pool so far

                \details
                    If not already stopped, closes the task queue and signals worker threads in the
                    pool to complete once the task queue is empty.
                    Blocks the calling thread until all threads in the pool have completed.

                \post
                    Task queue is empty.
                \post
                    All worker threads are stopped.

                \warning
                    Calling this method from different threads at the same time will likely
                    lead to crash.

            \~russian
                \brief
                    Завершить все задачи, поставленные перед данным пулом потоков

                \details
                    Если пул не остановлен, то закрывает очередь и сигнализирует рабочим потокам о
                    том, что они должны завершиться как только очередь задач опустеет.
                    Блокирует вызывающий поток до тех пор, пока не завершатся все рабочие потоки.

                \post
                    Очередь задач пуста.
                \post
                    Все рабочие потоки остановлены.

                \warning
                    Одновременный вызов данного метода из разных потоков с большой вероятностью
                    приведёт к падению.
         */
        void wait ()
        {
            if (not m_threads.empty())
            {
                std::unique_lock queue_lock(m_synchro.queue_mutex);
                m_synchro.queue_is_closed = true;

                queue_lock.unlock();
                m_synchro.queue_cond.notify_all();

                // Чтобы wait() можно было легально вызывать параллельно, завершение потоков пула и
                // их уничтожение должно быть под отдельной блокировкой.
                for (auto & t: m_threads)
                {
                    t.join();
                }
                m_threads.clear();

                assert(m_synchro.tasks.empty());
            }
        }

    private:
        std::vector<std::thread> m_threads;
        detail::thread_pool_synchronization_t m_synchro;
    };
} // namespace proxima
