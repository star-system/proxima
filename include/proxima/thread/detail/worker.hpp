#pragma once

#include <proxima/thread/detail/synchronization.hpp>

#include <deque>
#include <mutex>
#include <utility>

namespace proxima::detail
{
    /*!
        \~english
            \brief
                Extract one task from the queue

            \details
                After the task is removed from the queue, the lock is released. The idea is to
                allow other threads to work with the queue while the current task is running.

            \pre
                `lock` is locked.
            \pre
                Task queue is not empty.

            \post
                `lock` is unlocked.
            \post
                There is exactly one less task in the `tasks` queue than there was before.

        \~russian
            \brief
                Извлечь одну задачу из очереди

            \details
                После извлечения задачи из очереди блокировка снимается. Это нужно для того, чтобы
                не блокировать работу других потоков с очередью, пока выполняется текущая задача.

            \pre
                Блокировка `lock` заблокирована.
            \pre
                В очереди есть хотя бы одна задача.

            \post
                Блокировка `lock` разблокирована.
            \post
                В очереди `tasks` ровно на одну задачу меньше, чем было.
     */
    template <typename NothrowMoveConstructible, typename BasicLockable>
    auto extract_one (std::deque<NothrowMoveConstructible> & tasks, BasicLockable & lock)
    {
        auto task = std::move(tasks.front());
        tasks.pop_front();

        lock.unlock();

        return task;
    }

    /*!
        \~english
            \brief
                Handles tasks from the open queue

            \details
                Waits for the next symbol to appear in the queue and processes it.
                When the queue becomes closed for the input, it returns right away.

                This function can be used both with public and private queues.

            \post
                `synchro.queue_is_closed == true`

        \~russian
            \brief
                Обработка задач из открытой очереди

            \details
                Ждёт, пока в очереди появится очередной символ, и обрабатывает его.
                Когда вход в очередь закрывается, то сразу завершается.

                Данная функция может быть использована и с общими, и с частными очередями.

            \post
                `synchro.queue_is_closed == true`

        \~  \see closed_public_queue_loop
            \see closed_private_queue_loop
            \see thread_pool
            \see thread_pool_synchronization_t
            \see public_queue_worker
            \see private_queue_worker
     */
    inline void open_queue_loop (thread_pool_synchronization_t & synchro)
    {
        while (not synchro.queue_is_closed)
        {
            std::unique_lock queue_lock(synchro.queue_mutex);
            synchro.queue_cond.wait(queue_lock,
                [& synchro]
                {
                    return synchro.queue_is_closed || not synchro.tasks.empty();
                });

            if (not synchro.tasks.empty())
            {
                auto task = extract_one(synchro.tasks, queue_lock);
                task();
            }
        }
    }

    /*!
        \~english
            \brief
                Handles the remaining tasks in a closed public queue

            \details
                A queue considered public if several workers are associated with it (i.e. can take
                tasks from it).

                This function can be used both with public and private queue however using it with
                private queue is ineffective.

                Processes all the remaining elements in the queue and exits.

            \pre
                `synchro.queue_is_closed == true`

        \~russian
            \brief
                Обработка оставшихся задач в закрытой общей очереди

            \details
                Общей называется такая очередь, из которой могут доставать задачи сразу несколько
                разных обработчиков.

                Данная функция может быть использована и с общими, и с частными очередями, но
                использовать её с частными неэффективно.

                Обрабатывает все оставшиеся элементы в очереди и завершается.

            \pre
                `synchro.queue_is_closed == true`

        \~  \see thread_pool
            \see thread_pool_synchronization_t
            \see closed_private_queue_loop
            \see public_queue_worker
            \see open_queue_loop
     */
    inline void closed_public_queue_loop (thread_pool_synchronization_t & synchro)
    {
        while (not synchro.tasks.empty())
        {
            std::unique_lock queue_lock(synchro.queue_mutex);
            if (not synchro.tasks.empty())
            {
                auto task = extract_one(synchro.tasks, queue_lock);
                task();
            }
        }
    }

    /*!
        \~english
            \brief
                Handles the remaining tasks in a closed private queue

            \details
                A queue considered private if only one worker is associated with it (i.e. can take
                tasks from it).

                This function can't be used with public queues.

                Processes all the remaining elements in the queue and exits.
                This function does not use synchronization primitives when processing the remaining
                elements in the queue because the private queue has only one worker thread (by
                definition), and the queue is closed (by precondition). This means that nobody else
                will try to manipulate the queue, therefore, synchronization is no longer required.

            \pre
                `synchro.queue_is_closed == true`

        \~russian
            \brief
                Обработка оставшихся задач в закрытой частной очереди

            \details
                Частной называется такая очередь, с которой связан (то есть может доставать из неё
                задачи) только один обработчик.

                Данная функция не может быть использована с общей очередью.

                Обрабатывает все оставшиеся элементы в очереди и завершается.
                Особенность данной функции в том, что она не использует примитивы синхронизации при
                обработке оставшихся элементов в очереди, поскольку у частной очереди рабочий поток
                по построению один на очередь, а очередь по предусловию закрыта. Это значит, что
                больше никто уже не попытается манипулировать очередью, следовательно, и
                синхронизация более не требуется.

            \pre
                `synchro.queue_is_closed == true`

        \~  \see thread_pool
            \see thread_pool_synchronization_t
            \see closed_public_queue_loop
            \see private_queue_worker
            \see open_queue_loop
     */
    inline void closed_private_queue_loop (thread_pool_synchronization_t & synchro)
    {
        while (not synchro.tasks.empty())
        {
            auto task = std::move(synchro.tasks.front());
            synchro.tasks.pop_front();

            task();
        }
    }

    /*!
        \~english
            \brief
                The public task queue worker

            \details
                A queue considered public if several workers are associated with it (i.e. can take
                tasks from it).

                This worker can be used both with public and private queue however using it with
                private queue is ineffective.

                Runs in a separate thread.
                Waits for the next symbol to appear in the queue and processes it.
                When the queue becomes closed for the input, the worker processes all the remaining
                elements in the queue and exits.

        \~russian
            \brief
                Обработчик общей очереди задач

            \details
                Общей называется такая очередь, из которой могут доставать задачи сразу несколько
                разных обработчиков.

                Данный обработчик может быть использован и с общими, и с частными очередями, но
                использовать его с частными неэффективно.

                Запускается в отдельном потоке.
                Ждёт, пока в очереди появится очередной символ, и обрабатывает его.
                Когда вход в очередь закрывается, обрабатывает все оставшиеся элементы в очереди и
                завершается.

        \~  \see open_queue_loop
            \see closed_public_queue_loop
            \see thread_pool
            \see thread_pool_synchronization_t
            \see private_queue_worker
     */
    inline void public_queue_worker (thread_pool_synchronization_t & synchro)
    {
        open_queue_loop(synchro);
        closed_public_queue_loop(synchro);
    }

    /*!
        \~english
            \brief
                The private task queue worker

            \details
                A queue considered private if only one worker is associated with it (i.e. can take
                tasks from it).

                This worker is required for efficient work with private queues and cannot be used
                used with public queues.

                Runs in a separate thread.
                Waits for the next symbol to appear in the queue and processes it.
                When the queue becomes closed for the input, the worker processes all the remaining
                elements in the queue and exits.

        \~russian
            \brief
                Обработчик частной очереди задач

            \details
                Частной называется такая очередь, с которой связан (то есть может доставать из неё
                задачи) только один обработчик.

                Данный обработчик нужен для эффективной работы с частной очередью и не может быть
                использован с общей очередью.

                Запускается в отдельном потоке.
                Ждёт, пока в очереди появится очередной символ, и обрабатывает его.
                Когда вход в очередь закрывается, обрабатывает все оставшиеся элементы в очереди и
                завершается.

        \~  \see open_queue_loop
            \see closed_private_queue_loop
            \see thread_pool
            \see thread_pool_synchronization_t
            \see public_queue_worker
     */
    inline void private_queue_worker (thread_pool_synchronization_t & synchro)
    {
        open_queue_loop(synchro);
        closed_private_queue_loop(synchro);
    }
} // namespace proxima::detail

