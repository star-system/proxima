set(PROXIMA_CATCH2_VERSION 3.7.1)
set(PROXIMA_CATCH2_REPOSITORY https://github.com/catchorg/Catch2.git)

find_package(Catch2 ${PROXIMA_CATCH2_VERSION})

if (Catch2_FOUND)
    message(STATUS "Найден Catch2 ${Catch2_VERSION}: ${Catch2_DIR}")
elseif (PROXIMA_DOWNLOAD_PACKAGES)
    message(STATUS
        "Catch2 ${PROXIMA_CATCH2_VERSION} будет взят с гитхаба: ${PROXIMA_CATCH2_REPOSITORY}")

    include(FetchContent)
    FetchContent_Declare(Catch2
        GIT_REPOSITORY
            ${PROXIMA_CATCH2_REPOSITORY}
        GIT_TAG
            v${PROXIMA_CATCH2_VERSION}
    )
    FetchContent_MakeAvailable(Catch2)
    target_compile_options(Catch2 PRIVATE -Wno-error)
    get_property(PROXIMA_CATCH2_INCLUDES TARGET Catch2 PROPERTY INTERFACE_INCLUDE_DIRECTORIES)
    set_property(TARGET Catch2 PROPERTY INTERFACE_SYSTEM_INCLUDE_DIRECTORIES ${PROXIMA_CATCH2_INCLUDES})
else ()
    message(FATAL_ERROR
        "Для тестирования требуется библиотека Catch2. "
        "Можно установить её вручную, включить опцию PROXIMA_DOWNLOAD_PACKAGES, "
        "или отключить тестирование с помощью опции PROXIMA_TESTING"
    )
endif()
