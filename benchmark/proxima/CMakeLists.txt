find_package(Threads)

add_executable(reduce_hard reduce_hard.cpp)
target_link_libraries(reduce_hard
    PRIVATE
        Proxima::proxima
        Threads::Threads
)
