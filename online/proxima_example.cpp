#include <proxima/compose.hpp>
#include <proxima/kernel/sum.hpp>
#include <proxima/reduce.hpp>
#include <proxima/transducer/for_each.hpp>
#include <proxima/transducer/transform.hpp>

#include <cassert>

int main ()
{
    const int items[] = {1, 2, 3};                           // Исходная последовательность.
    const auto kernel =
        proxima::compose                                     // Создаём обработчик, который:
        (
            proxima::transform([] (auto x) {return x * x;}), // 1.  Возведёт в квадрат каждый входной
                                                             //     элемент;
            proxima::for_each([] (auto & x) {++x;}),         // 2.  Увеличит на единицу;
            proxima::sum                                     // 3.  Просуммирует полученные элементы.
        );
    const auto x = proxima::reduce(items, kernel);           // Применение созданного обработчика к
                                                             // исходной последовательности.
    assert(x == 17); // 1 * 1 + 1 + 2 * 2 + 1 + 3 * 3 + 1
}
